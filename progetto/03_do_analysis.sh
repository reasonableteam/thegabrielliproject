#!/bin/bash

#CLASSPATH="WordsRelator/bin:TweetsMining/libs/jena/*:TweetsMining/libs/guava/*:TweetsMining/libs/miner:TweetsMining/bin" 

CLASSPATH="../WordsRelator/bin:libs/jena/httpcore-4.2.2.jar:libs/jena/jena-iri-0.9.5.jar:libs/jena/jena-core-2.10.0.jar:libs/jena/jcl-over-slf4j-1.6.4.jar:libs/jena/slf4j-log4j12-1.6.4.jar:libs/jena/xml-apis-1.4.01.jar:libs/jena/jena-tdb-0.10.0.jar:libs/jena/xercesImpl-2.10.0.jar:libs/jena/jena-arq-2.10.0.jar:libs/jena/httpclient-4.2.3.jar:libs/jena/log4j-1.2.16.jar:libs/jena/slf4j-api-1.6.4.jar:libs/jena/commons-codec-1.6.jar:libs/guava/guava-17.0.jar:libs/guice/javax.inject.jar:libs/guice/aopalliance-1.0.jar:libs/guice/guice-3.0.jar:libs/miner/ws4j-1.0.1.jar:libs/miner/Jama-1.0.3.jar:libs/miner/commons-collections4-4.0.jar:libs/miner/aterm-java-1.6.jar:libs/miner/commons-collections4-4.0-tests.jar:./bin"

JAVA_CMD="java -cp $CLASSPATH org.tweetsmining.engines.graphsminer.main.GraphMinerMain"

cd TweetsMining

function help {
   $JAVA_CMD help
}

#cmd=$1
declare -A args;
case $1 in
   g) 
      [ $# -ne 3 ] && help && exit
      userToRelatedWordsListInput="../$2"
      userToMultiLayerGraphOutput="../$3"
      args[0]=$userToRelatedWordsListInput
      args[1]=$userToMultiLayerGraphOutput
      ;;
   m)
      [ $# -ne 5 ] && help && exit
      userToMultiLayerGraphInput="../$2"
      inflate="$3"
      iterate="$4"
      dbToClusterOutput=$5
      args[0]=$dbToClusterInput
      args[1]=$inflate
      args[2]=$iterate
      args[3]=$dbToClusterOutput
     ;;
   s)
      [ $# -ne 6 ] && help && exit
      userToMultiLayerGraphInput="../$2"
      dbToClusterInput="../$3"
      inflate="$4"
      iterate="$5"
      mineDifferencesOutput="../$6"
      args[0]=$dbToClusterInput
      args[1]=$userToMultiLayerGraphInput
      args[2]=$inflate
      args[3]=$iterate
      args[4]=$mineDifferencesOutput
      ;;
   *)
      $JAVA_CMD help
      exit
      ;;
esac

echo $JAVA_CMD $1 ${args[@]}

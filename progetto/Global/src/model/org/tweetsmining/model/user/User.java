package org.tweetsmining.model.user;

import java.io.Serializable;

public class User implements Serializable {

	private static final long serialVersionUID = -4220142970966358218L;
	private String user;
	
	public User() {}
	
	public User(String user) {
		this.user = user;
	}
	
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	
	@Override
	public String toString() {
		return this.user;
	}
	
}

package org.tweetsmining.model.graphsminer;

import java.io.StringWriter;
import java.util.LinkedList;

import org.tweetsmining.io.serialization.StringSerialization;
import org.tweetsmining.model.user.User;

public class Cluster extends StringSerialization {

	private static final long serialVersionUID = -427458371610398779L;
	private LinkedList<User> lu;
	
	public Cluster(String str) {
		super(str);
		lu = new LinkedList<>();
		for (String e : str.split("|")) {
			lu.add(new User(e));
		}
	}
	
	public Cluster(LinkedList<User> lu) {
		super(null);
		this.lu = lu;
	}

	public LinkedList<User> getListUsers() {
		return lu;
	}

	public void setListUsers(LinkedList<User> lu) {
		this.lu = lu;
	}
	
	@Override
	public String toString() {
		StringWriter toret = new StringWriter();
		for (User x: lu) {
			toret.append(x.toString());
		}
		return toret.toString();
	}
	
}

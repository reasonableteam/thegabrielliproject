/*******************************************************************************
 * Copyleft (c) 2014, "Massimiliano Leone - <maximilianus@gmail.com> - https://plus.google.com/+MassimilianoLeone"
 * This file (RelationStringNullException.java) is part of WordsRelator.
 * 
 *     RelationStringNullException.java is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     RelationStringNullException.java is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with .  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.tweetsmining.model.wordsrelator.exceptions;

public class RelationStringNullException extends RelationsException {

	private static final long serialVersionUID = 5389604712186718830L;

	public RelationStringNullException() {
		super();
	};

	public RelationStringNullException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public RelationStringNullException(String message, Throwable cause) {
		super(message, cause);
	}

	public RelationStringNullException(String message) {
		super(message);
	}

	public RelationStringNullException(Throwable cause) {
		super(cause);
	}

}

/*******************************************************************************
 * Copyleft (c) 2014, "Massimiliano Leone - <maximilianus@gmail.com> - https://plus.google.com/+MassimilianoLeone"
 * This file (WordRelations.java) is part of WordsRelator.
 * 
 *     WordRelations.java is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     WordRelations.java is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with .  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.tweetsmining.model.wordsrelator;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import net.sf.json.JSONObject;

import org.tweetsmining.io.serialization.Serializer;
import org.tweetsmining.io.serialization.JSONArraySetSerializer;
import org.tweetsmining.io.serialization.JSONObjectMapSerializer;
import org.tweetsmining.io.serialization.JSONObjectSerialization;
import org.tweetsmining.io.serialization.JSONObjectSerializationByReflection;
import org.tweetsmining.io.serialization.NullSerializer;


public class WordRelations extends JSONObjectSerialization {
	
	private final String word;
	private final RelationType relationTypeForMainsense;
	private final Map<String,Set<RelationedTerm>> mainsensesToTermsMap;
	private final JSONObjectMapSerializer<String,Set<RelationedTerm>> mapper;
	

	public WordRelations(JSONObject obj) {
		super(obj);
		System.out.println("WordRelations: unserialized");
		this.word = obj.getString("word");
		this.relationTypeForMainsense = RelationType.getUnmarshalledType(obj.getString("relationTypeForMainsense"));
		Serializer<String> keyser = new NullSerializer<String>();
		Serializer<Set<RelationedTerm>> valser = new JSONArraySetSerializer<RelationedTerm>(new JSONObjectSerializationByReflection<>(RelationedTerm.class));
		this.mapper = new JSONObjectMapSerializer<String,Set<RelationedTerm>>(keyser, valser);
		if (obj.get("mainsensesToTermMap") == null) {
			System.out.println("Element not found: ");
			for (Object x : obj.keySet()) {
				System.out.print(x + ", ");
			}
			System.out.println("to be picked up ");
		} else {
			JSONObject jo = (JSONObject) obj.get("mainsensesToTermMap");
			for (Object o : jo.keySet()) {
				System.out.print(o + ", ");
			}
		}
		this.mainsensesToTermsMap = this.mapper.unmarshalling(obj.get("mainsensesToTermMap"));
	}
	
	public WordRelations(String word, RelationType relationTypeToMainsense) {
		super(null);
		this.word = word;
		this.relationTypeForMainsense = relationTypeToMainsense;
		this.mainsensesToTermsMap  = new HashMap<>();
		Serializer<String> keyser = new NullSerializer<String>();
		Serializer<Set<RelationedTerm>> valser = new JSONArraySetSerializer<RelationedTerm>(new JSONObjectSerializationByReflection<>(RelationedTerm.class));
		this.mapper = new JSONObjectMapSerializer<String,Set<RelationedTerm>>(keyser, valser);
	}
	
	public void addMainsenseTermsEntry(String mainsense, Set<RelationedTerm> relationedTerms) {
		mainsensesToTermsMap.put(mainsense, relationedTerms);
	}
		
	public String getWord() {
		return word;
	}
	public Set<String> getMainsenses() {
		return mainsensesToTermsMap.keySet();
	}
	public void addMainsense(String mainsense) {
		mainsensesToTermsMap.keySet().add(mainsense);
	}
	public RelationType getRelationTypeForMainsense() {
		return relationTypeForMainsense;
	}
	
	public Set<RelationedTerm> getTermsForMainsense(String mainsense) {
		if (mainsensesToTermsMap.containsKey(mainsense))
			return mainsensesToTermsMap.get(mainsense);
		return new HashSet<>(0);
	}
	
	public Map<String, Set<RelationedTerm>> getMainsensesToTermMap() {
		return mainsensesToTermsMap;
	}
	
	public RelationType getRelationType() {
		return relationTypeForMainsense;
	}

	@Override
	public JSONObject serialize() {
		JSONObject toret = new JSONObject();
		toret.put("word", word);
		toret.put("relationTypeForMainsense", relationTypeForMainsense.toString());
		toret.put("mainsensesToTermMap", mapper.marshalling(mainsensesToTermsMap));
		System.out.println("serialized "+ word);
		return toret;
	}

}
 

/*******************************************************************************
 * Copyleft (c) 2014, "Massimiliano Leone - <maximilianus@gmail.com> - https://plus.google.com/+MassimilianoLeone"
 * This file (RelationedTerm.java) is part of WordsRelator.
 * 
 *     RelationedTerm.java is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     RelationedTerm.java is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with .  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.tweetsmining.model.wordsrelator;

import net.sf.json.JSONObject;

import org.tweetsmining.io.serialization.JSONObjectSerialization;


public class RelationedTerm extends JSONObjectSerialization {
			
	private RelationType relationType;
	private String term;
	
	public RelationedTerm(String term, RelationType relationType) {
		super(null);
		this.term = term;
		this.relationType = relationType;
	}
	
	public RelationedTerm(JSONObject o) {
		super(o);
		/*if (o==null)
			System.out.println("passed null");
		else 
			for (Object k : o.keySet()) {
				System.out.println("Relationed: " + k + " ~ " + o.get(k));
			}
		*/
		this.term = (String)o.get("term");
		this.relationType = RelationType.getUnmarshalledType((String)o.get("relationType"));
		
	}

	public RelationType getRelationType() {
		return relationType;
	}

	public String getTerm() {
		return term;
	}

	@Override
	public JSONObject serialize() {
		JSONObject toret = new JSONObject();
		toret.put("relationType", relationType.toString());
		toret.put("term", term);
		return toret;
	}

	@Override
	public boolean equals(Object o) {
		if (o==null)
			return false;
		if (!(o instanceof RelationedTerm))
			return false;
		RelationedTerm rt = (RelationedTerm)o;
		return (rt.getRelationType().equals(relationType) && rt.getTerm().equals(term));
	}

	/*
	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {
		term = in.readLine();
		relationType = Enum.valueOf(RelationType.class, in.readLine());
	}

	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		out.writeChars(term);
		out.writeChars(relationType.name());		
	}*/
}

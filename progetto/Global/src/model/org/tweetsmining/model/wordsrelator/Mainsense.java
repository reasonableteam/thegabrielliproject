/*******************************************************************************
 * Copyleft (c) 2014, "Massimiliano Leone - <maximilianus@gmail.com> - https://plus.google.com/+MassimilianoLeone"
 * This file (Mainsense.java) is part of WordsRelator.
 * 
 *     Mainsense.java is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     Mainsense.java is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with .  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.tweetsmining.model.wordsrelator;

public class Mainsense {

	private String word;
	private String mainsense;
	private RelationType relationType;
	
	public Mainsense(String mainsense, RelationType relationType) {
//		this.word = word;
		this.mainsense = mainsense;
		this.relationType = relationType;
	}

	public final String getWord() {
		return word;
	}

	protected final void setWord(String word) {
		this.word = word;
	}

	public final String getMainsense() {
		return mainsense;
	}

	public final void setMainsense(String mainsense) {
		this.mainsense = mainsense;
	}

	public final RelationType getRelationType() {
		return relationType;
	}

	public final void setRelationType(RelationType relationType) {
		this.relationType = relationType;
	}	
}

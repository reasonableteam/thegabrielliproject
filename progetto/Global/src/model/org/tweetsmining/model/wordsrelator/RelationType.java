/*******************************************************************************
 * Copyleft (c) 2014, "Massimiliano Leone - <maximilianus@gmail.com> - https://plus.google.com/+MassimilianoLeone"
 * This file (RelationType.java) is part of WordsRelator.
 * 
 *     RelationType.java is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     RelationType.java is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with .  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.tweetsmining.model.wordsrelator;

import org.tweetsmining.model.wordsrelator.exceptions.RelationStringNullException;


public enum RelationType {
	
	DISAMBIGUATION,
	HYPERNYM,
	PARTOF,
	SEMREL,
	SYNSET;
	
	public String getUrl() {
		return prefix+"#"+this.name().toLowerCase();
	}
	
	public static final String prefix = "http://tweetsmining.org/relation";
	
	public static final RelationType getRelationType(String relation) throws RelationStringNullException {
		if (relation==null)
            throw new RelationStringNullException();
        relation = relation.toLowerCase();
        if (relation.contains("holonym") || relation.contains("meronym"))  {
            return RelationType.PARTOF;
        } else if (relation.contains("hypernym")) {
            return RelationType.HYPERNYM;
        } else if (relation.contains("semantic") && relation.contains("related"))
        	return RelationType.SEMREL;
        else
            return RelationType.DISAMBIGUATION;
	}
	public static final RelationType getUnmarshalledType(String marshal)  {
		if (marshal.equals(DISAMBIGUATION.toString())) 
			return DISAMBIGUATION;
		else if (marshal.equals(HYPERNYM.toString()))
			return HYPERNYM;
		else if (marshal.equals(PARTOF.toString()))
			return PARTOF;
		else if (marshal.equals(SYNSET.toString()))
			return SYNSET;
		else 
			return SEMREL;
	}
	
	public static String relationToUrl(String relation) throws RelationStringNullException {
		
        return getRelationType(relation).getUrl();
			
		
        /*
		if (relation !=null) // to be completed from BasicProject.java
			
			if disambiguation
			if hyponym
			ecc ecc
			
			return RelationType.HYPERNYM.getUrl();
		return "";
		*/
	}

}

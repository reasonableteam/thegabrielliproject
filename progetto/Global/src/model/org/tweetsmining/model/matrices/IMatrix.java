/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.tweetsmining.model.matrices;


import java.util.Set;

import org.apache.commons.collections4.keyvalue.MultiKey;

/**
 *
 * @author gyankos
 */
public interface IMatrix {
    
   public int nCols();
   public int nRows();
    
   public double get(int i, int j);
   /**
    * Returns the cell's value
    * @param x cell coordinates
    * @return 
    */
   public double get(MultiKey x);
   
   /**
    * Adds val to the x cell value
    * @param x      Cell coordinates
    * @param val    Value
    * @ 
    */
   public void incr(MultiKey x, double val) ;
   
   /**
    * Removes the cell (i,j)s
    * @param i  Row
    * @param j  Column
    */
   public void rem(int i, int j);
   
   /**
    * Add val to the cell (i,j)
    * @param i  row
    * @param j  Column
    * @param val    Value
    * @ 
    */
   public void incr(int i, int j, double val) ;
   
   
   public void set(int i, int j,double val) ;
   
   public void set(MultiKey x,double val) ;
   
   /**
    * Removes cells that have non-empty values
    * @return
    * @ 
    */
   public Set<MultiKey> getValueRange() ;
   
   /**
    * Removes the whole row i
    * @param i 
    */
   public void removeRow(int i);
   
   /**
    * Removes the whole column j
    * @param j 
    */
   public void removeCol(int j);
   
   /**
    * Removes both row and columns with the same number
    * @param elem 
    */
   public void removeEnt(int elem);
   
   public void sum(IMatrix right);
   public void diff(IMatrix right);
    
}

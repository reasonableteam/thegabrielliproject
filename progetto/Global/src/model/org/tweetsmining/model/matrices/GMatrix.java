/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.tweetsmining.model.matrices;

import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.collections4.keyvalue.MultiKey;
import org.apache.commons.collections4.map.LinkedMap;
import org.apache.commons.collections4.map.MultiKeyMap;

/**
 *
 * @author gyankos
 */
public class GMatrix implements IMatrix {

    MultiKeyMap p;
    int row_min = 0;
    int row_max = 0;
    int col_min = 0;
    int col_max = 0;

    public GMatrix() {
        p = MultiKeyMap.multiKeyMap(new LinkedMap());
    }

    public GMatrix(IMatrix cpy) {
        this();
        try {
            for (MultiKey x : cpy.getValueRange()) {
                set(x, cpy.get(x));
            }
        } catch (Throwable ex) {
            Logger.getLogger(GMatrix.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns the value of cell (i,j)
     *
     * @param i riga
     * @param j colonna
     * @return valore
     */
    @Override
    public double get(int i, int j) {
        if (i < row_min || i > row_max || j < col_min || j > col_max) {
            return 0;
        }
        if (!p.containsKey(i, j)) {
            return 0;
        }
        return (Double) p.get(i, j);
    }

    /**
     * Returns the cell's value
     *
     * @param x Coordinate della cella
     * @return
     */
    @Override
    public double get(MultiKey x) {
        return get((Integer) x.getKey(0), (Integer) x.getKey(1));
    }

    /**
     * Increments x's cell of val
     *
     * @param x Coordinate della cella
     * @param val Valore
     * @throws Throwable
     */
    @Override
    public void incr(MultiKey x, double val)  {
        set(x, get(x) + val);
    }

    /**
     * Clears cell (i,j)
     *
     * @param i Roe
     * @param j Column 
     */
    @Override
    public void rem(int i, int j) {
        p.removeAll(i, j);
    }

    /**
     * Increments of val the cell (i,j)
     *
     * @param i Row
     * @param j Column
     * @param val Value
     * @throws Throwable
     */
    @Override
    public void incr(int i, int j, double val) {
        set(i, j, get(i, j) + val);
    }

    @Override
    public void set(int i, int j, double val)  {
        if (i < 0 || j < 0) {
            return;
        }
        if (val == 0) {
            rem(i, j);
        } else {
            if (i < row_min) {
                row_min = i;
            }
            if (i > row_max) {
                row_max = i;
            }
            if (j < col_min) {
                col_min = j;
            }
            if (j > col_max) {
                col_max = j;
            }
            p.put(i, j, val);
        }

    }

    @Override
    public void set(MultiKey x, double val)  {
        set((Integer) x.getKey(0), (Integer) x.getKey(1), val);
    }

    /**
     * Returns the non-zero matrix cells
     *
     * @return
     */
    @Override
    public Set<MultiKey> getValueRange()  {
        Set s = p.keySet();
        Set<MultiKey> mks = new HashSet<>();
        for (Object x : s) {
            if (!(x instanceof MultiKey)) {
                return s;
            } else {
                mks.add((MultiKey) x);
            }
        }
        return mks;
    }

    /**
     * Removes the whole i-th row
     *
     * @param i
     */
    @Override
    public void removeRow(int i) {
        if (i >= row_min && i <= row_max) {
            p.removeAll(i);
        }
        if (i == row_min) {
            row_min++;
        }
        if (i == row_max) {
            row_max--;
        }

    }

    /**
     * Rimuove l'intera colonna j
     *
     * @param j
     */
    @Override
    public void removeCol(int j) {
        if (j >= col_min && j <= col_max) {
            for (int i = row_min; i <= row_max; i++) {
                p.removeAll(i, j);
            }
        }
        if (j == col_min) {
            row_min++;
        }
        if (j == col_max) {
            row_max--;
        }
    }

    /**
     * Rimuove riga e colonna dello stesso numero
     *
     * @param elem
     */
    @Override
    public void removeEnt(int elem) {
        removeRow(elem);
        removeCol(elem);
    }

    @Override
    public void sum(IMatrix right) {
        try {
            Set<MultiKey> val = this.getValueRange();
            val.addAll(right.getValueRange());
            for (MultiKey x:val) {
                this.incr(x, right.get(x));
            }
        } catch (Throwable ex) {
            Logger.getLogger(GMatrix.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    @Override
    public void diff(IMatrix right) {
        try {
            Set<MultiKey> val = this.getValueRange();
            val.addAll(right.getValueRange());
            for (MultiKey x:val) {
                this.incr(x, -right.get(x));
            }
        } catch (Throwable ex) {
            Logger.getLogger(GMatrix.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public int nCols() {
        return col_max+1;
    }

    @Override
    public int nRows() {
        return row_max+1;
    }

}

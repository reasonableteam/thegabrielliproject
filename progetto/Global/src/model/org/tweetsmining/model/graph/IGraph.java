/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.tweetsmining.model.graph;


/**
 *
 * @author gyankos
 */
public interface IGraph<T> {
    
    public String getName();
    public IGraph<T> rename(String new_name);
    public void setVertex(String name, T val);
    public T addVertex(String name);
    public void addVertex(String name, T val);
    public T getVertex(String name);
    void removeVertex(String name);
    public boolean addEdge(String left, String right, double value);
    public boolean addEdge(String left, String right);
    public void removeEdge(String left, String right);
    public double getEdge(String left, String right);
    public void clear();
    
}

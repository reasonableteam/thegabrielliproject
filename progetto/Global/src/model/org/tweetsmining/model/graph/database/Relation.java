/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.tweetsmining.model.graph.database;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.collections4.keyvalue.MultiKey;
import org.tweetsmining.model.matrices.GMatrix;

import com.hp.hpl.jena.rdf.model.Property;

/**
 * The general instance of a Jena Property is seen as a Matrix between the entities
 * @author gyankos
 */
public class Relation extends GMatrix implements Comparable<Relation> {
    
    private Property prop;
    private String lay;
   
    public Relation(String layer, Property prop) {
        super();
        this.lay = layer;
        this.prop = prop;
    }
    
    public String getName() {
        return lay;
    }
    
    public Relation(String layer, Property prop, GMatrix cpy) {
        this(layer,prop);
        try {
            for (MultiKey x: cpy.getValueRange()) {
                set(x,cpy.get(x));
            }
        } catch (Throwable ex) {
            Logger.getLogger(Relation.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * Returns the Jena Property
     * @return 
     */
    public Property getProperty() {
        return prop;
    }

    @Override
    public int compareTo(Relation o) {
        return this.getName().compareTo(o.getName());
    }
    
    @Override
    public String toString() {
        return lay;
    }
    
}

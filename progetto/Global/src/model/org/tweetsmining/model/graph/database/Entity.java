/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.tweetsmining.model.graph.database;

import java.io.Serializable;
import java.util.Objects;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Resource;

/**
 * Commodity class for the Jena Resources (subject/object)
 * @author gyankos
 */
public class Entity implements Comparable<Entity>, Serializable {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = 2079110959550063321L;
	private String fullURI;
    private int val;
    
    /**
     * Crea un'entità all'interno del modello
     * @param fullURI   Percorso di riferimento 
     * @param pos       Numero dell'elemento
     */
    public Entity(String fullURI, int pos) {
        this.fullURI = fullURI;
        this.val = pos;
    }
    
    public Resource toResource(Model model) {
        return model.createResource(fullURI);
    }
    
    public String getFullUri() {
        return fullURI;
    }
    
    public int getIndex() {
        return val;
    }
    
    @Override
    public String toString() {
        return getFullUri();
    }

    @Override
    public int compareTo(Entity o) {
        return toString().compareTo(o.toString());
    }
    
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Entity))
            return false;
        Entity right = (Entity)obj;
        return (compareTo(right)==0);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 71 * hash + Objects.hashCode(this.fullURI);
        return hash;
    }
    
}

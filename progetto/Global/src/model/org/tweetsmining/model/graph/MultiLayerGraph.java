package org.tweetsmining.model.graph;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.collections4.keyvalue.MultiKey;
import org.tweetsmining.model.graph.database.ERTriple;
import org.tweetsmining.model.graph.database.Entity;
import org.tweetsmining.model.graph.database.Relation;
import org.tweetsmining.model.matrices.IMatrix;

import com.hp.hpl.jena.rdf.model.Model;

public final class MultiLayerGraph  extends AbstractMultiLayerGraph {

	private static final long serialVersionUID = 280449123062820576L;
	
	public MultiLayerGraph() {
		super();
	}
	
	public MultiLayerGraph(String ser) {
		super(ser);
	}
	
	private void writeObject(ObjectOutputStream out) throws IOException {
        out.defaultWriteObject();
        System.out.println("~~~~~"+toString());
        out.writeObject(toString());
    }
 
    private void readObject(ObjectInputStream in) throws IOException,ClassNotFoundException {
        in.defaultReadObject();
        Model nMod = createModel();
        InputStream is = new ByteArrayInputStream(((String)in.readObject()).getBytes());
        
        //System.out.println("IS~~~~~"+new Scanner(is,"UTF-8").useDelimiter("\\A").next());
        
        nMod.read(is,null,AbstractMultiLayerGraph.CONVERSION_FORMAT);
        update(nMod);
    }
    
    public static MultiLayerGraph create(IMatrix m, List<String> idRows, List<String> idCols, String relUri) {
        if (m.nRows() != idRows.size())
            return null;
        if (m.nCols() != idCols.size())
            return null;
        MultiLayerGraph obj = new MultiLayerGraph();
        Relation r = obj.newLinkType(relUri);
        for (MultiKey x: m.getValueRange()) {
            String nRow = idRows.get((Integer)x.getKey(0));
            String nCol = idRows.get((Integer)x.getKey(1));
            Entity src = obj.addVertex(nRow);
            Entity dst = obj.addVertex(nCol);
            obj.addTriple(src, r,dst,1);
        }
        return obj;
    }
    
    public static MultiLayerGraph create(Collection<ERTriple> ltriple) {
        MultiLayerGraph obj = new MultiLayerGraph();
        for (ERTriple x:ltriple) {
            Entity src = obj.addVertex(x.getSource().getFullUri());
            Entity dst = obj.addVertex(x.getDestination().getFullUri());
            Relation r = obj.newLinkType(x.getRelation().getName());
            try {
                obj.addTriple(src, r, dst, 1);
            } catch (Throwable ex) {
                Logger.getLogger(AbstractMultiLayerGraph.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return obj;
    }

	

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.tweetsmining.model.graph;

import org.tweetsmining.model.matrices.IMatrix;

/**
 *
 * @author gyankos
 */
public interface IAdjacencyGraph<T> extends IGraph<T>, IMatrix {
    
}

package org.tweetsmining.io.serialization;

import java.util.Collections;
import java.util.Iterator;
import java.util.Map;

import net.sf.json.JSONObject;

import org.apache.commons.collections15.map.HashedMap;


public class JSONObjectMapSerializer<K,V> implements Serializer<Map<K,V>> {

	private final Serializer<K> kSer;
	private final Serializer<V> vSer;
	public JSONObjectMapSerializer(Serializer<K> keySer, Serializer<V> valSer) {
		this.kSer = keySer;
		this.vSer = valSer;
	}
	
	@Override
	public Map<K, V> unmarshalling(Object jsonobject) {
		if (jsonobject == null)
			return Collections.emptyMap();
		HashedMap<K,V> toret = new HashedMap<>();
		if (jsonobject instanceof JSONObject) {
			//mi si è seccata la pianta, mo alloco e restituisco.
			JSONObject obj = (JSONObject)jsonobject;
			Iterator<Object> keys = obj.keys();
			while (keys.hasNext()) {
				Object oKey = keys.next();
				K key = kSer.unmarshalling(oKey);
				V val = vSer.unmarshalling(obj.get(key));
				if (val==null) {
					System.out.println("Value not well unmarshalled");
				} else {
					//System.out.println(val);
				}
				toret.put(key,val);
			}
		} 
		return toret;
	}

	@Override
	public Object marshalling(Map<K, V> val) {
		JSONObject map = new JSONObject();
		for (K key : val.keySet()) {
			map.put(kSer.marshalling(key), vSer.marshalling(val.get(key)));
		}
		return map;
	}
}

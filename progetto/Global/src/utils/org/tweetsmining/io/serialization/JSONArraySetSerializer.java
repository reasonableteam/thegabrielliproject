package org.tweetsmining.io.serialization;

import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import net.sf.json.JSONArray;

public class JSONArraySetSerializer<K> implements Serializer<Set<K>> {

	private final Serializer<K> ser;
	public JSONArraySetSerializer(Serializer<K> kSer) {
		ser = kSer;
	}
	
	@Override
	public Set<K> unmarshalling(Object array) {
		if (array == null)
			return Collections.emptySet();
		if (!(array instanceof JSONArray)) {
			System.out.println(array.getClass().toString());
			return Collections.emptySet();
		}
			
		JSONArray ja = (JSONArray)array;
		HashSet<K> hs = new HashSet<>();
		Iterator it = ja.iterator();
		while (it.hasNext()) {
			hs.add(ser.unmarshalling(it.next()));
		}
		return hs;
	}

	@Override
	public Object marshalling(Set<K> val) {
		JSONArray n = new JSONArray();
		for (K k : val) 
			n.add(k);
		return n;
	}

}

package org.tweetsmining.io.serialization;

public class NullSerializer<K> implements Serializer<K> {

	@Override
	public K unmarshalling(Object str) {
		return (K)str;
	}
	
	@Override
	public Object marshalling(K obj) {
		return obj;
	}
}

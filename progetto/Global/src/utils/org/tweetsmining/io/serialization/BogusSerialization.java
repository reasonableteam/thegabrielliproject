package org.tweetsmining.io.serialization;

public class BogusSerialization extends StringSerialization {

	private static final long serialVersionUID = -944352491806841573L;

	public BogusSerialization() {
		super(null);
	}
	
	public BogusSerialization(String str) {
		super("");
	}

	@Override
	public String toString() {
		return "";
	}

}

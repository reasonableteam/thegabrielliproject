package org.tweetsmining.io.serialization;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import net.sf.json.JSONArray;

public class JSONArrayListSerializer<K> implements Serializer<List<K>> {

	private final Serializer<K> ser;
	public JSONArrayListSerializer(Serializer<K> kSer) {
		ser = kSer;
	}
	
	@Override
	public List<K> unmarshalling(Object array) {
		if (array == null)
			return Collections.emptyList();
		if (!(array instanceof JSONArray)) {
			System.out.println(array.getClass().toString());
			return Collections.emptyList();
		}
			
		JSONArray ja = (JSONArray)array;
		LinkedList<K> hs = new LinkedList<>();
		Iterator it = ja.iterator();
		while (it.hasNext()) {
			hs.add(ser.unmarshalling(it.next()));
		}
		return hs;
	}

	@Override
	public Object marshalling(List<K> val) {
		JSONArray n = new JSONArray();
		for (K k : val) 
			n.add(k);
		return n;
	}
}

package org.tweetsmining.io.serialization;

import java.lang.reflect.InvocationTargetException;

public class StringSerializeByReflection<K> implements Serializer<K> {
	
	private Class<K> clazz_k;
	public StringSerializeByReflection(Class<K> clazz) {
		clazz_k = clazz;
	}
	
	@SuppressWarnings("unchecked")
	public K unmarshalling(Object string) {
		if (string == null)
			return null;
		try {
			return clazz_k.getConstructor(String.class).newInstance(string);
		} catch (InstantiationException | IllegalAccessException
				| IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return (K) new BogusSerialization();
		}
	}
	
	public String marshalling(K val) {
		return val.toString();
	}
	
}

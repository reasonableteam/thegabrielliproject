package org.tweetsmining.io.serialization;

import java.lang.reflect.InvocationTargetException;

import net.sf.json.JSONObject;

public class JSONObjectSerializationByReflection<K extends JSONObjectSerialization>
		implements Serializer<K> {

	private Class<K> clazz;
	public JSONObjectSerializationByReflection(Class<K> clazz) {
		this.clazz = clazz;
	}
	
	@Override
	public K unmarshalling(Object jsonobject) {
		if (jsonobject == null)
			return null;
		try {
			return clazz.getConstructor(JSONObject.class).newInstance(jsonobject);
		} catch ( IllegalAccessException
				| IllegalArgumentException 
				| NoSuchMethodException | SecurityException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			throw new RuntimeException(e1);
		} catch (InstantiationException | InvocationTargetException e2) {
			//This case is "normal" error
			return null;
		}
	}

	@Override
	public Object marshalling(K val) {
		return val.serialize();
	}

}

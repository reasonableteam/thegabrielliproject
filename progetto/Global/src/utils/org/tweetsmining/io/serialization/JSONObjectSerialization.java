package org.tweetsmining.io.serialization;

import net.sf.json.JSONObject;

public abstract class JSONObjectSerialization {
	
	protected JSONObjectSerialization(JSONObject o) {}
	
	public abstract JSONObject serialize();
}

package org.tweetsmining.io.serialization;


public interface Serializer<K> {
	
	public K unmarshalling(Object string);
	
	public Object marshalling(K val);

}

package org.tweetsmining.io.serialization;

import java.io.Serializable;

public abstract class StringSerialization implements Serializable {

	private static final long serialVersionUID = 8680694778608914092L;

	protected StringSerialization(String str) {}
	
	@Override
	public abstract String toString();
	
	/*
	 * TODO: 
	 * 
	 * private abstract void readObject(java.io.ObjectInputStream s);
	 * private abstract void readObject(java.io.ObjectInputStream s);
	 * 
	 */
	
}

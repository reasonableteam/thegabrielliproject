package org.tweetsmining.io;

public interface Committable {
	public void commit();
}

package org.tweetsmining.persister;

public interface MarshallablePersister {

	boolean containsKey(Object marshalledkey);

	Object put(Object marshalledKey, Object marshalledValue);

}

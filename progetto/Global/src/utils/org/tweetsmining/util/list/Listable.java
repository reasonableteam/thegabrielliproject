package org.tweetsmining.util.list;

import java.util.List;


public interface Listable<T> {
	public List<T> asList();
}

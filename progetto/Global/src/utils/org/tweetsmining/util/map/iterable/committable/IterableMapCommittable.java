package org.tweetsmining.util.map.iterable.committable;


import org.tweetsmining.io.Committable;
import org.tweetsmining.util.map.iterable.IterableMap;

public interface IterableMapCommittable<K, V> extends 
//	Iterable<Entry<K,V>>, Mappable<K, V>,
	IterableMap<K, V>,
	Committable {
}

package org.tweetsmining.util.map.iterable.committable;

import org.tweetsmining.persister.MarshallablePersister;

public abstract class AbstractIterableMapCommittableSerializable<K, V> implements IterableMapCommittable<K,V> {

	private final Serializer<K> keySerializer;
	private final Serializer<V> valueSerializer;
	private MarshallablePersister persister;

	protected AbstractIterableMapCommittableSerializable(MarshallablePersister persister, Serializer<K> keySerializer, Serializer<V> valueSerializer) {
		this.persister = persister;
		this.keySerializer = keySerializer;
		this.valueSerializer = valueSerializer;		
	}
	
	@Override
	public V get(K key) {
		
		return null;
	}
	
	@Override
	public boolean containsKey(K key) {
		return persister.containsKey( keySerializer.marshalling(key) );
	}
	
	@Override
    public V put(K key, V value) {
		Object returned = persister.put(keySerializer.marshalling(key), valueSerializer.marshalling(value));
        return valueSerializer.unmarshalling(returned);
    }
}

package org.tweetsmining.util.map;

import java.util.Set;

public interface Mappable<K, T> {
	
	public boolean containsKey(K key);
	
	public int size();
	public boolean isEmpty();

	public T get(K key);
	public T put(K key, T value);
	public T remove(K key) ;

	
	public Set<K> keySet();
	public boolean containsValue(T value);
	

	
	//public Collection<T> values();
	
	
	public void clear();

}

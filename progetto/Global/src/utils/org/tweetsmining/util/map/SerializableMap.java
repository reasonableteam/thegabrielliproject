package org.tweetsmining.util.map;

import org.tweetsmining.io.serialization.Serializer;

public interface SerializableMap<K,V> {

	public void setKeySerializer(Serializer<K> clazz);
	public void setValueSerializer(Serializer<V> clazz);
	
}

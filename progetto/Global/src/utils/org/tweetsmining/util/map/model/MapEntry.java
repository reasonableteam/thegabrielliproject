/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.tweetsmining.util.map.model;

import java.util.Map;

/**
 *
 * @author gyankos
 */
public class MapEntry<K, V> implements Map.Entry<K, V> {

        private K key;
        private V val;

        public MapEntry(K k, V v) {
            this.key = k;
            this.val = v;
        }

        @Override
        public K getKey() {
            return key;
        }

        @Override
        public V getValue() {
            return val;
        }

        @Override
        public V setValue(V value) {
            V old = getValue();
            val = value;
            return old;
        }
    }


package org.tweetsmining.util.map.iterable.listable;

import org.tweetsmining.util.list.Listable;
import org.tweetsmining.util.map.iterable.IterableMap;

public interface IterableListableMap<K,V> extends IterableMap<K, V>, Listable<V> {

}

package org.tweetsmining.util.map.iterable;

import java.util.Map.Entry;

import org.tweetsmining.util.map.Mappable;

public interface IterableMap<K,T> extends Mappable<K,T>, Iterable<Entry<K,T>> {}

/*******************************************************************************
 * Copyleft (c) 2014, "Massimiliano Leone - <maximilianus@gmail.com> - https://plus.google.com/+MassimilianoLeone"
 * This file (WordsRelatorDAOImplByMap.java) is part of WordsRelator.
 * 
 *     WordsRelatorDAOImplByMap.java is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     WordsRelatorDAOImplByMap.java is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with .  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.tweetsmining.dao.engines.wordsrelator;

import java.util.HashMap;
import java.util.Map;

import org.tweetsmining.engines.wordsrelator.model.WordRelations;

public class WordsRelatorDAOImplByMap implements WordRelatorDAO {
	
	private Map<String,WordRelations> wordRelationsMap = new HashMap<>();
	
	@Override
	public boolean containsRelationsFor(String word) {
		return wordRelationsMap.containsKey(word);
	}
	
	@Override
	public WordRelations getRelationsFor(String word) {
		return wordRelationsMap.get(word);
	}
	
	@Override
	public boolean addRelationsFor(String word, WordRelations relations) {
		WordRelations putted = wordRelationsMap.put(word, relations);
		return (putted!=null);
	}
}

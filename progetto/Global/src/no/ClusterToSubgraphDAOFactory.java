package org.tweetsmining.engines.graphsminer._di.guice;

import org.tweetsmining.engines.graphsminer.dao.ClusterToSubgraphsDAO;

public interface ClusterToSubgraphDAOFactory {
	public ClusterToSubgraphsDAO create(String name, String specialization);
}

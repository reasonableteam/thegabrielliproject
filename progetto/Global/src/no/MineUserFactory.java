package org.tweetsmining.engines.graphsminer._di.guice;

import org.tweetsmining.engines.graphsminer.dao.UserToGraphDAO;
import org.tweetsmining.engines.graphsminer.mining.MineUsers;

public interface MineUserFactory {
	public MineUsers create(int iterate, int inflate, UserToGraphDAO umgd);
}

package org.tweetsmining;

import org.tweetsmining.dao.engines.tweetsparser.UserToStemmedWordsDAO;
import org.tweetsmining.persister.engines.tweetsparser.UserToStemmedWordsPersisterByCouchDB;

import com.google.inject.AbstractModule;

public class PersistanceModule extends AbstractModule {

	@Override
	protected void configure() {
		bind(UserToStemmedWordsDAO.class).to(UserToStemmedWordsPersisterByCouchDB.class);
	}

}

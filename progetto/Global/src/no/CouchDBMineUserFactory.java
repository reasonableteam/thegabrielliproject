package org.tweetsmining.engines.graphsminer._di.guice;

import javax.inject.Provider;

import org.tweetsmining.engines.graphsminer.dao.ClusterToSubgraphsDAO;
import org.tweetsmining.engines.graphsminer.dao.UserToGraphDAO;
import org.tweetsmining.engines.graphsminer.dao.WordPositionDAO;
import org.tweetsmining.engines.graphsminer.mining.MineUsers;

public class CouchDBMineUserFactory implements MineUserFactory {
	private final Provider<ClusterToSubgraphsDAO> clToSubgraphDAO;
	private final Provider<WordPositionDAO> wordToPosDAO;

	public CouchDBMineUserFactory(Provider<ClusterToSubgraphsDAO> clToSubgraphDAO,
								  Provider<WordPositionDAO> wordToPosDAO) {
		this.clToSubgraphDAO = clToSubgraphDAO;
		this.wordToPosDAO = wordToPosDAO;
	}
	
	@Override
	public MineUsers create(int iterate, int inflate, UserToGraphDAO umgd) {
		return new MineUsers(inflate, iterate, clToSubgraphDAO.get(),wordToPosDAO.get());
	}

}

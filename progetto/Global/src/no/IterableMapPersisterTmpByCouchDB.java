package org.tweetsmining.dao.mapdao.persister.couchdb;

import org.tweetsmining.io.serialization.StringSerialization;
import org.tweetsmining.persister.couchdb.IterableMapPersisterByCouchDB;
import org.tweetsmining.persister.couchdb.helper.CouchDBDocumentHelper;

public class IterableMapPersisterTmpByCouchDB<K extends StringSerialization, V extends StringSerialization> extends IterableMapPersisterByCouchDB<K, V> {

	public IterableMapPersisterTmpByCouchDB(CouchDBDocumentHelper couchDBDocumentHelper) {
		super(couchDBDocumentHelper);
	}

}

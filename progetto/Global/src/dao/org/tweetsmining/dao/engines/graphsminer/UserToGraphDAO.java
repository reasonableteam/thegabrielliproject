package org.tweetsmining.dao.engines.graphsminer;


import org.tweetsmining.dao.base.iterable.listable.IterableListableMapDAO;
import org.tweetsmining.model.graph.MultiLayerGraph;
import org.tweetsmining.model.user.User;

public interface UserToGraphDAO 
//extends IterableListableMap<User, MultiLayerGraph> {
	extends IterableListableMapDAO<User, MultiLayerGraph> {
}


package org.tweetsmining.dao.engines.wordsrelator;

import java.util.List;

import org.tweetsmining.dao.base.iterable.IterableMapDAO;
import org.tweetsmining.model.user.User;
import org.tweetsmining.model.wordsrelator.WordRelations;

public interface UserToRelatedWordsDAO extends IterableMapDAO<User, List<WordRelations>> {}

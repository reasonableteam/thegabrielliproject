package org.tweetsmining.dao.engines.graphsminer;

import java.util.List;

import org.tweetsmining.dao.base.iterable.listable.IterableListableMapDAO;
import org.tweetsmining.model.graph.MultiLayerGraph;
import org.tweetsmining.model.graphsminer.Cluster;

public interface ClusterToSubgraphsDAO 
//extends IterableListableMap<Cluster, List<MultiLayerGraph>>
extends IterableListableMapDAO<Cluster, List<MultiLayerGraph>>
{}

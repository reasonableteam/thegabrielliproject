package org.tweetsmining.dao.engines.graphsminer;

import org.tweetsmining.dao.base.iterable.listable.IterableListableMapDAO;


public interface WordPositionDAO extends 
//IterableListableMap<String, Integer> {
	IterableListableMapDAO<String, Integer> {

}

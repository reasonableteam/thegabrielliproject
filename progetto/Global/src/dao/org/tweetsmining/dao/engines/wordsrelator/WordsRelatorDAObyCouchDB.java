/*******************************************************************************
 * Copyleft (c) 2014, "Massimiliano Leone - <maximilianus@gmail.com> - https://plus.google.com/+MassimilianoLeone"
 * This file (WordsRelatorDAObyCouchDB.java) is part of WordsRelator.
 * 
 *     WordsRelatorDAObyCouchDB.java is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     WordsRelatorDAObyCouchDB.java is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with .  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.tweetsmining.dao.engines.wordsrelator;

import javax.inject.Inject;

import org.tweetsmining.engines.wordsrelator.model.WordRelations;
import org.tweetsmining.persister.couchdb.IterableMapPersisterByCouchDB;

public class WordsRelatorDAObyCouchDB implements WordRelatorDAO {
	
	private IterableMapPersisterByCouchDB<String, WordRelations> couchDBMap;
	
	@Inject
	public WordsRelatorDAObyCouchDB(IterableMapPersisterByCouchDB<String, WordRelations> couchDBMap) {
		this.couchDBMap = couchDBMap;
	}

	@Override
	public boolean containsRelationsFor(String word) {
		return couchDBMap.containsKey(word);
	}

	@Override
	public WordRelations getRelationsFor(String word) {
		return couchDBMap.get(word);
	}

	@Override
	public boolean addRelationsFor(String word, WordRelations wordRelations) {
		return (couchDBMap.put(word, wordRelations)!=null)?true:false;
	}
}

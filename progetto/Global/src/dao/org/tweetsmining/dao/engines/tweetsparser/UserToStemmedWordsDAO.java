package org.tweetsmining.dao.engines.tweetsparser;

import java.util.Set;

import org.tweetsmining.dao.base.iterable.IterableMapDAO;
import org.tweetsmining.model.user.User;

public interface UserToStemmedWordsDAO 
//extends IterableMap<User, Set<String>>
extends IterableMapDAO<User, Set<String>>
{

}

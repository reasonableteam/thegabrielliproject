/*******************************************************************************
 * Copyleft (c) 2014, "Massimiliano Leone - <maximilianus@gmail.com> - https://plus.google.com/+MassimilianoLeone"
 * This file (PersisterDAO.java) is part of Persister.
 * 
 *     PersisterDAO.java is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     PersisterDAO.java is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with .  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.tweetsmining.dao.base;

import org.tweetsmining.io.Committable;


public interface DAO<K, V, R> extends Committable {

	public R put(K key, V value);
	public R remove(K key);
//	public R update(K key, V value);
	public V get(K key);
//	public Iterator<Entry<K,V>> iterator();
	public void commit();
}

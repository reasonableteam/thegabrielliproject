package org.tweetsmining.dao.base.iterable;

import org.tweetsmining.dao.base.DAO;
import org.tweetsmining.util.map.iterable.IterableMap;

public interface IterableMapDAO<K,V> extends DAO<K,V,V>, IterableMap<K,V> {}

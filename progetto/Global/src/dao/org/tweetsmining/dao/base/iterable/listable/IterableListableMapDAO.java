package org.tweetsmining.dao.base.iterable.listable;

import org.tweetsmining.dao.base.iterable.IterableMapDAO;
import org.tweetsmining.util.list.Listable;

public interface IterableListableMapDAO<K, V> extends IterableMapDAO<K, V>, Listable<V>{}

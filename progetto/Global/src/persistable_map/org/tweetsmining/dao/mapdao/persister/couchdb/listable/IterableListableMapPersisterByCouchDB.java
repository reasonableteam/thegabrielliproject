package org.tweetsmining.dao.mapdao.persister.couchdb.listable;

import java.util.List;

import org.tweetsmining.dao.mapdao.persister.couchdb.IterableMapPersisterByCouchDB;
import org.tweetsmining.persister.couchdb.CouchDBDocumentHelper;
import org.tweetsmining.persister.couchdb.CouchDBDocumentHelperRandomable;
import org.tweetsmining.util.map.iterable.listable.IterableListableMap;

public class IterableListableMapPersisterByCouchDB<K,V> extends IterableMapPersisterByCouchDB<K, V> 
	implements IterableListableMap<K, V> {

	//private CouchDBDocumentHelperRandomable couchDBDocumentHelperRandomable;
	private CouchDBDocumentHelper couchDBDocumentHelper;

	public IterableListableMapPersisterByCouchDB(CouchDBDocumentHelper couchDBDocumentHelper) {
		super(couchDBDocumentHelper);
		this.couchDBDocumentHelper = couchDBDocumentHelper;
		//this.couchDBDocumentHelperRandomable = couchDBDocumentHelperRandomable;
	}

	@Override
	public List<V> asList() {
		CouchDBDocumentHelperRandomable newdocument = new CouchDBDocumentHelperRandomable(couchDBDocumentHelper.getProvider());
		IterableMapPersisterByCouchDB<Integer, V> couchDBMap = new IterableMapPersisterByCouchDB<Integer,V>(newdocument);
		LinkedListPersistableByCouchDB<V> listViewOverMap = new LinkedListPersistableByCouchDB<V>(couchDBMap, newdocument);
		return listViewOverMap;
	}

}

package org.tweetsmining.config._di.guice;

import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.tweetsmining.config.Config;
import org.tweetsmining.engines.wordsrelator.printer.ConsolePrinter;
import org.tweetsmining.engines.wordsrelator.printer.Printer;

import com.google.inject.AbstractModule;
import com.google.inject.name.Names;

public class ConfigModule extends AbstractModule {

	/*
	 * here, we obtain: 
	 * --
	 * corpus= 
	 * persistenceType=
	 * couchdb.host= 
	 * couchdb.port=
	 * 
	 * stemmer.resources_dir=
	 * 
	 * miner.iterate=
	 * miner.inflate=
	 * --
	 */

	@Override
	protected void configure() {
		Properties properties = new Properties();
		try {
			properties.load(new FileReader(Config.CONFIG_FILE));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		Names.bindProperties(binder(), properties);
		
		bind(Printer.class).to(ConsolePrinter.class);
	}
}

package org.tweetsmining.persister.engines.graphminer.graphgenerator._di.guice.module;

import org.tweetsmining.dao.engines.graphsminer.WordPositionDAO;
import org.tweetsmining.engines.graphsminer.dao.couchdb.WordPositionCouchDB;

import com.google.inject.AbstractModule;

public class MinerPersisterModule extends AbstractModule {

	@Override
	protected void configure() {
		// TODO Auto-generated method stub

//		bind(UserToGraphDAO.class).to(UserToGraphCouchDB.class);
//		bind(ClusterToSubgraphsDAO.class).to(ClusterToSubgraphsCouchDB.class);
		bind(WordPositionDAO.class).to(WordPositionCouchDB.class);
	}

}

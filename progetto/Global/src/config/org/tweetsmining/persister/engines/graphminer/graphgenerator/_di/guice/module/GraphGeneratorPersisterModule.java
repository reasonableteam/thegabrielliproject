package org.tweetsmining.persister.engines.graphminer.graphgenerator._di.guice.module;

import org.tweetsmining.dao.engines.graphsminer.UserToGraphDAO;
import org.tweetsmining.engines.graphsminer.dao.couchdb.UserToGraphCouchDB;

import com.google.inject.AbstractModule;

public class GraphGeneratorPersisterModule extends AbstractModule {

	@Override
	protected void configure() {
		bind(UserToGraphDAO.class).to(UserToGraphCouchDB.class);
	}

}

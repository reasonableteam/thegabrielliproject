
package org.tweetsmining.persister.engines.graphminer.graphgenerator._di.guice.module;

import org.tweetsmining.dao.engines.graphsminer.ClusterToSubgraphsDAO;
import org.tweetsmining.engines.graphsminer.dao.couchdb.ClusterToSubgraphsCouchDB;

import com.google.inject.AbstractModule;

public class ClustersPersisterModule extends AbstractModule {

	@Override
	protected void configure() {
//		bind(ClusterToSubgraphsDAO.class).to(ClusterToSubgraphsCouchDB.class);
//		bind(WordPositionDAO.class).to(WordPositionCouchDB.class);
		bind(ClusterToSubgraphsDAO.class).to(ClusterToSubgraphsCouchDB.class);
	}

}

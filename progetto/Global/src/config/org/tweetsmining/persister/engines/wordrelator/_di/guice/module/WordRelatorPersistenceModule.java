package org.tweetsmining.persister.engines.wordrelator._di.guice.module;

import org.tweetsmining.dao.engines.wordsrelator.UserToRelatedWordsDAO;
import org.tweetsmining.dao.engines.wordsrelator.WordRelatorDAO;
import org.tweetsmining.persister.engines.wordrelator.couchdb.UserToRelatedWordsPersisterByCouchDB;
import org.tweetsmining.persister.engines.wordrelator.couchdb.WordRelatorPersisterByCouchDB;

import com.google.inject.AbstractModule;

public class WordRelatorPersistenceModule extends AbstractModule {

	@Override
	protected void configure() {
		bind(UserToRelatedWordsDAO.class).to(UserToRelatedWordsPersisterByCouchDB.class);
		bind(WordRelatorDAO.class).to(WordRelatorPersisterByCouchDB.class);
	}
}

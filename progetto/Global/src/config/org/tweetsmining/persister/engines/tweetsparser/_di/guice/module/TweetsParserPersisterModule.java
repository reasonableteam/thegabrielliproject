package org.tweetsmining.persister.engines.tweetsparser._di.guice.module;

import org.tweetsmining.dao.engines.tweetsparser.UserToStemmedWordsDAO;
import org.tweetsmining.persister.engines.tweetsparser.couchdb.UserToStemmedWordsPersisterByCouchDB;

import com.google.inject.AbstractModule;

public class TweetsParserPersisterModule extends AbstractModule {

	@Override
	protected void configure() {
		// we use couchdb
		bind(UserToStemmedWordsDAO.class).to(UserToStemmedWordsPersisterByCouchDB.class);
	}
}

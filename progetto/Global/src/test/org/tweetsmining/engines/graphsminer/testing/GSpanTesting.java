/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.tweetsmining.engines.graphsminer.testing;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.tweetsmining.engines.graphsminer.Database.Word;
import org.tweetsmining.engines.graphsminer.graphs.gSpan.gSpanGraph;
import org.tweetsmining.model.graph.IAdjacencyGraph;
import org.tweetsmining.model.graph.MultiLayerGraph;
import org.tweetsmining.model.graph.database.Entity;

/**
 *
 * @author gyankos
 */
public class GSpanTesting {
    
    private gSpanGraph first;
    private gSpanGraph second;
    private gSpanGraph third;
    private static final String LAYER1 = "http://first.lay/er";
    private List<MultiLayerGraph> db;
    
    public GSpanTesting() {
        //Setting vertices' names
        String vSet[] = {"A","B","C","D","E","F","G"};
        //Setting their urls
        for (int i =0; i<7; i++) 
            vSet[i] = Word.getWordUrl(vSet[i]);
        
        //Creating the first graph
        MultiLayerGraph f = new MultiLayerGraph();
        for (String v : vSet) 
            f.addVertex(v);
        IAdjacencyGraph<Entity> f_l1 = f.newLayer(LAYER1);
        for (int i=0; i<6; i++) {
            f_l1.addEdge(vSet[i], vSet[i+1], 1);
        }
        
        f_l1.addEdge(vSet[6], vSet[0], 1);
        
        if (f.getEdge(vSet[0], vSet[1])==0) {
            throw new RuntimeException("f - First edge not added");
        }
        
        //Creating the second graph
        MultiLayerGraph s = new MultiLayerGraph();
        for (int i=0; i<=4; i++)
            s.addVertex(vSet[i]);
        IAdjacencyGraph<Entity> s_l1 = s.newLayer(LAYER1);
        for (int i=0; i<4; i++)
            s_l1.addEdge(vSet[i], vSet[i+1], 1);
        
        if (s.getEdge(vSet[0], vSet[1])==0) {
            throw new RuntimeException("s - First edge not added");
        }
            
        //Creating the thrid graph
        MultiLayerGraph t = new MultiLayerGraph();
        for (int i=3; i<7; i++)
            t.addVertex(vSet[i]);
        IAdjacencyGraph<Entity> t_l1 = t.newLayer(LAYER1);
        t_l1.addEdge(vSet[0], vSet[1], 1);
        t_l1.addEdge(vSet[3], vSet[4], 1);
        t_l1.addEdge(vSet[3], vSet[6], 1);
        
        if (t.getEdge(vSet[0], vSet[1])!=0) {
            throw new RuntimeException("t - First edge added");
        }
        
        db = new LinkedList<>();
        db.add(f);
        db.add(s);
        db.add(t);
        
        
    }
    
    public void testGraphPruning() {
        Collection<MultiLayerGraph> tmp;
        //I try to mantain all the vertices as possible
//        tmp = GraphPruning.getVerticesAboveMode(db);
        
        /*for (MultiLayerGraph x: cl) {
            System.out.println(Arrays.toString(x.getVerticesURI().toArray()));
        }*/
        
        //Removes the edges by its link-strength
        //tmp = GraphPruning.strongestEdges(tmp, 1);
        
        System.out.println("-----------------------------------------");
        /*for (MultiLayerGraph x:tmp) {
            gSpanGraph xt = new gSpanGraph(x);
            System.out.println("-"+Arrays.toString(xt.getSimpleDFSCode().toArray())+"-");
        }*/
        //Ok: all the edges have weight 1, and so all the edges are kept
        
        //Test completed successfully
    }

    /*
    void test_gSpan() {
        Collection<gSpanGraph> tmp = Collections2.transform(db, new Function<MultiLayerGraph,gSpanGraph>() {

            @Override
            public gSpanGraph apply(MultiLayerGraph input) {
                return new gSpanGraph(input);
            }
        });
        
        Abstract_gSpan getSubgraphs = gSpanFactory.create(false, db);
        Collection<ERTriple> ers = getSubgraphs.getMostFrequentEdges(0.55);
        if (ers.isEmpty()) {
            throw new RuntimeException("ERROR: no edge is in the way");
        }
        
       
        for (Object x:getSubgraphs.getMostFrequentEdges(0.55))  {
            System.out.println(x);
        }
        
        // Heaving testing: bypassing all the other methods.
        // https://www.youtube.com/watch?v=Tp8tegq1DlU
        for (Object x:getSubgraphs.gSpan(0.55, 5)) {
            System.out.println(Arrays.toString(((IgSpanGraphs<GSCode>)x).getACode().toArray()));
        }
        
    }
    
    void test_ExtededgSpan() {
        
        Abstract_gSpan getSubgraphs = gSpanFactory.create(true, db);
        Collection<ERTriple> ers = getSubgraphs.getMostFrequentEdges(0.55);
        if (ers.isEmpty()) {
            throw new RuntimeException("ERROR: no edge is in the way");
        }
        for (Object x:getSubgraphs.getMostFrequentEdges(0.55))  {
            System.out.println(x);
        }
        
        for (Object x:getSubgraphs.gSpan(0, 5)) {
            System.out.println(Arrays.toString(((IgSpanGraphs<ExtendedGSCode>)x).getACode().toArray()));
        }
        
    }
    */
}

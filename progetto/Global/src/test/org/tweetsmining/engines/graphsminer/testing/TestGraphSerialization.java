package org.tweetsmining.engines.graphsminer.testing;

import java.util.Map.Entry;

import org.tweetsmining.dao.mapdao.mappedfile.MappedFile;
import org.tweetsmining.engines.graphsminer.Database.Word;
import org.tweetsmining.model.graph.MultiLayerGraph;
import org.tweetsmining.model.graph.database.Entity;
import org.tweetsmining.model.graph.database.Relation;

public class TestGraphSerialization {

	public static void main(String s[]) {
		
		MultiLayerGraph mlg1 = new MultiLayerGraph();
		Relation rel1 = mlg1.newLinkType("http://my.personal/org#rela");
		Relation rel2 = mlg1.newLinkType("http://my.personal/org#relb");
		Entity viva = mlg1.addVertex(Word.getWordUrl("Viva"));
		Entity la = mlg1.addVertex(Word.getWordUrl("la"));
		Entity pappa = mlg1.addVertex(Word.getWordUrl("pappa"));
		Entity col = mlg1.addVertex(Word.getWordUrl("col"));
		Entity pomo = mlg1.addVertex(Word.getWordUrl("pomodoro"));
		mlg1.addTriple(la, rel1, pappa, 1);
		mlg1.addTriple(col,rel1,pomo,1);
		mlg1.addTriple(viva, rel2, la, 1);
		mlg1.addTriple(pappa,rel2,col,2);
		String str = mlg1.toString();
		System.out.println("Expected Output: " + str);
		
		{
			MappedFile<String,MultiLayerGraph> mf = new MappedFile<>("/home/gyankos/vlfoca");
			mf.put("test", mlg1);
			mf.commit();
		}
		
		
		MappedFile<String,MultiLayerGraph> check = new MappedFile<>("/home/gyankos/vlfoca");
		for (Entry<String, MultiLayerGraph> x:check) {
			System.out.println("\n~~~~~~~~~~~~\nGiven Output: " + x.getValue().toString());
		}
		
		
	}
	
}

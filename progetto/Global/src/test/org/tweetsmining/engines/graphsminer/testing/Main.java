/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.tweetsmining.engines.graphsminer.testing;


import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.collections4.keyvalue.MultiKey;
import org.tweetsmining.engines.graphsminer.Database.Word;
import org.tweetsmining.engines.graphsminer.gmatrix.utils.gSpanFactory;
import org.tweetsmining.engines.graphsminer.graphs.DFS.AbstractDBVisit;
import org.tweetsmining.engines.graphsminer.graphs.DFS.DFS;
import org.tweetsmining.engines.graphsminer.graphs.gSpan.GSCode;
import org.tweetsmining.engines.graphsminer.graphs.gSpan.SimpleSubgraphOf;
import org.tweetsmining.engines.graphsminer.graphs.gSpanExtended.ExtendedGSCode;
import org.tweetsmining.engines.graphsminer.graphs.gSpanExtended.ExtendedSubgraphOf;
import org.tweetsmining.engines.graphsminer.matrices.MatrixOp;
import org.tweetsmining.model.graph.AbstractMultiLayerGraph;
import org.tweetsmining.model.graph.IAdjacencyGraph;
import org.tweetsmining.model.graph.IGraph;
import org.tweetsmining.model.graph.MultiLayerGraph;
import org.tweetsmining.model.graph.database.ERTriple;
import org.tweetsmining.model.graph.database.Entity;
import org.tweetsmining.model.graph.database.Relation;
import org.tweetsmining.model.matrices.GMatrix;
import org.tweetsmining.model.matrices.IMatrix;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Selector;
import com.hp.hpl.jena.rdf.model.SimpleSelector;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;

/**
 *
 * @author gyankos
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    
    static void testProd() throws Throwable {
        GMatrix g1 = new GMatrix();
        GMatrix g2 = new GMatrix();
        int i = 1;
        int a = 0;
        int b = 0;
        for (a=0; a<2; a++)
            for (b=0; b<3; b++) 
                g1.set(a,b,i++);
        for (a=0; a<3; a++)
            for (b=0; b<2; b++) 
                g2.set(a,b,i++);
        GMatrix g3 = (GMatrix)MatrixOp.prod(g1, g2);
        for (MultiKey x: g3.getValueRange()) {
            System.out.println(x.toString()+" "+g3.get(x));
        }  
    }
    
    static void testRem() throws Throwable {
        GMatrix g = new GMatrix();
      g.set(1,1,10.2);
      g.set(5,5,50);
      g.removeRow(3);
      g.removeCol(3);
      for (MultiKey x: g.getValueRange()) {
            System.out.println(x.toString()+" "+g.get(x));
      } 
    }
    
    public void testValue() {
        //Testing PropertyGraph 
        
        //Creating Pellet Module
        Model m = AbstractMultiLayerGraph.createModel();
        //Creating Module with initial properties and nodes
        // TODO remove null
        MultiLayerGraph mmg = new MultiLayerGraph(null);
        //Creating a new layer
        IGraph<Entity> wifeOfLayer = mmg.newLayer("http://my.new/full#wifeOf");
        
        Entity mum = wifeOfLayer.addVertex("http://is.a/onto/mum");
        if (mum==null) {
            System.err.println("Error: null mum");
            return;
        }
        
        Entity error = wifeOfLayer.addVertex(null);
        if (error!=null) {
            System.err.println("Error: not null entity");
            return;
        }
        
        Entity dad = wifeOfLayer.addVertex("http://is.a/onto/dad");
        if (mum.getIndex()+1!=dad.getIndex()) {
            System.err.println("Error: not increasing indexes");
            return;
        }
        error = wifeOfLayer.addVertex("http://is.a/onto/mum");
        if (error.getIndex()!=mum.getIndex()) {
            System.err.println("Error: not equal indices (generating new entities)");
            return;
        }
        if (!error.getFullUri().equals(mum.getFullUri())) {
            System.err.println("Error: WTF!!!");
            return;
        }
        
        if (mmg.getVertex(mum.getFullUri())==null) {
            System.err.println("Error: mmg and layer don't communicate (1 - null)");
            return;
        }
        if (mmg.getVertex(mum.getFullUri()).getIndex()!=mum.getIndex()) {
            System.err.println("Error: mmg and layer don't communicate (2 - index)");
            return;
        }
        
        //Creating a relation out of the games
        Model m2 = AbstractMultiLayerGraph.createModel();
        
        Property wifeOf = m2.createProperty("http://my.new/full#wifeOf");
        Property husbandOf = m2.createProperty("http://my.new/full#husbandOf");
        
        Statement mwifed = m2.createStatement(mum.toResource(m2), wifeOf, dad.toResource(m2));
        Statement dhusbm = m2.createStatement(dad.toResource(m2),husbandOf, mum.toResource(m2) );
        
        m2.add(mwifed);
        m2.add(dhusbm);
        
        if (mmg.getEdge(mum, dad)!=0) {
            System.err.println("Error: it has to be zero (1)");
            return;
        }
        
        
        mmg.union(m2);
        mmg.union(m2);//Union doesn't affect it all
        Selector s = new SimpleSelector(mum.toResource(m2),wifeOf,dad.toResource(m2));
        StmtIterator r = mmg.getModel().listStatements(s);
        for (Statement x:r.toList()) {
            System.out.println(x.toString());
        }
        
        if (wifeOfLayer.getEdge(dad.getFullUri(),mum.getFullUri())!=0) {
            System.err.println("Error: it has to be zero (2)");
            return;
        }
        
        wifeOfLayer.removeEdge(dad.getFullUri(),mum.getFullUri());
        if (mmg.getEdge(mum, dad)==0) {
            System.err.println("Error: it has to be 0.5 (1)");
            return;
        }
        if (mmg.getEdge(dad, mum)==0) {
            System.err.println("Error: it has to be 0.5 (2)");
            return;
        }
        
        mmg.removeEdge(dad.getFullUri(),mum.getFullUri());
        if (mmg.getEdge(dad,mum)!=0) {
            System.err.println("Error: it has to be zero (3)");
            return;
        }
        
        return;
    }
    
    public static void testTranspose() throws Throwable {
                GMatrix g1 = new GMatrix();
        IMatrix g2;
        int i = 1;
        int a = 0;
        int b = 0;
        for (a=0; a<2; a++)
            for (b=0; b<3; b++) 
                g1.set(a,b,i++);
        g2 = MatrixOp.transpose(g1);
        for (MultiKey x: g1.getValueRange()) {
            System.out.println(x.toString()+" "+g1.get(x));
        }  
        System.out.println("\n\n");
        for (MultiKey x: g2.getValueRange()) {
            System.out.println(x.toString()+" "+g2.get(x));
        }  
    }
    
    public static void testIntPair() throws Throwable {
        
        //Checking Tree Map
        int i;
        int j;
        int k=1;
        
       // TODO remove null
       MultiLayerGraph mlg = new MultiLayerGraph(null);
       SecureRandom random = new SecureRandom();
       
       IAdjacencyGraph<Entity> e = mlg.newLayer("http://my.new/rel/rel");
       for (i=0; i<5; i++)
           if (e.addVertex("http://my.rand/uri#"+(new BigInteger(130, random).toString(32)))==null) {
               throw new Throwable("ERROR: not adding vertex");
           }
       for (i=0; i<5; i++)
            for (j=0; j<=i; j++) {
                try {
                    e.set(i, j, k++);
                } catch (Throwable ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                    return;
                }
            }
       //GraphPruning<Entity> algorithm = new GraphPruning<>();
       /*for (String x:algorithm.getCode(e).getCode()) {
           System.out.println(x);
       }*/
       
    }
    
    public static void testDFS() {
        
    	MultiLayerGraph mmg = new MultiLayerGraph();
         AbstractDBVisit<List<String>> e = new AbstractDBVisit<List<String>>() {

             private List<String> dfsCode = new LinkedList<>();
             
             @Override
             public void visitVertex(Entity v) {
             }

             @Override
             public void visitEdge(Entity src, Relation l, Entity dst) {
                 dfsCode.add(src.getFullUri());
                 dfsCode.add(l.getName());
                 dfsCode.add(dst.getFullUri());
                 System.out.println(src.getFullUri()+" "+l.getName()+" "+dst.getFullUri());
             }

             @Override
             public List<String> getOutput() {
               return dfsCode;
             }

             @Override
             public Collection<ERTriple> getEdgeVisitOrder(SortedSet<ERTriple> cert) {
                 return cert;
             }
         };
         String names[] = {"A","B","C","D","E"};
         for (String x : names) 
             mmg.addVertex(Word.WORD_URI+x);
         IGraph<Entity> ipo = mmg.newLayer("http://is.prev/of");
         ipo.addEdge(Word.WORD_URI+"A",Word.WORD_URI+"B",1);
         ipo.addEdge(Word.WORD_URI+"B",Word.WORD_URI+"C",1);
         ipo.addEdge(Word.WORD_URI+"C",Word.WORD_URI+"D",1);
         ipo.addEdge(Word.WORD_URI+"D",Word.WORD_URI+"E",1);
         IGraph<Entity> voc = mmg.newLayer("http://is.a/vocal");
         voc.addEdge(Word.WORD_URI+"A", Word.WORD_URI+"E", 1);
         IGraph<Entity> cons = mmg.newLayer("http://is.a/consonant");
         cons.addEdge(Word.WORD_URI+"B", Word.WORD_URI+"C", 1);
         cons.addEdge(Word.WORD_URI+"C", Word.WORD_URI+"D", 1);
         
         DFS<List<String>> d = new DFS<>(mmg,e);
         /*for (String x:d.start()) {
             System.out.println(x);
         }*/
         
         ERTriple a = new ERTriple(mmg.getVertex(0), mmg.getRelation("http://is.prev/of"), mmg.getVertex(1));
         ERTriple b = new ERTriple(mmg.getVertex(Word.WORD_URI+"A"), mmg.getRelation("http://is.prev/of"), mmg.getVertex(1));
         
         HashMap<ERTriple,Integer> em = new HashMap<>();
         em.put(a, 1);
         if (!em.containsKey(b)) {
             System.err.println("Error on map");
         }
         
         System.out.println("-------------------------------------------------");
         //Testing subgraphs
         if (!gSpanFactory.subgraphOf(false, mmg, mmg)) {
             System.err.println("Error on ID");
             return;
         }
         
         /////////////////////////////////////////////////////////////////////
         MultiLayerGraph mm2 = new MultiLayerGraph();
         for (String x : names) 
             mm2.addVertex(Word.WORD_URI+x);
         IGraph<Entity> ipo2 = mm2.newLayer("http://is.prev/of");
         ipo2.addEdge(Word.WORD_URI+"A",Word.WORD_URI+"B",1);
         ipo2.addEdge(Word.WORD_URI+"B",Word.WORD_URI+"C",1);
         System.out.println("-------------------------------------------------");
         if (!gSpanFactory.subgraphOf(false,mm2, mmg)) {
             System.out.println("Error on Subgraph");
             return;
         }
         
         
         /////////////////////////////////////////////////////////////////////
         MultiLayerGraph mm3 = new MultiLayerGraph();
         for (String x : names) 
             mm3.addVertex(Word.WORD_URI+x);
         IGraph<Entity> ipo3 = mm3.newLayer("http://is.prev/of");
         ipo3.addEdge(Word.WORD_URI+"A",Word.WORD_URI+"B",1);
         ipo3.addEdge(Word.WORD_URI+"B",Word.WORD_URI+"E",1);
         System.out.println("-------------------------------------------------");
         if (gSpanFactory.subgraphOf(false,mm3, mmg)) {
             System.out.println("Error on False Subgraph");
             return;
         }
         
    }
    
    public static void eGSpanCodeTesting() {
        
        GSCode left = new GSCode(ERTriple.easyMakeTriple("B", "r", "C"),
                                ERTriple.easyMakeTriple("C", "r", "D"));
        
        GSCode right = new GSCode(ERTriple.easyMakeTriple("A", "r", "B"),
                                ERTriple.easyMakeTriple("B", "r", "C"),
                                ERTriple.easyMakeTriple("C", "r", "D"));
        
        SimpleSubgraphOf sso = SimpleSubgraphOf.getInstance();
        
        if (!sso.subgraphOf(right, right)) {
            throw new RuntimeException("noid");
        }
        
        if (!sso.subgraphOf(left, right)) {
            throw new RuntimeException("left is not subgraph");
        }
        
        if (sso.subgraphOf(right, left)) {
            throw new RuntimeException("Wrong un'");
        }
        
        ///////////////////////////////////////////////////////////
        ExtendedGSCode eleft = new ExtendedGSCode(left, null);
        ExtendedGSCode erigh = new ExtendedGSCode(right, null);
        
        ExtendedSubgraphOf eso = ExtendedSubgraphOf.getInstance();
        if (!eso.subgraphOf(eleft, erigh)) {
            throw new RuntimeException("Wrong un' (2)");
        }
        
        Set<GSCode> sgs = new HashSet<>();
        GSCode gs1 = new GSCode(ERTriple.easyMakeTriple("Y", "r", "K"),
                            ERTriple.easyMakeTriple("N", "r", "O"));
        sgs.add(gs1);
        erigh.setSecond(sgs);

        if (!eso.subgraphOf(eleft, erigh)) {
            throw new RuntimeException("Wrong un' (3)");
        }
        if (eso.subgraphOf(erigh,eleft)) {
            throw new RuntimeException("Wrong un' (4)");
        }
        
        
        
        
        
        Set<GSCode> sgs2 = new HashSet<>();
        GSCode gs2 = new GSCode(ERTriple.easyMakeTriple("Z", "r", "K"),
                            ERTriple.easyMakeTriple("W", "r", "E"));
        sgs2.add(gs2);
        eleft.setSecond(sgs2);
        if (eso.subgraphOf(eleft, erigh)) {
            throw new RuntimeException("Wrong un' (5)");
        }
        if (eso.subgraphOf(erigh,eleft)) {
            throw new RuntimeException("Wrong un' (6)");
        }
        
      
        sgs.add(gs2);
        sgs2.add(gs1);
        eleft.setSecond(sgs2);
        erigh.setSecond(sgs);
        
        if (!eso.subgraphOf(eleft, erigh)) {
            throw new RuntimeException("Wrong un' (7)");
        }
        if (eso.subgraphOf(erigh,eleft)) {
            throw new RuntimeException("Wrong un' (8)");
        }
        
        eleft.setFirst(right);//Compare graphs with codes in the correct order
        
        if (!eso.subgraphOf(erigh,eleft)) {
            throw new RuntimeException("Wrong un' (9)");
        }
        
        //left = eleft.getFirst();
        right = new GSCode(ERTriple.easyMakeTriple("A", "r", "B"),
                                ERTriple.easyMakeTriple("B", "r", "C"),
                                ERTriple.easyMakeTriple("C", "r", "D"));
        left =  new GSCode(ERTriple.easyMakeTriple("A", "r", "B"),
                                ERTriple.easyMakeTriple("B", "r", "C"),
                                ERTriple.easyMakeTriple("C", "r", "D"));
        sgs2 = new HashSet<>();
        sgs2.add(new GSCode(ERTriple.easyMakeTriple("G", "r", "H"), 
                            ERTriple.easyMakeTriple("H", "r", "I")));
        sgs2.add(new GSCode(ERTriple.easyMakeTriple("J", "r", "K")));
        sgs = new HashSet<>();
        sgs.add(new GSCode(ERTriple.easyMakeTriple("H", "r", "I")));
        erigh  = new ExtendedGSCode(right, sgs2);
        eleft = new ExtendedGSCode(left, sgs);
        //System.exit(2);
       
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        if (eso.subgraphOf(erigh,eleft)) {
            throw new RuntimeException("Wrong un' (10)");
        }
        if (!eso.subgraphOf(eleft,erigh)) {
            throw new RuntimeException("Wrong un' (11)");
        }
        
        sgs.add(new GSCode(ERTriple.easyMakeTriple("W", "r", "X")));
        eleft.setSecond(sgs);
        
        //WrX is not in right, hence left is not a subgraph of right
        if (eso.subgraphOf(eleft,erigh)) {
            throw new RuntimeException("Wrong un' (12)");
        }
        if (eso.subgraphOf(erigh,eleft)) {
            throw new RuntimeException("Wrong un' (13)");
        }
        
        System.out.println("Ok!");
    }
    
    public static void main(String[] args) throws Throwable {
        
        //MultiKey mk = new MultiKey(1, 2);
        //MultiKey mk2 = new MultiKey(1,3);
        //System.out.println(mk.equals(mk2));
                
        //SubgraphE<Character> algo = new SubgraphE<>();
        //String sub = "defglm";
        //boolean ret = algo.compare(new LinkedList<Character>(Chars.asList("defgklm".toCharArray())), Chars.asList("abcdefaztuvkgluwztm".toCharArray()));
        //System.out.println(ret);
        
        testDFS();//Error on false subgraph
        eGSpanCodeTesting();
        
        //eGSpanCodeTesting();
        //eGSpanCodeTesting();
        //testIntPair();
        //testDFS();
        
        /*
        SortedSet<ERTriple> e = new TreeSet<>();
        e.add(ERTriple.makeTest("A", "http://is.prev/of", "E"));
        e.add(ERTriple.makeTest("A", "http://is.prev/of", "B")); 
        for (ERTriple x : e) {
            System.out.println(x);
        }
                */
        //GSpanTesting gst = new GSpanTesting();
        //gst.testGraphPruning();
        //gst.test_gSpan();
        //gst.test_ExtededgSpan();
        //MarkovChain.main(null);
        //double d[] = new double[5];
        //System.out.println(d[3]);
        
        
        /*HashMap<Integer,LinkedList<String>> hm = new HashMap<>();
        hm.put(1, new LinkedList<String>());
        hm.get(1).add("5500");
        hm.get(1).add("66");
        for (String x:hm.get(1))
            System.out.println(x);*/
        
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.tweetsmining.engines.graphsminer.testing;

/**
 *
 * @author gyankos
 */

/*************************************************************************
 *  Compilation:  javac MarkovChain.java
 *  Execution:    java MarkovChain
 *  
 *  Computes the stationary distribution using several different
 *  methods.
 *
 *  Data taken from Glass and Hall (1949) who distinguish 7 states
 *  in their social mobility study:
 * 
 *   1. Professional, high administrative
 *   2. Managerial
 *   3. Inspectional, supervisory, non-manual high grade
 *   4. Non-manual low grade
 *   5. Skilled manual
 *   6. Semi-skilled manual
 *   7. Unskilled manual
 *
 *   See also Happy Harry, 2.39.
 *
 *************************************************************************/

import org.tweetsmining.engines.graphsminer.matrices.MatrixOp;
import org.tweetsmining.model.matrices.GMatrix;
import org.tweetsmining.model.matrices.IMatrix;

public class MarkovChain {

    public static void main(String[] args) { 

        // the state transition matrix
        int N = 3;
        double[][] transition = { { 0.1, 0.5, 0},
                                  { 0, 0, 0.7},
                                  { 0.5, 0.5, 0}
                                };
        IMatrix m = new GMatrix();
        for (int i=0; i<N; i++)
            for (int j=0; j<N; j++)
                m.set(i, j, transition[i][j]);

/*
        // compute using 50 iterations of power method
        Matrix A = new Matrix(transition);
        A = A.transpose();
        Matrix x = new Matrix(N, 1, 1.0 / N); // initial guess for eigenvector
        for (int i = 0; i < 50; i++) {
            x = A.times(x);
            x = x.times(1.0 / x.norm1());       // rescale
        }
        System.out.println("gia");
        x.print(9, 6);



        // compute by finding eigenvector corresponding to eigenvalue = 1
        System.out.println("gia");
        EigenvalueDecomposition eig = new EigenvalueDecomposition(A);
        Matrix V = eig.getV();
        double[] real = eig.getRealEigenvalues();
        for (int i = 0; i < N; i++) {
            if (Math.abs(real[i] - 1.0) < 1E-6) {
                x = V.getMatrix(0, N-1, i, i);
                x = x.times(1.0 / x.norm1());
                
                x.print(9, 6);
            }
        }
*/
        
        // If ergordic, stationary distribution = unique solution to Ax = x
        // up to scaling factor.
        // We solve (A - I) x = 0, but replace row 0 with constraint that
        // says the sum of x coordinates equals one
        /*Matrix B = A.minus(Matrix.identity(N, N));
        for (int j = 0; j < N; j++)
            B.set(0, j, 1.0);
        Matrix b = new Matrix(N, 1);
        b.set(0, 0, 1.0);
        x = B.solve(b);
        System.out.println("gia");
        x.print(9, 6);*/
        IMatrix cl = MatrixOp.stationaryDistribution(m);
        System.out.println(cl.nRows());
        
    }

}

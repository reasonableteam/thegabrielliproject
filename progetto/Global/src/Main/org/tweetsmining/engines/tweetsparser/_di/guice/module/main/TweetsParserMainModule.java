package org.tweetsmining.engines.tweetsparser._di.guice.module.main;

import org.tweetsmining.config._di.guice.ConfigModule;
import org.tweetsmining.engines.tweetsparser._di.guice.module.core.TweetsParserCoreModule;
import org.tweetsmining.persister.couchdb._di.guice.module.PersisterModuleCouchDB;
import org.tweetsmining.persister.engines.tweetsparser._di.guice.module.TweetsParserPersisterModule;

import com.google.inject.AbstractModule;

public class TweetsParserMainModule extends AbstractModule {

	@Override
	protected void configure() {
		// here we parse "stemmer.wordnet_resources_dir"
		install( new ConfigModule() );
		
		//persistence zone
		install( new PersisterModuleCouchDB() );
		install( new TweetsParserPersisterModule() );
		
		// core bindings
		install( new TweetsParserCoreModule() );
	}
}

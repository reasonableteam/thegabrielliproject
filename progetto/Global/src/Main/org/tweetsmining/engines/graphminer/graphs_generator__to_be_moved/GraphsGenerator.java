package org.tweetsmining.engines.graphminer.graphs_generator__to_be_moved;

import java.util.List;
import java.util.Map.Entry;

import javax.inject.Inject;

import org.tweetsmining.dao.engines.graphsminer.UserToGraphDAO;
import org.tweetsmining.dao.engines.wordsrelator.UserToRelatedWordsDAO;
import org.tweetsmining.engines.graphsminer.Database.Word;
import org.tweetsmining.model.graph.MultiLayerGraph;
import org.tweetsmining.model.graph.database.Entity;
import org.tweetsmining.model.graph.database.Relation;
import org.tweetsmining.model.user.User;
import org.tweetsmining.model.wordsrelator.RelationedTerm;
import org.tweetsmining.model.wordsrelator.WordRelations;

public class GraphsGenerator {

	private final UserToRelatedWordsDAO userToRelatedWordsDAO;
	private final UserToGraphDAO userToGraphDAO;
	
	@Inject
	public GraphsGenerator(UserToRelatedWordsDAO userToRelatedWordsDAO, UserToGraphDAO userToGraphDAO) {
		this.userToRelatedWordsDAO = userToRelatedWordsDAO;
		this.userToGraphDAO = userToGraphDAO;
	}	

	public void createUserGraphs() {
		for (Entry<User, List<WordRelations>> userToRelated : userToRelatedWordsDAO) {
			User user = userToRelated.getKey();
			List<WordRelations> wordRelationsList = userToRelated.getValue();
			MultiLayerGraph userGraph = new MultiLayerGraph();
			System.out.println("\n\t"+user.toString()+": ");

			
			
			// Creating the user graph
			for (WordRelations wordRelations: wordRelationsList) {
			/*	System.out.println("\n"+wordRelations.getWord() + " --> "+ wordRelations.getRelationType().getUrl());
				Entity word = userGraph.addVertex(Word.getWordUrl(wordRelations.getWord()));
				Relation senseRelation = userGraph.newLinkType(wordRelations.getRelationType().getUrl());
				for (String sense : wordRelations.getMainsenses()) {
					// Linking each word to its sense
					Entity nodeSense = userGraph.addVertex(Word.getWordUrl(sense));
					userGraph.addTriple(word, senseRelation, nodeSense, 1);

					for (RelationedTerm relatedToSense : wordRelations.getTermsForMainsense(sense)) {
						if (relatedToSense == null)
							System.out.println("badass");
						
						Entity leafWord = userGraph.addVertex(Word.getWordUrl(relatedToSense.getTerm()));
						Relation relatedToLeafWith = userGraph.newLinkType(relatedToSense.getRelationType().getUrl());
						userGraph.addTriple(nodeSense, relatedToLeafWith, leafWord, 1);
					}
				}*/
				addToMultiLayerGraph(userGraph, wordRelations);
			}
			userToGraphDAO.put(user, userGraph);
		}
		userToGraphDAO.commit();
	}
	
	public static void addToMultiLayerGraph(MultiLayerGraph userGraph, WordRelations wordRelations) {
//		System.out.println("\n"+wordRelations.getWord() + " --> "+ wordRelations.getRelationType().getUrl());
		Entity word = userGraph.addVertex(Word.getWordUrl(wordRelations.getWord()));
		Relation senseRelation = userGraph.newLinkType(wordRelations.getRelationType().getUrl());
		for (String sense : wordRelations.getMainsenses()) {
			// Linking each word to its sense
			Entity nodeSense = userGraph.addVertex(Word.getWordUrl(sense));
			userGraph.addTriple(word, senseRelation, nodeSense, 1);

			for (RelationedTerm relatedToSense : wordRelations.getTermsForMainsense(sense)) {
				if (relatedToSense == null)
					System.out.println("badass");
				
				Entity leafWord = userGraph.addVertex(Word.getWordUrl(relatedToSense.getTerm()));
				Relation relatedToLeafWith = userGraph.newLinkType(relatedToSense.getRelationType().getUrl());
				userGraph.addTriple(nodeSense, relatedToLeafWith, leafWord, 1);
			}
		}
	}
}

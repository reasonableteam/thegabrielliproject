package org.tweetsmining.engines.graphminer._di.guice.module.main;

import org.tweetsmining.config._di.guice.ConfigModule;
import org.tweetsmining.persister.couchdb._di.guice.module.PersisterModuleCouchDB;
import org.tweetsmining.persister.engines.graphminer.graphgenerator._di.guice.module.ClustersPersisterModule;

import com.google.inject.AbstractModule;

public class ClustersMainModule extends AbstractModule {

	@Override
	protected void configure() {

		// common
		install(new ConfigModule());

		// persistence zone
		install(new PersisterModuleCouchDB());
		
		// core
		install( new ClustersPersisterModule() );

	}

}

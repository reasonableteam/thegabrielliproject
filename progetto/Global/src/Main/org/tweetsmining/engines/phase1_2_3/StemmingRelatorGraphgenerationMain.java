/*******************************************************************************
 * Copyleft (c) 2014, "Massimiliano Leone - <maximilianus@gmail.com> - https://plus.google.com/+MassimilianoLeone"
 * This file (WordsRelatorByBabelnet.java) is part of WordsRelatorByBabelnet.
 * 
 *     WordsRelatorByBabelnet.java is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     WordsRelatorByBabelnet.java is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with .  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.tweetsmining.engines.phase1_2_3;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

import javax.inject.Inject;
import javax.inject.Named;

import org.tweetsmining.dao.engines.graphsminer.UserToGraphDAO;
import org.tweetsmining.dao.engines.tweetsparser.UserToStemmedWordsDAO;
import org.tweetsmining.dao.engines.wordsrelator.UserToRelatedWordsDAO;
import org.tweetsmining.dao.engines.wordsrelator.WordRelatorDAO;
import org.tweetsmining.engines.graphminer.graphs_generator__to_be_moved.GraphsGenerator;
import org.tweetsmining.engines.wordsrelator.babelnetsearcher.BabelnetSearcher;
import org.tweetsmining.engines.wordsrelator.babelnetsearcher.exceptions.NullRelatedMapException;
import org.tweetsmining.engines.wordsrelator.babelnetsearcher.exceptions.NullSynsetsException;
import org.tweetsmining.engines.wordsrelator.printer.Printer;
import org.tweetsmining.model.graph.MultiLayerGraph;
import org.tweetsmining.model.user.User;
import org.tweetsmining.model.wordsrelator.WordRelations;
import org.tweetsmining.model.wordsrelator.exceptions.RelationStringNullException;

import pi.ParIterator;
import pi.ParIteratorFactory;

import com.google.inject.Guice;
import com.google.inject.Injector;

public class StemmingRelatorGraphgenerationMain {

	private final BabelnetSearcher babelnetSearcher;
	private final WordRelatorDAO wordRelatorDAO;
	private Printer printer;
	private final UserToRelatedWordsDAO userToRelatedWordsDAO;
	private final UserToStemmedWordsDAO userToStemmedWordsDAO;
	private final UserToGraphDAO userToGraphDAO;
	private final int threadsNumber;
	private final FutureTask<Void> searchTask;

	@Inject
	public StemmingRelatorGraphgenerationMain
		   (BabelnetSearcher babelnetSearcher,
			UserToStemmedWordsDAO userToStemmedWordsDAO,
			WordRelatorDAO wordRelatorDAO,
			UserToRelatedWordsDAO userToRelatedWordsDAO,
			UserToGraphDAO userToGraphDAO,
			String tweetCorpusName, 
			Printer printer,
			@Named("num_threads") int threadsNumber) {
		this.babelnetSearcher = babelnetSearcher;
		this.userToStemmedWordsDAO = userToStemmedWordsDAO;
		this.wordRelatorDAO = wordRelatorDAO;				//Persister
		this.userToRelatedWordsDAO = userToRelatedWordsDAO; //Online Database
		this.userToGraphDAO = userToGraphDAO;
		this.printer = printer;
		// this.wordRelatorDAO.init(tweetCorpusName);
		this.threadsNumber = threadsNumber;
		
		this.searchTask = initFutureTask();
//		futureTask.run();
//		submit.cancel(mayInterruptIfRunning);
	}
	
	private FutureTask<Void> initFutureTask() {
		Callable<Void> searchCallable = new Callable<Void>() {
			@Override
			public Void call() throws Exception {
				searchForWordsAndGenerateGraphs();
				return null;
			}
		};
		FutureTask<Void> futureTask = new FutureTask<Void>(searchCallable) { 
			@Override
			public boolean cancel(boolean mayInterruptIfRunning) {
				commitAll();
				return  super.cancel(mayInterruptIfRunning);
			}
		};
		return futureTask;
	}
	
	public FutureTask<Void> getSearchTask() {
		return searchTask;
	}
	
	/*
	public void addToMLGraph(MultiLayerGraph userGraph, WordRelations wordRelations) {
		System.out.println("~~ " + wordRelations.getWord() + " --> " + wordRelations.getRelationType().getUrl());
		Entity word = userGraph.addVertex(Word.getWordUrl(wordRelations.getWord()));
		Relation senseRelation = userGraph.newLinkType(wordRelations.getRelationType().getUrl());
		for (String sense : wordRelations.getMainsenses()) {
			//Linking each word to its sense
			Entity nodeSense = userGraph.addVertex(Word.getWordUrl(sense));
			userGraph.addTriple(word, senseRelation, nodeSense, 1);
			
			for (RelationedTerm relatedToSense : wordRelations.getTermsForMainsense(sense)) {
				if (relatedToSense == null)
					System.out.println("badass");
				Entity leafWord = userGraph.addVertex(Word.getWordUrl(relatedToSense.getTerm()));
				Relation relatedToLeafWith = userGraph.newLinkType(relatedToSense.getRelationType().getUrl());
				userGraph.addTriple(nodeSense, relatedToLeafWith, leafWord, 1);
			}
		}
	}*/

	public void searchForWordsAndGenerateGraphs() {		

		int usersCounter = 1;
		Iterator<Entry<User, Set<String>>> userToStemmedWordIterator = userToStemmedWordsDAO.iterator();
		System.out.println("Found " + userToStemmedWordsDAO.size() + " users. Done:");
		while (userToStemmedWordIterator.hasNext()) {
			Entry<User, Set<String>> next = userToStemmedWordIterator.next();
			User user = next.getKey();
			printer.print("\nUser " +usersCounter+ " ("+user.toString()+")"+" : ");
			
			if (userToGraphDAO.containsKey(user)) {
				printer.println(" already exists, skipping");
				continue;
			}
			
			Set<String> stemmedWords = next.getValue();
			printer.println(stemmedWords.size()+" words");
			List<WordRelations> wordRelationsList = new ArrayList<WordRelations>();
			
			MultiLayerGraph userGraph = new MultiLayerGraph();
			
			ParIterator<String> stemmedWordParIterator = ParIteratorFactory.createParIterator(stemmedWords, threadsNumber);
			
//			for (String word : stemmedWords) {
			while (stemmedWordParIterator.hasNext()) {
				String word = stemmedWordParIterator.next();
//				printer.print(word);
				WordRelations relationsForWordFounded = null;
				if (wordRelatorDAO.containsKey(word)) { // found in cache
					relationsForWordFounded = wordRelatorDAO.get(word);
					wordRelationsList.add(relationsForWordFounded);
					printer.print(" (l) ");
				} else {
					try {
						relationsForWordFounded = babelnetSearcher.findRelationsForWord(word);
						printer.print(" (b) ");
					} catch (NullSynsetsException e) {
						e.printStackTrace();
						printer.print("(bE) ");
					} catch (NullRelatedMapException e) {
						e.printStackTrace();
						printer.print("(bE) ");
					} catch (RelationStringNullException e) {
						e.printStackTrace();
						printer.print("(bE) ");
					}
				}
				if (!relationsForWordFounded.getMainsenses().isEmpty()) {
					GraphsGenerator.addToMultiLayerGraph(userGraph, relationsForWordFounded);
				}
			}
			if (!wordRelationsList.isEmpty())
				userToGraphDAO.put(user, userGraph);
			
			usersCounter++;
		}
		commitAll();
	}
	
	private void commitAll() {
		printer.print("\ncommitting all: ");
		userToRelatedWordsDAO.commit();
		printer.print("userToRelatedWordsDAO, ");
		wordRelatorDAO.commit();
		printer.print("wordRelatorDAO, ");
		userToGraphDAO.commit();
		printer.print("userToGraphDAO.");
	}


	public static void main(String[] args) {

		// if (args.length!=1) {
		// System.out.println("needed argument: [filename_without_extension_as_tweet_corpus_name]");
		// } else {

		Injector injector = Guice.createInjector( new StemmingRelatorGraphgenerationMainModule() );
		StemmingRelatorGraphgenerationMain stemmingRelatorGraphgenerationMain = injector.getInstance(StemmingRelatorGraphgenerationMain.class);
//		System.out.println("ok!");
		stemmingRelatorGraphgenerationMain.searchForWordsAndGenerateGraphs();
		// }
	}
}


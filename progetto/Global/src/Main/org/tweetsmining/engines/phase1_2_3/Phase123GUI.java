package org.tweetsmining.engines.phase1_2_3;

import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.concurrent.FutureTask;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import com.google.inject.Guice;
import com.google.inject.Injector;

public class Phase123GUI extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8819899793699276604L;
	private JButton startButton;
	private JButton stopButton;
	protected FutureTask<Void> task;
	
//	JPanel panel;
	private JTextArea jTextArea;
	
	public Phase123GUI(final FutureTask<Void> task) {
    	super("Demo printing to JTextArea");
    	
    	this.task = task;
    	setSize(800,600); // sets the size of the window

//    	JPanel panel = new JPanel();
//    	panel.setSize(800, 600);
//    	panel.setOpaque(true);
//    	panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

    	setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.insets = new Insets(10, 10, 10, 10);
        constraints.anchor = GridBagConstraints.WEST;
        
//        add(new JScrollPane(textArea), constraints);
		
		startButton = new JButton("Start");
		stopButton = new JButton("Stop");
		
		JPanel inputpanel = new JPanel();
        inputpanel.setLayout(new FlowLayout());
        inputpanel.add(startButton);
        inputpanel.add(stopButton);
        
		add(startButton, constraints);
        
        constraints.gridx = 1;
        add(stopButton, constraints);
         
        constraints.gridx = 0;
        constraints.gridy = 1;
        constraints.gridwidth = 2;
        constraints.fill = GridBagConstraints.BOTH;
        constraints.weightx = 1.0;
        constraints.weighty = 1.0;
		
		
		JScrollPane textPanel = new JScrollPane();
		textPanel.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		textPanel.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		jTextArea = new JTextArea("aa",30,50);
		jTextArea.setText("aasdjasd");
		jTextArea.setVisible(true);
//		jTextArea.setForeground(Color.WHITE);
//		jTextArea.setBackground(Color.BLACK);
		jTextArea.setWrapStyleWord(true);
		jTextArea.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
//        jTextArea.setEditable(false);
//        DefaultCaret caret = (DefaultCaret) textArea.getCaret();
//        caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		jTextArea.append("Text:");
		textPanel.add(jTextArea);
		
	
//		add(textPanel, constraints);
		
//		panel.add(textPanel);
//		panel.add(inputpanel);
//		
//		
//		add(panel);
		
//		getContentPane().add(BorderLayout.CENTER, panel);
        pack();
        setLocationByPlatform(true);
        setVisible(true);
        requestFocus();
		
		
		
		startButton.addActionListener( new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
//				String actualText = jTextArea.getText();
//				jTextArea.setText(actualText+"a");
//				System.out.println("a");
				task.run();
			}
		});
		stopButton.addActionListener( new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				task.cancel(true);				
			}
		});
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//        setSize(800, 600);
        setLocationRelativeTo(null);    // centers on screen
	}
	
	public static void main(String[] args) {
		try {
            //UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
            UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
//            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

        } catch (UnsupportedLookAndFeelException ex) {
            ex.printStackTrace();
        } catch (IllegalAccessException ex) {
            ex.printStackTrace();
        } catch (InstantiationException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        UIManager.put("swing.boldMetal", Boolean.FALSE);
        
        
        Injector injector = Guice.createInjector( new StemmingRelatorGraphgenerationMainModule() );
		final StemmingRelatorGraphgenerationMain stemmingRelatorGraphgenerationMain = injector.getInstance(StemmingRelatorGraphgenerationMain.class);
//		System.out.println("ok!");
//		stemmingRelatorGraphgenerationMain.searchForWordsAndGenerateGraphs();
        
        
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
            	new Phase123GUI( stemmingRelatorGraphgenerationMain.getSearchTask() ).setVisible(true);
            }
        });
	}



}

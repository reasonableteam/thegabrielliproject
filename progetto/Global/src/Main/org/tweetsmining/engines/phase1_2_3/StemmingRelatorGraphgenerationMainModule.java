package org.tweetsmining.engines.phase1_2_3;

import org.tweetsmining.config._di.guice.ConfigModule;
import org.tweetsmining.engines.tweetsparser._di.guice.module.core.TweetsParserCoreModule;
import org.tweetsmining.engines.wordsrelator._di.guice.module.core.WordsRelatorCoreModule;
import org.tweetsmining.persister.couchdb._di.guice.module.PersisterModuleCouchDB;
import org.tweetsmining.persister.engines.graphminer.graphgenerator._di.guice.module.GraphGeneratorPersisterModule;
import org.tweetsmining.persister.engines.tweetsparser._di.guice.module.TweetsParserPersisterModule;
import org.tweetsmining.persister.engines.wordrelator._di.guice.module.WordRelatorPersistenceModule;

import com.google.inject.AbstractModule;

public class StemmingRelatorGraphgenerationMainModule extends AbstractModule {

	@Override
	protected void configure() {
		// common
		install(new ConfigModule());

		// persistence zone
		install(new PersisterModuleCouchDB());
		
		install( new TweetsParserPersisterModule() );
		install( new TweetsParserCoreModule() );
		
		install( new WordRelatorPersistenceModule() );
		install( new WordsRelatorCoreModule() );
		
		install( new GraphGeneratorPersisterModule() );
	}

}

/*******************************************************************************
 * Copyleft (c) 2014, "Massimiliano Leone - <maximilianus@gmail.com> - https://plus.google.com/+MassimilianoLeone"
 * This file (WordsRelatorByBabelnetModule.java) is part of WordsRelatorByBabelnet.
 * 
 *     WordsRelatorByBabelnetModule.java is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     WordsRelatorByBabelnetModule.java is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with .  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.tweetsmining.engines.wordsrelator._di.guice.module.main;

import org.tweetsmining.config._di.guice.ConfigModule;
import org.tweetsmining.engines.wordsrelator._di.guice.module.core.WordsRelatorCoreModule;
import org.tweetsmining.persister.couchdb._di.guice.module.PersisterModuleCouchDB;
import org.tweetsmining.persister.engines.tweetsparser._di.guice.module.TweetsParserPersisterModule;
import org.tweetsmining.persister.engines.wordrelator._di.guice.module.WordRelatorPersistenceModule;

import com.google.inject.AbstractModule;

public class WordsRelatorMainModule extends AbstractModule {

	@Override
	protected void configure() {
		
		// common
		install( new ConfigModule() );

		// we use couchdb for persistence
		install( new PersisterModuleCouchDB() );
		
		install( new WordRelatorPersistenceModule() );
		
		// dependence
		// to read from
		install( new TweetsParserPersisterModule() );
		
		// core
		install( new WordsRelatorCoreModule() );
	}
}

package org.tweetsmining.main.engines.graphminer;

import org.tweetsmining.engines.graphminer._di.guice.module.main.GraphGeneratorMainModule;
import org.tweetsmining.engines.graphminer.graphs_generator__to_be_moved.GraphsGenerator;

import com.google.inject.Guice;

public class GraphsGeneratorMain {

	public static void main(String[] args) {
		Guice.createInjector( new GraphGeneratorMainModule() )
			.getInstance(GraphsGenerator.class)
				.createUserGraphs();
	}

}

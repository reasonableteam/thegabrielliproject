package org.tweetsmining.main.engines.graphminer;

import java.util.List;

import javax.inject.Inject;

import org.tweetsmining.engines.graphminer._di.guice.module.main.MinerMainModule;
import org.tweetsmining.engines.graphsminer.mining.MineUsers;
import org.tweetsmining.model.graphsminer.Cluster;

import com.google.inject.Guice;

public class MineUsersMain {
	
	private final MineUsers mineUsers;
	
	@Inject
	public MineUsersMain(MineUsers mineUsers) {
		this.mineUsers = mineUsers;
	}

	public void doDataMining() {
		List<Cluster> getCluster = mineUsers.dbToCluster();
		for (Cluster x : getCluster) {
			System.out.println(x.getListUsers());
		}
	}
	
	public static void main(String[] args) {
		Guice.createInjector( new MinerMainModule() )
			.getInstance(MineUsersMain.class)
				.doDataMining();		
	}

}

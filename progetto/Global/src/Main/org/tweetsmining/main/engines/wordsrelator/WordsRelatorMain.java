package org.tweetsmining.main.engines.wordsrelator;

import org.tweetsmining.engines.wordsrelator.WordsRelator;
import org.tweetsmining.engines.wordsrelator._di.guice.module.main.WordsRelatorMainModule;

import com.google.inject.Guice;
import com.google.inject.Injector;

public class WordsRelatorMain {
	
	public static void main(String[] args) {

		// if (args.length!=1) {
		// System.out.println("needed argument: [filename_without_extension_as_tweet_corpus_name]");
		// } else {

//		String tweetCorpusName = args[0];
		
		Injector injector = Guice.createInjector( new WordsRelatorMainModule() );
		WordsRelator wordsRelator = injector.getInstance(WordsRelator.class);
//		System.out.println("ok!");
		wordsRelator.searchForWords();
		// }
	}
}

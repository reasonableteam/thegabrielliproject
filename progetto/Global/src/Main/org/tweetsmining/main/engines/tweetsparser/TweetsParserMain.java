package org.tweetsmining.main.engines.tweetsparser;

import org.tweetsmining.engines.tweetsparser._di.guice.module.main.TweetsParserMainModule;
import org.tweetsmining.engines.tweetsparser.utils.csv.TweetsCSVParser;

import com.google.inject.Guice;
import com.google.inject.Injector;

public class TweetsParserMain {

	public static void main(String[] args) throws Exception {

//		String dirParentPath = args[0];
//		String dirPath = args[1];

//		String corpusPath = dirParentPath + File.separatorChar + dirPath;
//		String corpusName = args[0];

		Injector injector = Guice.createInjector(new TweetsParserMainModule());
		
		/*
		String configsFilePath = "resources"+File.separatorChar+"tweets_parser"+File.separatorChar;

		TweetParser tweetParser = new TweetParser();
		
		WordNetStemmer wordNetStemmer = new WordNetStemmer(configsFilePath);
		
		StopwordsRemover stopwordsRemover = new StopwordsRemover(configsFilePath+"stopwords.txt");
		
		SimpleDateFormat dateFormat = new SimpleDateFormat();
		String dateFormatPattern = "yyyy-MM-dd HH:mm:ss";
		dateFormat.applyLocalizedPattern(dateFormatPattern);
		
		UserToStemmedWordsDAO userToStemmedWordsDAO = injector.getInstance(UserToStemmedWordsDAO.class);
		
		String corpusName = injector.getInstance(Key.get(String.class, Names.named("corpus")));
		
		
		TweetsCSVParser tweetCSVParser = new TweetsCSVParser(tweetParser,
				wordNetStemmer,
				stopwordsRemover,
				dateFormat, 
				userToStemmedWordsDAO,
				corpusName);
		*/
		TweetsCSVParser tweetCSVParser = injector.getInstance(TweetsCSVParser.class);
		
		tweetCSVParser.extractUsersAndWordsFromCSVDirectoryAndApplyStemming();
		//tweetCSVParser.serializeUsersAndWords(corpusName+"_userToStemmedWords.ser");
		tweetCSVParser.persistUsersWithStemmedWords();
	}
}

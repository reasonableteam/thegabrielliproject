/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.tweetsmining.dao.mapdao.mappedfile;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Map;

import org.tweetsmining.util.map.model.MapEntry;

/**
 *
 * @author gyankos
 * @param <K>
 * @param <T>
 */
public class MappedFileIterator<K extends Serializable,T extends Serializable> implements Iterator<Map.Entry<K,T>> {
        private final MappedFile<K,T> km;
        private K current_key;
        private T current_val;
        
        private void pace() {
            current_key = (K)this.km.read();
            current_val = (T)this.km.read();
        }
        
        public MappedFileIterator(MappedFile<K,T> km) {
            this.km = km;
            this.km.close();
            pace();
        }

        @Override
        public boolean hasNext() {
            return ((current_key!=null)&&(current_val!=null));
        }

        @Override
        public Map.Entry<K, T> next() {
            MapEntry<K, T> toret;
            toret = new MapEntry<>(current_key,current_val);
            pace();
            return toret;
        }

        @Override
        public void remove() {
            km.remove(current_key);
            pace();
        }
        
    }


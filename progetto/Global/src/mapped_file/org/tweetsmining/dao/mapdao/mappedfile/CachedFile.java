/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.tweetsmining.dao.mapdao.mappedfile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 *
 * @author gyankos
 */
public class CachedFile {

    private String file;
    private boolean opened;
    private boolean doRead;
    private FileInputStream checkEnd;
    private ObjectInputStream objectInputStream;
    private ObjectOutputStream objectOutputStream;


    public CachedFile(String filename) {
        this.file = filename;
        this.opened = false;
        this.doRead = false;
    }
    
    protected void close() {
        if (this.doRead && this.opened) {
            try {
                objectInputStream.close();
                objectInputStream = null;
                this.checkEnd = null;
                this.opened = false;
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        } else if ((!this.doRead) && this.opened) {
            try {
                objectOutputStream.close();
                objectOutputStream = null;
                this.opened = false;
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    protected Object read() {
        if ((!this.doRead) && (this.opened)) {
            close();
        }
        if (!this.opened) {
            try {
                this.checkEnd = new FileInputStream(this.file);
            } catch (FileNotFoundException ex) {
                //ex.printStackTrace();
                close();
                return null;
            }
            try {
                objectInputStream = new ObjectInputStream((this.checkEnd));
                this.doRead = true;
                this.opened = true;
            } catch (IOException ex) {
                ex.printStackTrace();
            
            }
        }
        try {
            return objectInputStream.readObject();
        } catch (Throwable ex) {
            //ex.printStackTrace();
            close();
            return null;
        }
        
    }
    
    /**
     * Returns true if the file has been removed
     * @return 
     */
    public boolean rm() {
        File f = new File(getFileName());
        if (f.exists() && !f.isDirectory()) {
            f.delete();
            return true;
        } else
            return false;
    }

    public void write(Object obj)  {
        if ((this.doRead) && (this.opened)) {
            close();
        }
        File f = new File(file);
        
        if (!this.opened) {
            try {
                this.doRead = false;
                if (!f.exists())
                    objectOutputStream = new ObjectOutputStream((new FileOutputStream(this.file)));
                else if (!f.isDirectory()) {
                    objectOutputStream = new ObjectOutputStream(new FileOutputStream(this.file, true)) {
                               protected void writeStreamHeader() throws IOException {
                                   reset();
                               }
                           };
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        try {
            objectOutputStream.writeObject(obj);
            this.opened = true;
        } catch (IOException ex) {
            ex.printStackTrace();
            close();
        }
    }


    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        close();
    }
    
    public String getFileName() {
        return this.file;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.tweetsmining.dao.mapdao.mappedfile;

import java.io.File;
import java.io.Serializable;
import java.util.ListIterator;

/**
 *
 * @author gyankos
 * @param <T>
 */
public class LinkedFileIterator<T extends Serializable> implements ListIterator<T> {

    private int count;
    private File f;
    private T current;
    private CachedFile male;
    
    
    public LinkedFileIterator(String file) {
    	male = new CachedFile(file);
        count = 0;
        current = (T)male.read();
    }
    
    public LinkedFileIterator(File f) {
    	male = new CachedFile(f.getAbsolutePath());
        count = 0;
        current = (T)male.read();
    }
 
    @Override
    public boolean hasNext() {
        return (current!=null);
    }

    @Override
    public T next() {
        count++;
        T toret = current;
        current = (T)male.read();
        if (toret==null)
        	count = 0;
        return toret;
    }

    @Override
    public boolean hasPrevious() {
    	return (count!=0);
    }

    @Override
    public T previous() {
        male.close();
        for (int i = 0; i<count-1; i++)
            next();
        count--;
        return next();
    }

    @Override
    public int nextIndex() {
        return (count+1);
    }

    @Override
    public int previousIndex() {
        return (count-1);
    }

    @Override
    public void remove() {
        male.close();
        File f = new File(male.getFileName() + ".tmp");

        CachedFile cf = new CachedFile(f.getAbsolutePath());
        
        for (int i = 0; i<count; i++) 
            cf.write(male.read());
        male.read();
        Object tmp;
        while ((tmp = male.read())!=null)
            cf.write(tmp);
        String name = male.getFileName();
        male.rm();
        f.renameTo(new File(name));
        
        male.close();
        for (int i = 0; i<count; i++)
            male.read();
        count--;
    }

    @Override
    public void set(T e) {
        male.close();
        File f = new File(male.getFileName() + ".tmp");

        CachedFile cf = new CachedFile(f.getAbsolutePath());
        
        for (int i = 0; i<count; i++) 
            cf.write(male.read());
        
        cf.write(e);
        
        Object tmp;
        while ((tmp = male.read())!=null)
            cf.write(tmp);
        String name = male.getFileName();
        male.rm();
        f.renameTo(new File(name));
    
        male.close();
        for (int i = 0; i<count; i++)
        	male.read();
    }

    @Override
    public void add(T e) {
    
    	male.close();
        File f = new File(male.getFileName() + ".tmp");

        CachedFile cf = new CachedFile(f.getAbsolutePath());
        
        for (int i = 0; i<count; i++) 
            cf.write(male.read());
        
        cf.write(e);
        //cf.write(read());//Object at current position
        
        Object tmp;
        while ((tmp = male.read())!=null)
            cf.write(tmp);
        String name = male.getFileName();
        male.rm();
        f.renameTo(new File(name));
        male.close();
        for (int i = 0; i<count; i++)
        	male.read();
    }

    public void delete() {
    	male.rm();
    	count = 0;
    }

	public void close() {
		male.close();
		count = 0;
		current = (T)male.read();
	}

	public String getFileName() {
		return male.getFileName();
	}

}

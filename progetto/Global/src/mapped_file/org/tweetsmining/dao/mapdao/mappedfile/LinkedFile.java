/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.tweetsmining.dao.mapdao.mappedfile;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import org.tweetsmining.io.Committable;
import org.tweetsmining.io.SerializableList;


/**
 *
 * @author gyankos
 * @param <T>
 */
public class LinkedFile<T extends Serializable> implements SerializableList<T>, Committable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 8572307987801525625L;
	private LinkedFileIterator<T> fit;
    private String name;
    int count = 0;
    
    public LinkedFile(String str) {
        fit = new LinkedFileIterator<>(str);
        name = str;
        if (new File(str).exists())
	        while (fit.hasNext()) {
	        	count++;
	        	fit.next();
	        }
    }
    
    private void start() {
    	if (fit==null)
    		fit = new LinkedFileIterator<>(name);
    	fit.close();
    }
    
    @Override
    public int size() {
    	return count;
    }

    @Override
    public boolean isEmpty() {
    	return (count==0);
    }

    @Override
    public boolean contains(Object o) {
    	start();
    	while (fit.hasNext())
    		if (fit.next().equals(o)) {
    			fit = null;
    			return true;
    		}
    	return false;
    }

    @Override
    public Iterator<T> iterator() {
    	start();
    	return fit;
    }

    @Override @Deprecated
    public Object[] toArray() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override @Deprecated
    public <T> T[] toArray(T[] a) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean add(T e) {
    	start();
    	fit.add(e);
    	return true;
    }

    @Override
    public boolean remove(Object o) {
    	start();
    	boolean toret = false;
    	while (fit.hasNext())
    		if (fit.next().equals(o)) {
    			fit.remove();
    			toret = true;
    		}
    	return toret;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
    	start();
    	boolean toret = true;
    	while (fit.hasNext())
    		if (c.contains(fit.next()))
    			toret = toret && true;
    		else 
    			toret = false;
    	return toret;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
    	for (T x : c) {
    		fit.add(x);
    		fit.next();
    	}
    	return true;
    }

    @Override //Ordine retroverso: error
    public boolean addAll(int index, Collection<? extends T> c) {
    	start();
    	for (int i = 0; i<index; i++)
    		fit.next();
    	return addAll(c);
    }

    @Override
    public boolean removeAll(Collection<?> c) {
    	start();
    	for (Object o : c) 
    		remove(o);
    	return true;
    }

    @Override @Deprecated
    public boolean retainAll(Collection<?> c) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void clear() {
    	this.fit.delete();
    }

    @Override
    public T get(int index) {
    	start();
    	for (int i=0; i<index; i++)
    		fit.next();
    	return fit.next();
    }

    @Override
    public T set(int index, T element) {
    	start();
    	for (int i=0; i<index; i++)
    		fit.next();
    	fit.set(element);
    	return null;
    }

    @Override
    public void add(int index, T element) {
    	start();
    	for (int i=0; i<index; i++)
    		fit.next();
    	fit.add(element);
    }

    @Override
    public T remove(int index) {
    	start();
    	for (int i=0; i<index; i++)
    		fit.next();
    	T toret = fit.next();
    	fit.previous();
    	for (int i=0; i<index; i++)
    		fit.next();
    	fit.remove();
    	return toret;
    }

    @Override
    public int indexOf(Object o) {
    	start();
    	int c = 0;
    	while (fit.hasNext())
    		if (fit.next().equals(o))
    			return c+1;
    		else
    			c++;
    	return -1;
    }

    @Override //Forced
    public int lastIndexOf(Object o) {
    	return indexOf(o);
    }

    @Override
    public ListIterator<T> listIterator() {
    	return listIterator(0);
    }

    @Override
    public ListIterator<T> listIterator(int index) {
    	start();
    	for (int i=0; i<index; i++)
    		this.fit.next();
    	return this.fit;
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
    	LinkedFile<T> lf;
    	try {
    		LinkedFileIterator<T> fi = new LinkedFileIterator<T>(File.createTempFile(Integer.toString(fromIndex), Integer.toString(toIndex)));
			lf = new LinkedFile<>(fi.getFileName());
		} catch (IOException e) {
			return null;
		}
    	start();
    	for (int i=0; i<fromIndex; i++)
    		this.fit.next();
    	for (int i=fromIndex; i<toIndex; i++)
    		lf.add((T)fit.next());
    	return lf;
    }

	@Override
	public void commit() {
		fit.close();
	}
    
}

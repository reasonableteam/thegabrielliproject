/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.tweetsmining.dao.mapdao.mappedfile;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.tweetsmining.util.map.iterable.IterableMap;

/**
 *
 * @author gyankos
 * @param <K> Key
 * @param <T> Value
 */
public class MappedFile<K extends Serializable, T extends Serializable> extends CachedFile implements IterableMap<K,T> {


    private Set<K> keyset;
    int count;
    
    protected void recheck() {
        File f = new File(getFileName());
        K key;
        if (f.exists() && !f.isDirectory()) {
            while ((key = (K) read()) != null) {
                if (read() != null) {
                    this.keyset.add(key);
                    count++;
                } else {
                    break;
                }
            }
        }
    }

    public MappedFile(String filename) {
        super(filename);
        this.keyset = new HashSet<>();
        this.count = 0;
        recheck();
    }

   

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        close();
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public boolean isEmpty() {
        return ((count == 0));
    }

    @Override
    public boolean containsKey(K key) {
        return this.keyset.contains( key);
    }

    @Override
    public boolean containsValue(T value) {
        if (( value) == null) {
            return false;
        }
        T val = (T) value;

        while (read() != null) {
            T tmp = (T) read();
            if (tmp == null) {
                continue;
            }
            if (tmp.equals(val)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public T get(K key) {
        if (((K) key) == null) {
            return null;
        }
        K k = (K) key;

        if (!this.keyset.contains(k)) {
            return null;
        }

        Object tmp;
        K val;
        close();//from start ~ ensure it
        while ((tmp = read()) != null) {
            val = (K) tmp;
            /*if (val == null) {
                continue;
            }*/
            if (val.equals(k)) {
                return (T) read();
            }
        }
        return null;
    }

    @Override
    public T put(K key, T value) {
        //close();
        if (key==null)
            return null;
        T toret = remove(key);//will eventually perform close
        write(key);
        write(value);
        return toret;
    }

    public Set<T> remove(K... keys) {
        if (keys == null) {
            return null;
        }
        List<K> toRemoveKeys = Arrays.asList(keys);
        return remove(new HashSet(toRemoveKeys));

    }

//    @Override    
    public void putAll(Map<? extends K, ? extends T> m) {
        remove(m.keySet());
        for (Entry<? extends K, ? extends T> x: m.entrySet()) {
           put(x.getKey(),x.getValue()); 
        }
    }

    @Override
    public void clear() {
        if (super.rm()) {
            count = 0;
            keyset.clear();
        }
    }

    @Override
    public Set<K> keySet() {
        return this.keyset;
    }

    /*
    @Override @Deprecated
    public Collection<T> values() {
        close();
        Set<T> toret = new HashSet<>();
        while (read() != null) {
            T obj = (T) read();
            if (obj != null) {
                toret.add(obj);
            }
        }
        return toret;
    }*/

    /*
    @Override @Deprecated
    public Set<Entry<K, T>> entrySet() {
        close();
        Set<Entry<K, T>> toret = new HashSet<>();
        Object obj;
        while ((obj = read()) != null) {
            K key = (K) obj;
            if (key == null) {
                read();
                continue;
            }
            Object tmp = read();
            if (tmp == null) {
                return toret;
            }
            T value = (T) tmp;
            toret.add(new MapEntry<>(key, value));
        }
        return toret;
    }*/

    private Set<T> remove(Set<? extends K> toRemoveKeys) {
        boolean checkIntersect = false;
        Set<T> toret = new HashSet<>();
        for (K k : toRemoveKeys) {
            if (this.keyset.contains(k)) {
                checkIntersect = true;
            }
        }
        if (!checkIntersect) {
            return toret;
        }

        ObjectOutputStream dos;
        try {
            //tmp file 
            dos = new ObjectOutputStream((new FileOutputStream(getFileName() + "todelete_file")));
        } catch (IOException ex) {
            ex.printStackTrace();
            return toret;
        }

        close();//from start ~ ensure it
        Object tmp;
        K val;
        while ((tmp = read()) != null) {
            val = (K) tmp;
            /*if (val == null) {
                continue;
            }*/
            if (!toRemoveKeys.contains(val)) {
                try {
                    dos.writeObject(tmp);
                    dos.writeObject(read());
                } catch (IOException ex) {
                    ex.printStackTrace();
                    return null;
                }
            } else {
                toret.add((T) read());
            }
        }
        close();
        clear();
        File f = new File(getFileName() + "todelete_file");
        if (f.exists() && !f.isDirectory()) {
            f.renameTo(new File(getFileName()));
        }
        recheck();
        //At the next step, a new map will be created
        return toret;
    }

    @Override
    public T remove(K key) {
        if (key == null) {
            return null;
        }
        K k = (K) key;
        if (!keyset.contains(k)) {
            return null;
        }
        Set<K> torem = new HashSet<>();
        torem.add(k);
        for (T x : remove(torem)) {
            return x;
        }
        return null;
    }

    @Override
    public MappedFileIterator<K, T> iterator() {
        return new MappedFileIterator<>(this);
    }
    
    public void commit() {
    	close();
    }

    /**
     * dummy method
     * @return empty list
     */
	/*@Override @Deprecated
	public Collection<T> values() {
		return Collections.EMPTY_LIST; 
	}*/

    /*
	@Override
	public List<T> generateNewListAccordingly() {
		return new LinkedFile<>(getFileName()+"_"+RandomString.randomString());
	}*/
    
}

package org.tweetsmining.util.random;

import java.math.BigInteger;
import java.security.SecureRandom;

public enum RandomString {
	
	INSTANCE;

    private final SecureRandom random = new SecureRandom();
    
    public String randomString() {
    	return new BigInteger(130, random).toString(32);
    }
	
}

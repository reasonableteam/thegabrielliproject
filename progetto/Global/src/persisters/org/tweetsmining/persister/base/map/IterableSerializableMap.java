package org.tweetsmining.persister.base.map;

import org.tweetsmining.util.map.SerializableMap;
import org.tweetsmining.util.map.iterable.IterableMap;

public interface IterableSerializableMap<K, V> 
	extends IterableMap<K,V>, SerializableMap<K,V> {}

package org.tweetsmining.persister.base.list;

import org.tweetsmining.io.Committable;
import org.tweetsmining.io.SerializableList;

public interface LinkedListPersistable<T> extends SerializableList<T>, Committable {

}

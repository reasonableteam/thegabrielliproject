package org.tweetsmining.persister.couchdb.listable;

import java.util.ListIterator;

public class LinkedListIteratorOnCouchDB<E> implements ListIterator<E> {

	private LinkedListPersistableByCouchDB<E> lcdb;
	int current_pos = 0;
	public LinkedListIteratorOnCouchDB(LinkedListPersistableByCouchDB<E> linkedListPersistableByCouchDB) {
		this.lcdb = linkedListPersistableByCouchDB;
	}
	
	@Override
	public boolean hasNext() {
		return (current_pos < lcdb.size());
	}

	@Override
	public E next() {
		E toret = lcdb.get(current_pos);
		current_pos++;
		return toret;
	}

	@Override
	public boolean hasPrevious() {
		return (current_pos>0);
	}

	@Override
	public E previous() {
		if (current_pos == 0)
			current_pos = lcdb.size();
		current_pos--;
		return lcdb.get(current_pos);
	}

	@Override
	public int nextIndex() {
		if (current_pos<lcdb.size()) {
			return current_pos+1;
		} else
			return lcdb.size();
	}

	@Override
	public int previousIndex() {
		if (current_pos>0)
			return current_pos-1;
		else
			return lcdb.size();
	}

	@Override
	public void remove() {
		lcdb.remove(current_pos);
	}

	@Override
	public void set(E e) {
		lcdb.set(current_pos, e);
	}

	@Override
	public void add(E e) {
		lcdb.add(current_pos, e);
	}
	
}

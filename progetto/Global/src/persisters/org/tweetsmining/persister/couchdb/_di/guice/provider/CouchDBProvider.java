package org.tweetsmining.persister.couchdb._di.guice.provider;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Provider;
import javax.inject.Singleton;

import net.sf.json.JSONException;

import com.fourspaces.couchdb.CouchResponse;
import com.fourspaces.couchdb.Database;
import com.fourspaces.couchdb.Session;


@Singleton
public class CouchDBProvider implements Provider<Database> {
	
	private Database database;

	@Inject
	public CouchDBProvider(@Named("corpus") String databaseName, @Named("couchdb.host") String host, @Named("couchdb.port") int port) {
		Session session = new Session(host, port);
		Database database = null;
		try {
			database = session.getDatabase(databaseName);
//			if (database == null)
//				session.createDatabase(databaseName);
		} catch (JSONException e ) {
//			System.out.println("create database!");
			database = session.createDatabase(databaseName);
		}
		this.database = database;
//		System.out.println("database: "+database);
	}	
	

	@Override
	public Database get() {
		return database;
	}
}

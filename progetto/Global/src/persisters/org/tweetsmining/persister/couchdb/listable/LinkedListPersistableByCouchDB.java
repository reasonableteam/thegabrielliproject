package org.tweetsmining.persister.couchdb.listable;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map.Entry;

import org.tweetsmining.io.Committable;
import org.tweetsmining.io.SerializableList;
import org.tweetsmining.persister.couchdb.IterableSerializableMapByCouchDB;
import org.tweetsmining.persister.couchdb.helper.CouchDBDocumentHelperTmp;

public class LinkedListPersistableByCouchDB<T> implements SerializableList<T>, Committable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6192781688117331532L;
	private final IterableSerializableMapByCouchDB<Integer, T> iterableSerializableMapByCouchDB;
	private final CouchDBDocumentHelperTmp couchDBDocumentHelperTmp;
	
	private int count;
	
	public LinkedListPersistableByCouchDB(IterableSerializableMapByCouchDB<Integer, T> iterableMapPersisterByCouchDB, CouchDBDocumentHelperTmp couchDBDocumentHelperRandomable) {
		this.iterableSerializableMapByCouchDB = iterableMapPersisterByCouchDB;
		this.couchDBDocumentHelperTmp = couchDBDocumentHelperRandomable;
		count = 0;
	}

	@Override
	public int size() {
		return iterableSerializableMapByCouchDB.size();
	}

	@Override
	public boolean isEmpty() {
		return iterableSerializableMapByCouchDB.isEmpty();
	}

	@Override
	public boolean contains(Object o) {
		return iterableSerializableMapByCouchDB.containsValue(o);
	}

	@Override
	public Iterator<T> iterator() {
//		return ParIteratorFactory.createParIterator(
//				iterableMapPersisterByCouchDB., 
//				Config.THREADS);
		return listIterator();
	}

	@Override @Deprecated
	public Object[] toArray() {
		return Collections.emptyList().toArray();
	}

	@Override @Deprecated
	public <T> T[] toArray(T[] a) {
		return (T[]) Collections.emptyList().toArray();
	}

	@Override
	public boolean add(T e) {
		return (iterableSerializableMapByCouchDB.put(count++, e)!=null);
	}

	@Override
	public boolean remove(Object o) {
		if (((T)o)==null)
			return false;
		T tmp = (T)o;
		int key = -1;
		for (Entry<Integer, T> x : iterableSerializableMapByCouchDB) {
			if (tmp.equals(x.getValue()))
			{
				key = x.getKey();
				break;
			}
		}
		if (key != -1) {
			iterableSerializableMapByCouchDB.remove(key);
		}
		return (key != -1);
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		boolean all = true;
		for (Object o : c) {
			all = all && contains(o);
			if (!all)
				return false;
		}
		return true;
	}

	@Override
	public boolean addAll(Collection<? extends T> c) {
		boolean all = true;
		for (Object o : c) {
			all = all && add((T)o);
		}
		return all;
	}

	@Override @Deprecated
	public boolean addAll(int index, Collection<? extends T> c) {
		//I should change the key of all the entities: 
		return addAll(c);
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		boolean toret = true;
		for (Object o : c) {
			toret = toret && remove(o);
		}
		return toret;
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		for (Entry<Integer, T> x : iterableSerializableMapByCouchDB) {
			if (!c.contains(x.getValue()))
				iterableSerializableMapByCouchDB.remove(x.getKey());
		}
		return true;
	}

	@Override
	public void clear() {
		iterableSerializableMapByCouchDB.clear();
	}

	@Override
	public T get(int index) {
		return iterableSerializableMapByCouchDB.get(index);
	}

	@Override
	public T set(int index, T element) {
		return iterableSerializableMapByCouchDB.put(index, element);
	}

	@Override
	public void add(int index, T element) {
		for (Integer x : iterableSerializableMapByCouchDB.keySet()) {
			
		}
	}

	@Override
	public T remove(int index) {
		return iterableSerializableMapByCouchDB.remove(index);
	}

	@Override
	public int indexOf(Object o) {
		for (Entry<Integer, T> x: iterableSerializableMapByCouchDB) {
			if (x.getValue().equals(o))
				return x.getKey();
		}
		return -1;
	}

	@Override
	public int lastIndexOf(Object o) {
		int idx = -1;
		for (Entry<Integer, T> x: iterableSerializableMapByCouchDB) {
			if (x.getValue().equals(o)) {
				if (x.getKey()>idx)
					idx = x.getKey();
			}
		}
		return idx;
	}

	@Override
	public ListIterator<T> listIterator() {
		return new LinkedListIteratorOnCouchDB<T>(this);
	}

	@Override
	public ListIterator<T> listIterator(int index) {
		return subList(index,Integer.MAX_VALUE).listIterator();
	}

	@Override
	public List<T> subList(int fromIndex, int toIndex) {
		/*Database database = null; // TODO from guice
		IterableMapPersisterByCouchDB<Integer, T> dcb = 
				new IterableMapPersisterByCouchDB<>(
						database, 
						RandomString.INSTANCE.randomString());*/
				
		IterableSerializableMapByCouchDB<Integer, T> dcb = 
			new IterableSerializableMapByCouchDB<Integer,T>(
				couchDBDocumentHelperTmp
			);
		for (Entry<Integer, T> x:dcb) {
			if (fromIndex <= x.getKey() && toIndex >= x.getKey()) {
				dcb.put(x.getKey(), x.getValue());
			}
		}
		return new LinkedListPersistableByCouchDB<T>(dcb, couchDBDocumentHelperTmp);
	}

	@Override
	public void commit() {
		iterableSerializableMapByCouchDB.couchDBCommit();
	}

}
package org.tweetsmining.persister.couchdb.listable;

import org.tweetsmining.io.SerializableList;
import org.tweetsmining.persister.couchdb.IterableSerializableMapByCouchDB;
import org.tweetsmining.persister.couchdb.helper.CouchDBDocumentHelper;
import org.tweetsmining.persister.couchdb.helper.CouchDBDocumentHelperTmp;
import org.tweetsmining.util.map.iterable.listable.IterableListableMap;

public class IterableMapListablePersisterByCouchDB<K,V> extends IterableSerializableMapByCouchDB<K, V> implements IterableListableMap<K, V> 
{

	private final CouchDBDocumentHelperTmp couchDBDocumentHelperTmp;

	public IterableMapListablePersisterByCouchDB(CouchDBDocumentHelper couchDBDocumentHelper, CouchDBDocumentHelperTmp couchDBDocumentHelperTmp) {
		super(couchDBDocumentHelper);
		this.couchDBDocumentHelperTmp = couchDBDocumentHelperTmp;
	}

	@Override
	public SerializableList<V> asList() {
		IterableSerializableMapByCouchDB<Integer, V> couchDBMap = 
				new IterableSerializableMapByCouchDB<Integer,V>(
				couchDBDocumentHelperTmp
				);
		LinkedListPersistableByCouchDB<V> linkedCouchDB = new LinkedListPersistableByCouchDB<V>(
				couchDBMap, couchDBDocumentHelperTmp
				);
		return linkedCouchDB;
	}
}

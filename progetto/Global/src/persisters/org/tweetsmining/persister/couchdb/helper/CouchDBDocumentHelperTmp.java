package org.tweetsmining.persister.couchdb.helper;

import javax.inject.Inject;

import org.tweetsmining.engines.wordsrelator.printer.Printer;
import org.tweetsmining.persister.couchdb._di.guice.provider.CouchDBProvider;
import org.tweetsmining.util.random.RandomString;

public class CouchDBDocumentHelperTmp extends CouchDBDocumentHelper {

	@Inject
	public CouchDBDocumentHelperTmp(CouchDBProvider couchDBProvider) {
		super(couchDBProvider, RandomString.INSTANCE.randomString());
	}
}

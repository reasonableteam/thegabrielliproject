package org.tweetsmining.persister.couchdb._di.guice.module;

import org.tweetsmining.persister.couchdb._di.guice.provider.CouchDBProvider;

import com.fourspaces.couchdb.Database;
import com.google.inject.AbstractModule;

public class PersisterModuleCouchDB extends AbstractModule {

	@Override
	protected void configure() {
		bind(Database.class).toProvider(CouchDBProvider.class);
	}
}

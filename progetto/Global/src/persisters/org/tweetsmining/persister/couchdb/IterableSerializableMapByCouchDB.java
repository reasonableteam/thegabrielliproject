/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.tweetsmining.persister.couchdb;


import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

import org.tweetsmining.io.Committable;
import org.tweetsmining.io.serialization.NullSerializer;
import org.tweetsmining.io.serialization.Serializer;
import org.tweetsmining.persister.base.map.IterableSerializableMap;
import org.tweetsmining.persister.couchdb.helper.CouchDBDocumentHelper;

import com.fourspaces.couchdb.Document;

/**
 *
 * @author gyankos
 * @param <K>
 * @param <V>
 */

public class IterableSerializableMapByCouchDB<K,V> 
	implements 
//	IterableMap<K,V>, SerializableMap<K,V>
	IterableSerializableMap<K, V>
//, Committable
{
    
    private final Document document;
	protected final CouchDBDocumentHelper couchDBDocumentHelper;
	private Serializer<K> kSer;
	private Serializer<V> vSer;

//	@Inject
    public IterableSerializableMapByCouchDB(CouchDBDocumentHelper couchDBDocumentHelper) {
		this.couchDBDocumentHelper = couchDBDocumentHelper;
		this.document = couchDBDocumentHelper.getDocument();
		this.kSer = new NullSerializer<>();
		this.vSer = new NullSerializer<>();
	}
    
    @Override
    public void setKeySerializer(Serializer<K> clazz) {
    	this.kSer = clazz;
    }
    
    @Override
    public void setValueSerializer(Serializer<V> clazz) {
    	this.vSer = clazz;
    }

    
    /*public CouchDBHelper(Database db, String specialization) {
//    	this.database = db;
//    	Document existantDocument = database.getDocument(specialization);
    	
    	if (existantDocument == null) {  
            document = new Document();
            document.setId(specialization);
    	}
    	else
    		document = existantDocument;
    }*/
    
    public String getSpecialization() {
    	return document.getId();
    }
    
//    @Override
    public void couchDBCommit() {
		couchDBDocumentHelper.commit();
    }
    
    @Override
    public int size() {
        return document.size();
    }

    @Override
    public boolean isEmpty() {
        return (size()==0);
    }

    @Override
    public boolean containsValue(Object val) {
        return document.containsValue(vSer.marshalling((V)val));
    }

    @Override
    public V get(K key) {
    	return vSer.unmarshalling(document.get(kSer.marshalling(key)));
    }
    

    @Override
    public V put(K key, V value) {
        return vSer.unmarshalling(document.put(kSer.marshalling(key), vSer.marshalling(value)));
    }
    
    @Override
    public void clear() {
        document.clear();
    }

   
    @Override
    public Iterator<Entry<K, V>> iterator() {
    	/*return ParIteratorFactory.createParIterator(
				document.entrySet(), 
				Config.THREADS);*/
        return new CouchDBMapIterator<K, V>(document,kSer,vSer);
    }

	@Override
	public Set<K> keySet() {
		HashSet<K> hsk = new HashSet<>();
		Iterator<String> is = document.keys();
		while (is.hasNext()) {
			hsk.add(kSer.unmarshalling(is.next()));
		}
		return hsk;
	}

	@Override
	public boolean containsKey(K key) {
		return document.containsKey(kSer.marshalling(key));
	}

	@Override
	public V remove(K key) {
		return vSer.unmarshalling(document.remove(kSer.marshalling(key)));
	}
}

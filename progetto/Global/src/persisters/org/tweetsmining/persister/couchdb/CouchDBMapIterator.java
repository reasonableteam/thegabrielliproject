/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.tweetsmining.persister.couchdb;

import java.util.Iterator;
import java.util.Map.Entry;

import org.tweetsmining.io.serialization.Serializer;
import org.tweetsmining.io.serialization.NullSerializer;
import org.tweetsmining.util.map.model.MapEntry;

import com.fourspaces.couchdb.Document;
/**
 *
 * @author gyankos
 */
public class CouchDBMapIterator<K, V>  implements Iterator<Entry<K,V>>{

    private final Document localcp;
    private final Iterator<String> couchDBDocumentKeysIterator;
    private final Serializer<K> keySerializer;
    private final Serializer<V> valueSerializer;
    
    private void silly_couchdb4j() {
        if (couchDBDocumentKeysIterator.hasNext())
        	couchDBDocumentKeysIterator.next();   //The first item is the document name. Silly
        if (couchDBDocumentKeysIterator.hasNext())
        	couchDBDocumentKeysIterator.next();   //The next one is the revision. More Silly
    }
    
    public CouchDBMapIterator(Document doc) {
        this.localcp = doc;
        this.couchDBDocumentKeysIterator = doc.keys();
        keySerializer = new NullSerializer<>();
        valueSerializer = new NullSerializer<>();
        silly_couchdb4j();
    }
    
    public CouchDBMapIterator(Document doc, Serializer<K> keySerializer, Serializer<V> valueSerializer) {
        this.localcp = doc;
        this.couchDBDocumentKeysIterator = doc.keys();
        this.keySerializer = keySerializer;
        this.valueSerializer = valueSerializer;
        silly_couchdb4j();
    }

    @Override
    public boolean hasNext() {
        return couchDBDocumentKeysIterator.hasNext();
    }

    @Override
    public MapEntry<K, V> next() {
    	String sKey = (String)couchDBDocumentKeysIterator.next();
        K key = keySerializer.unmarshalling(sKey);
        V get = valueSerializer.unmarshalling(localcp.get(sKey));
        return new MapEntry<>(key,get);
    }

    @Override
    public void remove() {
        couchDBDocumentKeysIterator.remove();
    }
    
}

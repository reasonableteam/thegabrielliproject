package org.tweetsmining.persister.couchdb.helper;


import net.sf.json.JSONException;

import org.tweetsmining.engines.wordsrelator.printer.Printer;
import org.tweetsmining.persister.couchdb._di.guice.provider.CouchDBProvider;

import com.fourspaces.couchdb.Database;
import com.fourspaces.couchdb.Document;

public class CouchDBDocumentHelper {
	
	private Document document;
	private final Database database;
	private final CouchDBProvider provider;
//	private final Printer printer;

	public CouchDBDocumentHelper(CouchDBProvider couchDBProvider, String documentName) {
//		this.printer = printer;
		database = couchDBProvider.get();
		provider = couchDBProvider;

		try {
			this.document = database.getDocument(documentName);
			System.out.println("get old document:" +document.getId());
		} catch (JSONException e) {
			document = new Document();
            document.setId(documentName);
            System.out.println("set a new document:" +document.getId());
		}
	}
	
	public CouchDBProvider getProvider() {
		return provider;
	}
	
	public Document getDocument() {
		return document;
	}
	
	public void commit() {
		database.saveDocument(document);
    }
}

package org.tweetsmining.persister.engines.wordrelator.couchdb;

import java.util.List;

import javax.inject.Inject;

import org.tweetsmining.dao.engines.wordsrelator.UserToRelatedWordsDAO;
import org.tweetsmining.io.serialization.JSONArrayListSerializer;
import org.tweetsmining.io.serialization.JSONObjectSerializationByReflection;
import org.tweetsmining.io.serialization.StringSerializeByReflection;
import org.tweetsmining.model.user.User;
import org.tweetsmining.model.wordsrelator.WordRelations;
import org.tweetsmining.persister.couchdb.IterableSerializableMapByCouchDB;
import org.tweetsmining.persister.engines.wordrelator.couchdb._di.guice.providers.CouchDBDocumentHelperForUserToRelatedWordsProvider;

public class UserToRelatedWordsPersisterByCouchDB extends IterableSerializableMapByCouchDB<User, List<WordRelations>> implements UserToRelatedWordsDAO {
	
	@Inject
	public UserToRelatedWordsPersisterByCouchDB(
			CouchDBDocumentHelperForUserToRelatedWordsProvider couchDBDocumentHelperForUserToRelatedWordsProvider) {
		super(couchDBDocumentHelperForUserToRelatedWordsProvider.get());
		setKeySerializer(new StringSerializeByReflection<>(User.class));
		setValueSerializer(new JSONArrayListSerializer<>(new JSONObjectSerializationByReflection<>(WordRelations.class)));
	}
	
	@Override
	public void commit() {
		super.couchDBCommit();		
	}
	
}

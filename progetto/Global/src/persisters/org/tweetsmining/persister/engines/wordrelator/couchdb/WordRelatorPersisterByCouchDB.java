package org.tweetsmining.persister.engines.wordrelator.couchdb;

import javax.inject.Inject;

import org.tweetsmining.dao.engines.wordsrelator.WordRelatorDAO;
import org.tweetsmining.io.serialization.JSONObjectSerializationByReflection;
import org.tweetsmining.model.wordsrelator.WordRelations;
import org.tweetsmining.persister.couchdb.IterableSerializableMapByCouchDB;
import org.tweetsmining.persister.engines.wordrelator.couchdb._di.guice.providers.CouchDBDocumentHelperForWordRelatorProvider;

public class WordRelatorPersisterByCouchDB extends IterableSerializableMapByCouchDB<String, WordRelations> implements WordRelatorDAO {

	@Inject
	public WordRelatorPersisterByCouchDB(CouchDBDocumentHelperForWordRelatorProvider couchDBDocumentHelperForWordRelatorProvider) {
		super(couchDBDocumentHelperForWordRelatorProvider.get());
		//Key serialize is null: it is already a string
		//Value is serialized via its type
		setValueSerializer(new JSONObjectSerializationByReflection<>(WordRelations.class));
	}
	
	@Override
	public void commit() {
		super.couchDBCommit();		
	}

}

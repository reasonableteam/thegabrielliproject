package org.tweetsmining.persister.engines.tweetsparser.couchdb._di.guice.provider;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.inject.Singleton;

import org.tweetsmining.engines.wordsrelator.printer.Printer;
import org.tweetsmining.persister.couchdb._di.guice.provider.CouchDBProvider;
import org.tweetsmining.persister.couchdb.helper.CouchDBDocumentHelper;

@Singleton
public class CouchDBDocumentHelperForUserToStemmedWordsProvider implements Provider<CouchDBDocumentHelper>{

	private final static String documentName = "UserToStemmedWordsDocument";
	private final CouchDBDocumentHelper couchDBDocumentHelper;

	@Inject
	public CouchDBDocumentHelperForUserToStemmedWordsProvider(CouchDBProvider couchDBProvider) {
		couchDBDocumentHelper = new CouchDBDocumentHelper(couchDBProvider, documentName);
	}

	@Override
	public CouchDBDocumentHelper get() {
		return couchDBDocumentHelper;
	}

}

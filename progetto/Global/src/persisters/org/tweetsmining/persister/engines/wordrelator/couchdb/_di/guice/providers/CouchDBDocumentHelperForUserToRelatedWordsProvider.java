package org.tweetsmining.persister.engines.wordrelator.couchdb._di.guice.providers;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.inject.Singleton;

import org.tweetsmining.engines.wordsrelator.printer.Printer;
import org.tweetsmining.persister.couchdb._di.guice.provider.CouchDBProvider;
import org.tweetsmining.persister.couchdb.helper.CouchDBDocumentHelper;

@Singleton
public class CouchDBDocumentHelperForUserToRelatedWordsProvider implements Provider<CouchDBDocumentHelper>{

	
	private final String documentName = "UserToRelatedWords";
	private final CouchDBDocumentHelper couchDBDocumentHelper;

	@Inject
	public CouchDBDocumentHelperForUserToRelatedWordsProvider(CouchDBProvider couchDBProvider) {
		couchDBDocumentHelper = new CouchDBDocumentHelper(couchDBProvider, documentName);
	}

	@Override
	public CouchDBDocumentHelper get() {
		return couchDBDocumentHelper;
	}

}

package org.tweetsmining.persister.engines.tweetsparser.couchdb;

import java.util.Set;

import javax.inject.Inject;

import org.tweetsmining.dao.engines.tweetsparser.UserToStemmedWordsDAO;
import org.tweetsmining.io.serialization.JSONArraySetSerializer;
import org.tweetsmining.io.serialization.NullSerializer;
import org.tweetsmining.io.serialization.StringSerializeByReflection;
import org.tweetsmining.model.user.User;
import org.tweetsmining.persister.couchdb.IterableSerializableMapByCouchDB;
import org.tweetsmining.persister.engines.tweetsparser.couchdb._di.guice.provider.CouchDBDocumentHelperForUserToStemmedWordsProvider;

public class UserToStemmedWordsPersisterByCouchDB extends IterableSerializableMapByCouchDB<User, Set<String>> implements UserToStemmedWordsDAO {

	@Inject
	public UserToStemmedWordsPersisterByCouchDB(CouchDBDocumentHelperForUserToStemmedWordsProvider couchDBDocumentHelperForUserToStemmedWords) {
		super(couchDBDocumentHelperForUserToStemmedWords.get());
		
		//Installs the serialization
		setKeySerializer(new StringSerializeByReflection<>(User.class));
		setValueSerializer(new JSONArraySetSerializer<String>(new NullSerializer<String>()));
	}
	
	@Override
	public void commit() {
		super.couchDBCommit();
	}
	
	
	/*@Override
	public Set<String> update(User user, Set<String> stemmedWords) {
		return super.put(user, stemmedWords);
	}*/

	/*@Override
	public boolean containsValue(Set<String> value) {
		return super.containsValue(value);
	}*/

}

package org.tweetsmining.persister.engines.wordrelator.couchdb._di.guice.providers;

import javax.inject.Inject;

import org.tweetsmining.persister.couchdb._di.guice.provider.CouchDBProvider;
import org.tweetsmining.persister.couchdb.helper.CouchDBDocumentHelper;

import com.google.inject.Provider;

public class CouchDBDocumentHelperForWordRelatorProvider implements
		Provider<CouchDBDocumentHelper> {
	
	private final static String documentName = "WordRelator";
	private final CouchDBDocumentHelper couchDBDocumentHelper;

	@Inject
	public CouchDBDocumentHelperForWordRelatorProvider(CouchDBProvider couchDBProvider) {
		couchDBDocumentHelper = new CouchDBDocumentHelper(couchDBProvider, documentName);
	}

	@Override
	public CouchDBDocumentHelper get() {
		return couchDBDocumentHelper;
	}

}

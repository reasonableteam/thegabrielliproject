/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.tweetsmining.engines.graphsminer.gmatrix.utils;


/**
 *
 * @author gyankos
 */
public class PerRowPair implements Comparable<PerRowPair> {

    private int i;
    private int j;
    private int dove;
    
    public PerRowPair(int i, int j) {
        this.i = i;
        this.j = j;
        this.dove = (i+((i+j)*(i+j+1))/2);
    }
    
    public int getDovetailingCode() {
        return dove;
    }
    
    @Override
    public String toString() {
        return "<"+Integer.toString(i)+","+Integer.toString(j)+">";
    }
    
    public int getRow() {
        return i;
    }
    public int getCol() {
        return j;
    }
    
    @Override
    public int compareTo(PerRowPair right) {
            if (i<right.getRow())
                return -1;
            else if (i>right.getRow())
                return 1;
            else  {
                if (j<right.getCol()) 
                    return -1;
                else if (j>right.getCol())
                    return 1;
                else
                    return 0;
            }
    }
    
}

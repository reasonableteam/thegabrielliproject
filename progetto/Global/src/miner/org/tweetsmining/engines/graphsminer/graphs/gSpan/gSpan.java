/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.tweetsmining.engines.graphsminer.graphs.gSpan;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.tweetsmining.engines.graphsminer.graphs.Abstract_gSpan;
import org.tweetsmining.engines.graphsminer.graphs.IgSpanGraphs;
import org.tweetsmining.engines.graphsminer.graphs.DFS.AbstractDBVisit;
import org.tweetsmining.engines.graphsminer.graphs.DFS.DFS;
import org.tweetsmining.model.graph.MultiLayerGraph;
import org.tweetsmining.model.graph.database.Entity;

/**
 *
 * @author gyankos
 */
public class gSpan extends Abstract_gSpan<GSCode> {

    public static Collection<IgSpanGraphs<GSCode>> CollectionOf_gSpanGraphToAbstract(Collection<gSpanGraph> tmp) {
        Collection<IgSpanGraphs<GSCode>> tt = new LinkedList<>();
        for (gSpanGraph x : tmp) {
            tt.add(x);
        }
        return tt;
    }

    public gSpan() {
        super(SimpleSubgraphOf.getInstance());
    }

    public static List<Entity> getCommonEntities(IgSpanGraphs<GSCode> mlg) {
        AbstractDBVisit<List<Entity>> adbv_rightmost = new SimpleRightmostExpansion();
        DFS<List<Entity>> d = new DFS(mlg.getRepresentation(), adbv_rightmost);
        return d.start();
    }
    
    @Override

    public List<Entity> getEntities(final IgSpanGraphs<GSCode> mlg) {
        return getCommonEntities(mlg);
    }

    @Override
    public IgSpanGraphs<GSCode> createCodeGraph(MultiLayerGraph m) {
        return new gSpanGraph(m);
    }

}

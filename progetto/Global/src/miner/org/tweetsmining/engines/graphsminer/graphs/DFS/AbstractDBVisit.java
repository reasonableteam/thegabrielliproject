/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.tweetsmining.engines.graphsminer.graphs.DFS;

import java.util.Collection;
import java.util.SortedSet;

import org.tweetsmining.model.graph.database.ERTriple;
import org.tweetsmining.model.graph.database.Entity;
import org.tweetsmining.model.graph.database.Relation;

/**
 * Concretization of the Visit algorithm for RDF-Model Graphs
 * @author gyankos
 */
public abstract class AbstractDBVisit<T> extends AbstractAlgorithmOverVisit<Entity, Relation, T> {

    /**
     * This function is called when we want to choose the next edge to visit
     * in order to reach another vertex. In order to do so, we could manipulate
     * the input list in order to filter the edges to visit and to choose which
     * one to visit first. 
     * @param cert  The edges that could be visited ordered in lexicographic order
     * @return 
     */
    public abstract Collection<ERTriple> getEdgeVisitOrder(SortedSet<ERTriple> cert);
    
}

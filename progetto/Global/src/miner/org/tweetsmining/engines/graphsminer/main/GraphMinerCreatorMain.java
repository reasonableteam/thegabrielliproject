package org.tweetsmining.engines.graphsminer.main;

import java.util.List;
import java.util.Map.Entry;

import javax.inject.Inject;

import org.tweetsmining.dao.engines.graphsminer.UserToGraphDAO;
import org.tweetsmining.dao.engines.graphsminer.WordPositionDAO;
import org.tweetsmining.dao.engines.wordsrelator.UserToRelatedWordsDAO;
import org.tweetsmining.engines.graphsminer.Database.Word;
import org.tweetsmining.engines.graphsminer.mining.MineUsers;
import org.tweetsmining.model.graph.MultiLayerGraph;
import org.tweetsmining.model.graph.database.Entity;
import org.tweetsmining.model.graph.database.Relation;
import org.tweetsmining.model.graphsminer.Cluster;
import org.tweetsmining.model.user.User;
import org.tweetsmining.model.wordsrelator.RelationedTerm;
import org.tweetsmining.model.wordsrelator.WordRelations;

import com.google.inject.Guice;
import com.google.inject.Injector;

public class GraphMinerCreatorMain {
	
	private final UserToRelatedWordsDAO userToRelatedWordsDAO;
	private final UserToGraphDAO userToGraphDAO;
	private final WordPositionDAO wordPositionMap;
	private final MineUsers mineUsers;
	
	@Inject
	public GraphMinerCreatorMain
		(UserToRelatedWordsDAO userToRelatedWordsDAO,
				UserToGraphDAO userToGraphDAO,
				WordPositionDAO wordPositionMap,
				MineUsers mineUsers) {
		this.userToRelatedWordsDAO = userToRelatedWordsDAO;
		this.userToGraphDAO = userToGraphDAO;
		this.wordPositionMap = wordPositionMap;
		this.mineUsers = mineUsers;
	}
	
	public void doDataMining() {
		List<Cluster> getCluster = mineUsers.dbToCluster();
		for (Cluster x : getCluster) {
			System.out.println(x.getListUsers());
		}
		mineUsers.mineDifferences(getCluster);
	}
	
	public void createUserGraphs() {
		for (Entry<User, List<WordRelations>> userToRelated : userToRelatedWordsDAO) {
			User user = userToRelated.getKey();
			List<WordRelations> relations = userToRelated.getValue();
			MultiLayerGraph userGraph = new MultiLayerGraph();
			System.out.println(user.toString());
			
			//Creating the user graph
			for (WordRelations stemmedword: relations) {
				System.out.println("~~ " + stemmedword.getWord() + " --> " + stemmedword.getRelationType().getUrl());
				Entity word = userGraph.addVertex(Word.getWordUrl(stemmedword.getWord()));
				Relation senseRelation = userGraph.newLinkType(stemmedword.getRelationType().getUrl());
				for (String sense : stemmedword.getMainsenses()) {
					//Linking each word to its sense
					Entity nodeSense = userGraph.addVertex(Word.getWordUrl(sense));
					userGraph.addTriple(word, senseRelation, nodeSense, 1);
					
					for (RelationedTerm relatedToSense : stemmedword.getTermsForMainsense(sense)) {
						if (relatedToSense == null)
							System.out.println("badass");
						Entity leafWord = userGraph.addVertex(Word.getWordUrl(relatedToSense.getTerm()));
						Relation relatedToLeafWith = userGraph.newLinkType(relatedToSense.getRelationType().getUrl());
						userGraph.addTriple(nodeSense, relatedToLeafWith, leafWord, 1);
					}
				}
			}
			
			userToGraphDAO.put(user, userGraph);
			
		}
		userToGraphDAO.commit();
	}
	
	public static void main(String args[]) {
		Injector injector = Guice.createInjector( new GraphMinerCreatorModule());
		GraphMinerCreatorMain self = injector.getInstance(GraphMinerCreatorMain.class);
		//self.createUserGraphs(); // phase 1
		self.doDataMining();
		
	}

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.tweetsmining.engines.graphsminer.matrices;

import java.util.Set;

import org.apache.commons.collections4.keyvalue.MultiKey;
import org.tweetsmining.model.matrices.GMatrix;
import org.tweetsmining.model.matrices.IMatrix;

import Jama.Matrix;

/**
 *
 * @author gyankos
 */
public abstract class MatrixOp {
    
        public static int getRow(MultiKey x) {
        if (x==null)
            return -1;
        else
            return (Integer)x.getKey(0);
    }
    
    public static int getCol(MultiKey x) {
        if (x==null)
            return -1;
        else
            return (Integer)x.getKey(1);
    }
       
    
    
   /**
    * Matrix sum
    * @param left
    * @param right
    * @return
    */
   public static IMatrix sum(IMatrix left, IMatrix right) {
       Set<MultiKey> iter = left.getValueRange();
       iter.addAll(right.getValueRange());
       IMatrix g = new GMatrix();
       for (MultiKey x : iter) {
           g.set(x,left.get(x)+right.get(x));
       }
       return g;
   }
   
   /**
    * MAtrix difference
    * @param left
    * @param right
    * @return
    */
   public static IMatrix diff(IMatrix left, IMatrix right) {
       Set<MultiKey> iter = left.getValueRange();
       iter.addAll(right.getValueRange());
       IMatrix g = new GMatrix();
       for (MultiKey x : iter) {
           g.set(x,left.get(x)-right.get(x));
       }
       return g;
   }
   
   /**
    * Matrix product
    * @param left
    * @param right
    * @return 
    */
   public static IMatrix prod(IMatrix left, IMatrix right) {
       IMatrix g = new GMatrix();
       for (MultiKey l : left.getValueRange()) {
           int li = (Integer)l.getKey(1);
           for (MultiKey r : right.getValueRange()) {
               int ri = (Integer)r.getKey(0);
               if (li==ri) {
                   double lv = (Double)left.get(l);
                   double rv = (Double)right.get(r);
                   //System.out.println(lv+" "+rv+" "+lv*rv);
                   g.incr((Integer)l.getKey(0),(Integer)r.getKey(1),lv*rv);
               }
           }
       }
       return g;
   }
   
   
   /**
    * Matrix divide
    * @param left
    * @param r
    * @return
    */
   public static IMatrix div(IMatrix left, double r) {
       IMatrix g = new GMatrix();
       for (MultiKey l : left.getValueRange()) {
               int li = (Integer)l.getKey(0);
               int ri = (Integer)l.getKey(1);
               g.incr(li,ri,(left.get(l)/r));
       }
       return g;
   }
   
     /**
    * Matrix divide
    * @param left
    * @param right
    * @return
    */
   public static IMatrix div(IMatrix left, IMatrix right) {
       IMatrix g = new GMatrix();
       for (MultiKey l : left.getValueRange()) {
           for (MultiKey r : right.getValueRange()) {
               int li = (Integer)l.getKey(1);
               int ri = (Integer)r.getKey(0);
               if (li==ri) {
                   double lv = (Double)left.get(l);
                   double rv = (Double)right.get(r);
                   //System.out.println(lv+" "+rv+" "+lv*rv);
                   g.incr((Integer)l.getKey(0),(Integer)r.getKey(1),lv/rv);
               }
           }

       }
       return g;
   }
   

   /**
    * Matrix transpose
    * @param m
    * @return
    */
   public static IMatrix transpose(IMatrix m)  {
       IMatrix g = new GMatrix();
       for (MultiKey k: m.getValueRange()) {
               int li = (Integer)k.getKey(0);
               int ri = (Integer)k.getKey(1);
               g.set(ri, li, m.get(k));
       }
       return g;
   }
   
   public static IMatrix stationaryDistribution(IMatrix m) {
       Matrix tmp = MatrixFactory.toMatrix(m);
       int N = tmp.getColumnDimension();
       Matrix B = tmp.minus(Matrix.identity(N, N));
       for (int j = 0; j < N; j++)
           B.set(0, j, 1.0);
       Matrix b = new Matrix(N, 1);
       b.set(0, 0, 1.0);
       return MatrixFactory.toGMatrix(B.solve(b));
   }
   
   public static IMatrix diagonal(double... d) {
       IMatrix tmp = new GMatrix();
       for (int i=0; i<d.length; i++)
           tmp.set(i,i, d[i]);
       return tmp;
   }
   
   public static IMatrix diagonal(double val, int size) {
       IMatrix tmp = new GMatrix();
       for (int i=0; i<size; i++)
           tmp.set(i,i, val);
       return tmp;
   }
   
   public static double[] rowSums(IMatrix m) {
       int size = Math.max(m.nCols(), m.nRows());
       double toret[] = new double[size];
       for (int i=0; i<size; i++)
           toret[i]=0;
       for (int i=0; i<size; i++)
           for (int j=0; j<size; j++)
               toret[i] += m.get(i,j);
       return toret;
   }
   
   public static IMatrix regularizedLaplacianMatrix(IMatrix m) {
       IMatrix tmp = MatrixOp.prod(m, MatrixOp.transpose(m));
       int size = tmp.nCols();
       IMatrix i = diagonal(1,size);
       IMatrix d = diagonal(rowSums(tmp));
       IMatrix laplacian = new GMatrix();
       for (int ii = 0; ii<size; ii++)
           for (int ji = 0; ji<size; ji++)
               laplacian.set(ii, ji, (d.get(ii, ji)-tmp.get(ii, ji))/ Math.sqrt(d.get(ii, ii)*d.get(ji, ji)));
       return laplacian;
   }
    
}

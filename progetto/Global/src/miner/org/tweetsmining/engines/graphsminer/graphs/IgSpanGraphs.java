/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.tweetsmining.engines.graphsminer.graphs;

import java.util.SortedSet;

import org.tweetsmining.model.graph.MultiLayerGraph;
import org.tweetsmining.model.graph.database.ERTriple;
import org.tweetsmining.model.graph.database.Entity;

/**
 *
 * @author gyankos
 * @param <TypeCodeEncoding>
 */
public interface IgSpanGraphs<TypeCodeEncoding>  {
   
    public Entity getFirstNode();
    
    public SortedSet<ERTriple> getAllEdges();
    
    public TypeCodeEncoding getACode();
    
    public MultiLayerGraph getRepresentation();
    
    @Override
    public boolean equals(Object o);
    
}
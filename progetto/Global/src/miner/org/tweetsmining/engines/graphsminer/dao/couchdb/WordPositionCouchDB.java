package org.tweetsmining.engines.graphsminer.dao.couchdb;

import javax.inject.Inject;

import org.tweetsmining.dao.engines.graphsminer.WordPositionDAO;
import org.tweetsmining.io.serialization.StringSerializeByReflection;
import org.tweetsmining.persister.couchdb.helper.CouchDBDocumentHelperTmp;
import org.tweetsmining.persister.couchdb.listable.IterableMapListablePersisterByCouchDB;

public class WordPositionCouchDB extends IterableMapListablePersisterByCouchDB<String,Integer> implements WordPositionDAO {

	@Inject
	public WordPositionCouchDB(CouchDBDocumentHelperForUserToGraphProvider couchDBDocumentHelperForUserToGraphProvider
			/*, CouchDBProvider couchDBProvider*/
			,CouchDBDocumentHelperTmp couchDocumentHelperTmp
			) {
		super(couchDBDocumentHelperForUserToGraphProvider.get(), couchDocumentHelperTmp);
		setKeySerializer(new StringSerializeByReflection<>(String.class));
		setValueSerializer(new StringSerializeByReflection<>(Integer.class));
	}
	
	@Override
	public void commit() {
		super.couchDBCommit();		
	}
	
}

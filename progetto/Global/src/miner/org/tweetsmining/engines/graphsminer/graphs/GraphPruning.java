/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.tweetsmining.engines.graphsminer.graphs;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.tweetsmining.dao.engines.graphsminer.UserToGraphDAO;
import org.tweetsmining.dao.engines.graphsminer.WordPositionDAO;
import org.tweetsmining.model.graph.MultiLayerGraph;
import org.tweetsmining.model.graph.database.Entity;
import org.tweetsmining.model.user.User;

/**
 *
 * @author gyankos
 */
public class GraphPruning<T> {

	private WordPositionDAO vertexCount;
	
	public GraphPruning(WordPositionDAO mapStrToInt) {
		this.vertexCount = mapStrToInt;
	}

    public WordPositionDAO getVerticesFrequencies(UserToGraphDAO database) {

        for (Entry<User, MultiLayerGraph> ml : database) {
            for (Entity x : ml.getValue().getVertices()) {
                String uri = x.getFullUri();
                if (!vertexCount.containsKey(uri)) {
                    vertexCount.put(uri, 1);
                } else {
                    vertexCount.put(uri, vertexCount.get(uri) + 1);
                }
            }
        }
        return vertexCount;
    }

    public static int getMode(WordPositionDAO values) {
        HashMap<Integer, Integer> freqs = new HashMap<>();

        for (Entry<String, Integer> x : values) {
        	Integer val = x.getValue();
            Integer freq = freqs.get(x);
            freqs.put(val, (freq == null ? 1 : freq + 1));
        }

        int mode = 0;
        int maxFreq = 0;

        for (Map.Entry<Integer, Integer> entry : freqs.entrySet()) {
            int freq = entry.getValue();
            if (freq > maxFreq) {
                maxFreq = freq;
                mode = entry.getKey();
            }
        }

        return mode;
    }
    
    
    public void getVerticesAboveMode(UserToGraphDAO db) {
        double mode = getMode(getVerticesFrequencies(db));
        mode = mode / ((double)db.size());
        dbWithFrequentVertices(db, mode);
    }

    /**
     * Removes from the database the most unfrequent vertices.
     *
     * @param database Graph database seen as a list of MultiLayerGraph
     * @param thereshold frequency below which remove the vertex from all the
     * graphs
     * @return
     */
    public UserToGraphDAO dbWithFrequentVertices(UserToGraphDAO database, double thereshold) {
    	WordPositionDAO count = getVerticesFrequencies(database);
        if (thereshold <= 0) {
            thereshold = Double.MIN_VALUE;
        }

        for (Entry<String, Integer> x : count) {
            double calc = ((double) x.getValue()) / ((double) database.size());
            System.out.println(x.getKey() + " " + calc);
            if (calc < thereshold) {
                final String k = x.getKey();
                for (Entry<User, MultiLayerGraph> x1: database) {
                	MultiLayerGraph input = x1.getValue();
                	input.removeVertex(k);
                }
                for (User kkey : database.keySet()) {
                    MultiLayerGraph input = database.get(kkey);
                    input.removeVertex(k);
                    database.put(kkey, input);
                }
            }
        }

        return database;
    }

    /**
     * Removes the edges that have a link that is below the thereshold
     *
     * @param database
     * @param thereshold
     * @return
     *
    public static Collection<SerializableMultiLayerGraph> strongestEdges(Collection<SerializableMultiLayerGraph> database, final double thereshold) {
        return Collections2.transform(database, new Function<SerializableMultiLayerGraph, SerializableMultiLayerGraph>() {
            @Override
            //For each MultiLayerGraph
            public SerializableMultiLayerGraph apply(SerializableMultiLayerGraph f) {
                //And for each of its layers
                for (IAdjacencyGraph<Entity> x : f.getLayers()) {
                    try {
                        for (MultiKey y : x.getValueRange()) {
                            if (x.get(y) < thereshold) {
                                x.rem((Integer) y.getKey(0), (Integer) y.getKey(1));
                            }
                        }
                    } catch (Throwable ex) {
                        Logger.getLogger(GraphPruning.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                return f;
            }

        });
    }*/

}

package org.tweetsmining.engines.graphsminer.dao.couchdb;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.tweetsmining.persister.couchdb._di.guice.provider.CouchDBProvider;
import org.tweetsmining.persister.couchdb.helper.CouchDBDocumentHelper;

import com.google.inject.Provider;

@Singleton
public class CouchDBDocumentHelperForUserToGraphProvider implements Provider<CouchDBDocumentHelper> {
	
	private final static String documentName = "UserToGraph";
	private final CouchDBDocumentHelper couchDBDocumentHelper;

	@Inject
	public CouchDBDocumentHelperForUserToGraphProvider(CouchDBProvider couchDBProvider) {
		couchDBDocumentHelper = new CouchDBDocumentHelper(couchDBProvider, documentName);
	}

	@Override
	public CouchDBDocumentHelper get() {
		return couchDBDocumentHelper;
	}
}
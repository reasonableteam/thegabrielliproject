package org.tweetsmining.engines.graphsminer.dao.couchdb;

import javax.inject.Inject;

import org.tweetsmining.dao.engines.graphsminer.UserToGraphDAO;
import org.tweetsmining.io.serialization.StringSerializeByReflection;
import org.tweetsmining.model.graph.MultiLayerGraph;
import org.tweetsmining.model.user.User;
import org.tweetsmining.persister.couchdb.helper.CouchDBDocumentHelperTmp;
import org.tweetsmining.persister.couchdb.listable.IterableMapListablePersisterByCouchDB;

public class UserToGraphCouchDB extends IterableMapListablePersisterByCouchDB<User, MultiLayerGraph> implements UserToGraphDAO {

	@Inject
	public UserToGraphCouchDB(CouchDBDocumentHelperForUserToGraphProvider couchDBDocumentHelperForUserToGraphProvider, CouchDBDocumentHelperTmp couchDBDocumentHelperTmp) {
		super(couchDBDocumentHelperForUserToGraphProvider.get(), couchDBDocumentHelperTmp);
		setKeySerializer(new StringSerializeByReflection<>(User.class));
		setValueSerializer(new StringSerializeByReflection<>(MultiLayerGraph.class));
	}
	
	@Override
	public void commit() {
		super.couchDBCommit();		
	}
}

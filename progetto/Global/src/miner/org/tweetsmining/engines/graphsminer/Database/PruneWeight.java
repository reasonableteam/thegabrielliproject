package org.tweetsmining.engines.graphsminer.Database;


import org.tweetsmining.model.graph.AbstractMultiLayerGraph;

import edu.cmu.lti.ws4j.impl.Lin;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ UNUSED ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * @author gyankos
 */
public class PruneWeight {
    
    Lin lin;
    
    public PruneWeight(AbstractMultiLayerGraph mlg) {
        MultiLayerDB mldb = new MultiLayerDB(mlg);
        lin = new Lin(mldb);
    }
    
    public double similarity(String word1, String word2) {
        return lin.calcRelatednessOfWords(word1, word2);
    }
    
}

package org.tweetsmining.engines.graphsminer.main;

import org.tweetsmining.config._di.guice.ConfigModule;
import org.tweetsmining.dao.engines.graphsminer.ClusterToSubgraphsDAO;
import org.tweetsmining.dao.engines.graphsminer.UserToGraphDAO;
import org.tweetsmining.dao.engines.graphsminer.WordPositionDAO;
import org.tweetsmining.engines.graphsminer.dao.couchdb.ClusterToSubgraphsCouchDB;
import org.tweetsmining.engines.graphsminer.dao.couchdb.UserToGraphCouchDB;
import org.tweetsmining.engines.graphsminer.dao.couchdb.WordPositionCouchDB;

import com.google.inject.AbstractModule;

// just for test
public class GraphMinerCreatorModule extends AbstractModule {

	@Override
	public void configure() {
		
		//General configurations
		install( new ConfigModule() );
		
		//
		bind(UserToGraphDAO.class).to(UserToGraphCouchDB.class);
		bind(ClusterToSubgraphsDAO.class).to(ClusterToSubgraphsCouchDB.class);
		bind(WordPositionDAO.class).to(WordPositionCouchDB.class);

	}

}

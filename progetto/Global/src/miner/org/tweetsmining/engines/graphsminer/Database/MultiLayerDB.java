/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.tweetsmining.engines.graphsminer.Database;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Set;

import org.tweetsmining.model.graph.AbstractMultiLayerGraph;
import org.tweetsmining.model.graph.database.Entity;
import org.tweetsmining.model.graph.database.Relation;

import com.google.common.base.Function;
import com.google.common.collect.Collections2;

import edu.cmu.lti.lexical_db.ILexicalDatabase;
import edu.cmu.lti.lexical_db.data.Concept;

/**
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ UNUSED ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * @author gyankos
 */
public class MultiLayerDB implements ILexicalDatabase {
    
    public final String HYPERNYM_RELATION = "http://our.project/relation/hypernym";
    public final String SYNSET_RELATION = "http://our.project/relation/synset";
    public final String SEMANTICALLY_RELATION = "http://our.project/relation/semrel";
    public final String DISAMBIGUATE_RELATION = "http://tweetsmining.org/relation/disambiguation";
    
    public static String getUrlFromName(String term) {
        return term; //TODO: convertire (Computer vecchio)
    }
    
    private AbstractMultiLayerGraph mlg;
    
    public MultiLayerDB(AbstractMultiLayerGraph db) {
        this.mlg = db;
    }

    /**
     * Returns the concept derived from word that has the majority of incoming links
     * @param word
     * @param pos (ignoring the word's pos: this argument is not used)
     * @return 
     */
    @Override
    public Concept getMostFrequentConcept(String word, String pos) {
        Collection<String> conc = 
        Collections2.transform(getAllConcepts(word,pos), new Function<Concept,String>() {
            @Override
            public String apply(Concept f) {
                return f.getName();
            }
        });
        HashMap<String, Integer> tm = new HashMap<>();
        for (String concept : conc) {
            Entity w = mlg.getVertex(Word.WORD_URI + word);
            if (tm.containsKey(concept)) {
                tm.put(concept, tm.get(concept)+mlg.getInSet(w, null).size());
            } else
                tm.put(concept,mlg.getInSet(w, null).size());
        }
        int count = -1;
        String key = null;
        for (Entry<String,Integer> e:tm.entrySet()) {
            if (e.getValue()>count) {
                count = e.getValue();
                key = e.getKey();
            }
        }
        return strToConcept(key);
    }

    /**
     * TODO
     * @param word
     * @param posText (ignoring the word's pos: this argument is not used)
     * @return 
     */
    @Override
    public Collection<Concept> getAllConcepts(String word, String posText) {
        Relation prop = mlg.getRelation(SYNSET_RELATION);
        Entity w = mlg.getVertex(Word.WORD_URI + word);
        Set<Concept> sconc = new HashSet<>();
        for (Relation dprop : mlg.getRelations()) {
            for (Entity conc : mlg.getOutSet(w, dprop)) {
                sconc.addAll(Collections2.transform(mlg.getOutSet(conc, prop), new Function<Entity,Concept> () {
                @Override
                public Concept apply(Entity f) {
                    return strToConcept(Word.getStringWordFromEntity(f));
                }
                }));
            }
        }
        return sconc;
    }

    /**
     * Returns the first is-a relations
     * @param word
     * @return 
     */
    @Override
    public Collection<String> getHypernyms(String word) {
        Relation prop = mlg.getRelation(HYPERNYM_RELATION);
        Entity w = mlg.getVertex(Word.WORD_URI + word);
        Relation dprop = mlg.getRelation(DISAMBIGUATE_RELATION);
        Set<String> sconc = new HashSet<>();
        for (Entity conc : mlg.getOutSet(w, dprop)) {
            sconc.addAll(Collections2.transform(mlg.getOutSet(conc, prop), new Function<Entity,String> () {
            @Override
            public String apply(Entity f) {
                return Word.getStringWordFromEntity(f);
            }
            }));
        }
        return sconc;
    }

    @Override
    public Concept findSynsetBySynset(String string) {
        return strToConcept(string);
    }

    /**
     * Bogus
     * @param concept
     * @return 
     */
    @Override
    public String conceptToString(String concept) {
        return concept;
    }

    public static Concept strToConcept(String str) {
        return new Concept(str);
    }
    
    /**
     * This different implementation returns all the semantically related objects
     * @param cncpt
     * @param string
     * @return 
     */
    @Override
    public Collection<String> getGloss(Concept cncpt, String string) {
        Entity w = mlg.getVertex(Word.WORD_URI + cncpt.getSynset());
        Relation prop = mlg.getRelation(DISAMBIGUATE_RELATION);
        
        return Collections2.transform(mlg.getOutSet(w, prop),new Function<Entity,String>() {
            @Override
            public String apply(Entity f) {
                return Word.getStringWordFromEntity(f);
            }
        });
    }
    
}

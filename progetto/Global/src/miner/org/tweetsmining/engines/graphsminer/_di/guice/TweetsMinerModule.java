package org.tweetsmining.engines.graphsminer._di.guice;


import java.net.URL;
import java.util.Properties;

import org.tweetsmining.dao.engines.graphsminer.ClusterToSubgraphsDAO;
import org.tweetsmining.dao.engines.graphsminer.UserToGraphDAO;
import org.tweetsmining.dao.engines.graphsminer.WordPositionDAO;
import org.tweetsmining.engines.graphsminer.dao.couchdb.ClusterToSubgraphsCouchDB;
import org.tweetsmining.engines.graphsminer.dao.couchdb.UserToGraphCouchDB;
import org.tweetsmining.engines.graphsminer.dao.couchdb.WordPositionCouchDB;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.name.Names;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;

public class TweetsMinerModule extends AbstractModule {

	private static Properties loadProperties() throws Exception {
		 Properties properties = new Properties();
		 ClassLoader loader = TweetsMinerModule.class.getClassLoader();
		 URL url = loader.getResource("config.properties");
		 properties.load(url.openStream());
		 return properties;
	}
	
	@Override
	protected void configure() {
		bind(ClusterToSubgraphsDAO.class).to(ClusterToSubgraphsCouchDB.class);
		bind(UserToGraphDAO.class).to(UserToGraphCouchDB.class);
		bind(WordPositionDAO.class).to(WordPositionCouchDB.class);
		try {
	           Properties props = loadProperties();
	           Names.bindProperties(binder(),props);
	    } catch (Exception e) {
	           e.printStackTrace();
	    }
	}
	
	@Provides
	public Model providesModel() {
		 return ModelFactory.createDefaultModel();
	}

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.tweetsmining.engines.graphsminer.graphs.DFS;

import java.util.Collections;
import java.util.List;

/**
 * Generic abstraction of the oprations that coul be performed over a search algorithm
 * 
 * @author gyankos
 * @param <Vertex>
 * @param <LayerName>
 * @param <Output>
 */
public abstract class AbstractAlgorithmOverVisit<Vertex extends Comparable<Vertex>,LayerName extends Comparable<LayerName>,Output> {
    
    /**
     * Perform a visit over the vertex v
     * @param v 
     */
    public abstract void visitVertex(Vertex v);
    
    /**
     * Sorts the vertices in ascending order
     * 
     * @param els
     * @return 
     */
    public List<Vertex> sortAscendingVertices(List<Vertex> els) {
       Collections.sort(els);
       return els;
    }
    
    /**
     * Visiting the current edge
     * @param src   Source Vertex
     * @param l     Link
     * @param dst   Destination Vertex
     */
    public abstract void visitEdge(Vertex src, LayerName l, Vertex dst);
    
    /**
     * Sorte the edges in ascending order
     * @param els
     * @return 
     */
    public List<LayerName> sortAscendingEdges(List<LayerName> els) {
       Collections.sort(els);
       return els;
    }
    
    /**
     * Returns the output at the end of the visit algorithm
     * @return 
     */
    public abstract Output getOutput();
    
}

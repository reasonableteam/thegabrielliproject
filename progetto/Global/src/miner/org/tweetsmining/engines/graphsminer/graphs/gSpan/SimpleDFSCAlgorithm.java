/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.tweetsmining.engines.graphsminer.graphs.gSpan;

import org.tweetsmining.engines.graphsminer.gmatrix.utils.NonCompPair;
import org.tweetsmining.engines.graphsminer.graphs.AbstractCodeGenerators;
import org.tweetsmining.engines.graphsminer.graphs.DFS.DFS;
import org.tweetsmining.model.graph.AbstractMultiLayerGraph;
import org.tweetsmining.model.graph.database.Entity;

/**
 *
 * @author gyankos
 */
public class SimpleDFSCAlgorithm extends AbstractCodeGenerators<GSCode> {
    
    @Override
    public NonCompPair<GSCode, Entity> getADFSCode(AbstractMultiLayerGraph ml) {
        SimpleDFSCodeGenerator e = new SimpleDFSCodeGenerator();
        DFS<GSCode> d = new DFS<>(ml, e);
        GSCode dfsc = d.start();
        if (e.getOutput() == null || (e.getOutput().isEmpty() && (!dfsc.isEmpty()))) {
            throw new RuntimeException("Java SuX");
        }
        Entity firstNode;
        if (e.getFirst() == null) {
            firstNode = null;
        } else {
            firstNode = e.getFirst().getSource();
        }
        return new NonCompPair<>(dfsc, firstNode);
    }
    
}

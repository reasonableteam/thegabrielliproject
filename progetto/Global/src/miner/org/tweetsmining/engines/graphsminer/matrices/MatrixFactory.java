/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.tweetsmining.engines.graphsminer.matrices;

import org.apache.commons.collections4.keyvalue.MultiKey;
import org.tweetsmining.model.matrices.GMatrix;
import org.tweetsmining.model.matrices.IMatrix;

import Jama.Matrix;

/**
 *
 * @author gyankos
 */
public class MatrixFactory {
    
    public static GMatrix toGMatrix(Matrix m) {
        GMatrix toret = new GMatrix();
        for (int i=0; i<m.getRowDimension(); i++)
            for (int j=0; j<m.getColumnDimension(); j++)
                toret.set(i, j, m.get(i, j));
        return toret;
    }

    public static Matrix toMatrix(IMatrix m) {
        int size = Math.max(m.nRows(), m.nCols());
        Matrix toret = new Matrix(size,size);
        try {
            for (MultiKey x: m.getValueRange()) {
                toret.set(MatrixOp.getRow(x), MatrixOp.getCol(x), m.get(x));
            }
            return toret;
        } catch (Throwable ex) {
            return toret;
        }
    }
    
    public static double[] toColumn(IMatrix m) {
        if (m.nCols()==1) {
            double toret [] = new double[m.nRows()];
            for (MultiKey x : m.getValueRange()) {
                toret[(Integer)x.getKey(0)] = m.get(x);
            }
            return toret;
        } else
            return new double[0];
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.tweetsmining.engines.graphsminer.gmatrix.utils;

import java.util.Collection;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;

/**
 *
 * @author gyankos
 */
public class Collections3 {
    
    public static <E extends Object> boolean exists(Collection<E> unfiltered, Predicate<? super E> predicate) {
        if (unfiltered.isEmpty())
            return false;
        return (Collections2.<E>filter(unfiltered, predicate).size()>0);
    }
    
    
    public static <E extends Object> boolean forall(Collection<E> unfiltered, Predicate<? super E> predicate) {
        if (unfiltered.isEmpty())
            return true;
        int fall_size = Collections2.<E>filter(unfiltered, predicate).size();
        return (fall_size==unfiltered.size());
    }
    
    
    public static <E extends Object> Collection<E> intersect(Collection<E> left,final Collection<E> right) {
        
        return Collections2.<E>filter(left, new Predicate<E>() {
            @Override
            public boolean apply(E input) {
                return right.contains(input);
            }
        });
    }
    
    
}

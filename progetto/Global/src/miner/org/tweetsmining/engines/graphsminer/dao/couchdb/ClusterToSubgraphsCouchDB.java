package org.tweetsmining.engines.graphsminer.dao.couchdb;

import java.util.List;

import javax.inject.Inject;

import org.tweetsmining.dao.engines.graphsminer.ClusterToSubgraphsDAO;
import org.tweetsmining.io.serialization.JSONArrayListSerializer;
import org.tweetsmining.io.serialization.StringSerializeByReflection;
import org.tweetsmining.model.graph.MultiLayerGraph;
import org.tweetsmining.model.graphsminer.Cluster;
import org.tweetsmining.persister.couchdb.helper.CouchDBDocumentHelperTmp;
import org.tweetsmining.persister.couchdb.listable.IterableMapListablePersisterByCouchDB;



public class ClusterToSubgraphsCouchDB extends IterableMapListablePersisterByCouchDB<Cluster, List<MultiLayerGraph>> implements ClusterToSubgraphsDAO {

	@Inject
	public ClusterToSubgraphsCouchDB(CouchDBDocumentHelperForUserToGraphProvider couchDBDocumentHelperForUserToGraphProvider, CouchDBDocumentHelperTmp couchDbDocumentHelperTmp) {
		super(couchDBDocumentHelperForUserToGraphProvider.get(), couchDbDocumentHelperTmp);
		setKeySerializer(new StringSerializeByReflection<Cluster>(Cluster.class));
		setValueSerializer(new JSONArrayListSerializer<>(new StringSerializeByReflection<>(MultiLayerGraph.class)));
	}
	
	@Override
	public void commit() {
		super.couchDBCommit();		
	}

	/*
	@Override
	public boolean containsKey(Cluster key) {
		return si.containsKey(key);
	}

	@Override
	public int size() {
		return si.size();
	}

	@Override
	public boolean isEmpty() {extends IterableListableMapPersisterByCouchDB<
		return si.isEmpty();
	}

	@Override
	public List<MultiLayerGraph> get(Cluster key) {
		return si.get(key);
	}

	@Override
	public List<MultiLayerGraph> put(Cluster key, List<MultiLayerGraph> value) {
		return si.put(key, value);
	}
	
	@Override
	public List<MultiLayerGraph> remove(Cluster key) {
		return si.remove(key);
	}

	@Override
	public boolean containsValue(List<MultiLayerGraph> value) {
		return si.containsValue(value);
	}

	@Override
	public Set<Cluster> keySet() {
		return si.keySet();
	}

	@Override
	public void clear() {
		si.clear();
	}

	@Override
	public Iterator<Entry<Cluster, List<MultiLayerGraph>>> iterator() {
		return si.iterator();
	}

	@Override
	public SerializableList<List<MultiLayerGraph>> asList() {
		return si.asList();
	}

	@Override
	public void commit() {
		si.commit();
	}
*/
	/*@Override
	public Collection<SerializableList<MultiLayerGraph>> values() {
		return Collections.emptyList();
	}*/
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.tweetsmining.engines.graphsminer.graphs;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author gyankos
 */
public class SubgraphE<E> {
    
    public boolean compare(List<E> sub, List<E> sup) {
        if (sub == null)
            return true;
        else if (sup == null)
            return false;
        while (!sub.isEmpty()) {
            while ((!sup.isEmpty()) && (!sub.get(0).equals(sup.get(0)))){
                if (sup.size()>1)
                    sup = sup.subList(1, sup.size());
                else 
                    sup = new LinkedList<>();
            }
            if ((sup.isEmpty()) && (!sub.isEmpty()))
                return false;
            //System.out.println(subI.hasNext()+" "+supI.hasNext());
            while ((!sub.isEmpty()) && (!sup.isEmpty()) && sub.get(0).equals(sup.get(0))){
                if (sub.size()>1)
                    sub = sub.subList(1, sub.size());
                else 
                    sub = new LinkedList<>();
                
                if (sup.size()>1)
                    sup = sup.subList(1, sup.size());
                else 
                    sup = new LinkedList<>();
            }
        }
        return true; // sub == empty is a subgraph
    }
    
}

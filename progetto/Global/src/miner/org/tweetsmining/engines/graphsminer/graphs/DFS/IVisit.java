/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.tweetsmining.engines.graphsminer.graphs.DFS;

import java.util.SortedSet;

import org.tweetsmining.model.graph.database.Entity;

/**
 * Generic interface for a visit algorithm
 * @author gyankos
 */
public interface  IVisit<T> {
    
    public SortedSet<Entity> getVisitedVertices();
    
    public int nVertices();
 
    public boolean hasVisitedAllVertices();
    
    public abstract boolean hasNext();
    
    /**
     * The current position of the visit
     * @return  If the DFS has not started yet, it returns the vertex from which
     *          the computation will be started, otherwise it will return the
     *          vertex that is currently visited.
     */
    public Entity getCurrentPos();
    
    /**
     * Starts the visit of the algorithm, and returns an 
     * @return 
     */
    public T start();
    
}

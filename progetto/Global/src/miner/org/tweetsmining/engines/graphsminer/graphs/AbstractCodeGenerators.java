/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.tweetsmining.engines.graphsminer.graphs;

import org.tweetsmining.engines.graphsminer.gmatrix.utils.NonCompPair;
import org.tweetsmining.model.graph.AbstractMultiLayerGraph;
import org.tweetsmining.model.graph.database.Entity;

/**
 * Interface used by the classes that generate DFS codes
 * @author gyankos
 */
public abstract class AbstractCodeGenerators<T> {
    
    /**
     * Generate a DFS code for the given MultiLayerGraph
     * @param ml
     * @return 
     */
    public abstract NonCompPair<T, Entity> getADFSCode(AbstractMultiLayerGraph ml);
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.tweetsmining.engines.graphsminer.matrices;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;

import org.apache.commons.collections4.keyvalue.MultiKey;
import org.tweetsmining.engines.graphsminer.gmatrix.utils.Collections3;
import org.tweetsmining.model.matrices.IMatrix;

/**
 *
 * @author Mattia
 */
public class MarkovClustering extends MatrixOp {

    /**
     * Matrix inflation approximated as a matrix product
     * @param m     matrix  
     * @param inf   Inflation product
     * @return 
     */
    public static IMatrix mclInflate (IMatrix m, int inf){
        int i;
        IMatrix tmp = m;
        for(i=1;i<=inf;i++){
            tmp = prod(tmp, m);
        }
        return tmp;
    }

    /**
     * Matrix normalization
     * @param m
     * @return 
     */
    public static IMatrix mclNorm (IMatrix m) {
        int columSum = 0;
        for (MultiKey x : m.getValueRange()) {
            columSum += m.get(x);
        }
        return transpose(div(transpose(m),columSum));
    }

    /**
     * Performs the matrix clustering
     * @param m         Matrix 
     * @param inf       Inflate parameter
     * @param iter      Iteration parameter
     * @return          The clustered matrix
     */
    public static IMatrix mcl(IMatrix m,int inf, int iter) {
        int i;
        IMatrix oldm;
        IMatrix mNorm;
        for(i=1;i<=iter;i++){
            oldm = m;
            
            mNorm = mclNorm(m);
            m = prod(mNorm,mNorm);
            m = mclInflate(m, inf);
            m = mclNorm(m);
            
            //obtaining the common valus
            Collection<MultiKey> oldmVals = oldm.getValueRange();
            Collection<MultiKey> mcalcVals = m.getValueRange();
            int row = Math.max(m.nCols(), oldm.nCols());
            int col = Math.max(m.nRows(), oldm.nRows());
            
            
            if (oldmVals.size()==mcalcVals.size() && oldmVals.size()==(row*col))
            {
                int count = 0;
                for (MultiKey x : Collections3.intersect(oldmVals, mcalcVals)) {
                    if (oldm.get(x)==m.get(x))
                        count += 1;
                }
                //break through ~ stationary matrix reached
                if (count == row*col)
                    return m;
            }
            
            //if()
        }
        return m;
    }


    /**
     * Given a matrix over which we have performed MCL, we want to return the set of clusters
     * @param m
     * @return 
     */
    public static Collection<LinkedList<Integer>> collectMCLFeatures (IMatrix m){
        //Maps each cluster on the i-th row into a list of elements 
        HashMap<Integer,LinkedList<Integer>> hm = new HashMap<>();
        
        //Obtaining the clusters only for the non-empty rows
        for (MultiKey ij : m.getValueRange()) {
        	System.out.println(ij.getKey(0)+"~"+ij.getKey(1));
            if (!hm.containsKey((Integer)ij.getKey(0)))
                hm.put((Integer)ij.getKey(0), new LinkedList<Integer>());
            LinkedList<Integer> li = hm.get((Integer)ij.getKey(0));
            li.add((Integer)ij.getKey(1));
            hm.put((Integer)ij.getKey(0),li);
        }

        //Obtaining the clusters only
        return hm.values();
    }
}

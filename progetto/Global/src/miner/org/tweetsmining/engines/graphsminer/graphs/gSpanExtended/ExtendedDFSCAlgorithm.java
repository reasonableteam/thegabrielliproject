/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.tweetsmining.engines.graphsminer.graphs.gSpanExtended;

import java.util.HashSet;
import java.util.SortedSet;
import java.util.TreeSet;

import org.tweetsmining.engines.graphsminer.gmatrix.utils.NonCompPair;
import org.tweetsmining.engines.graphsminer.graphs.AbstractCodeGenerators;
import org.tweetsmining.engines.graphsminer.graphs.DFS.DFS;
import org.tweetsmining.engines.graphsminer.graphs.gSpan.GSCode;
import org.tweetsmining.engines.graphsminer.graphs.gSpan.SimpleDFSCodeGenerator;
import org.tweetsmining.model.graph.AbstractMultiLayerGraph;
import org.tweetsmining.model.graph.database.Entity;

/**
 * This algorithm returns the Extended DFSCode ~ Used in ExtendedgSpanGraph
 * @author gyankos
 */
public class ExtendedDFSCAlgorithm extends AbstractCodeGenerators<ExtendedGSCode> {
    
    @Override
    public NonCompPair<ExtendedGSCode,Entity> getADFSCode(AbstractMultiLayerGraph ml) {
        
        SimpleDFSCodeGenerator e = new SimpleDFSCodeGenerator();
        
        //First Part of the triple
        DFS<GSCode> d = new DFS<>(ml,e);
        Entity start = d.getCurrentPos();
        GSCode first = d.start();
        
        if (d.hasVisitedAllVertices()) {
            //Classic implementation
            return new NonCompPair<>(new ExtendedGSCode(first,new HashSet<GSCode>()),start);
        }
        
        //Visited Vertices
        SortedSet<Entity> visited = d.getVisitedVertices();
        //Vertices to visit
        TreeSet<Entity> unvisited = new TreeSet<>(ml.getVertices());
        unvisited.removeAll(visited);
        
        
        //DFSCode extensions
        TreeSet<GSCode> second = new TreeSet<>(); 
        while (!unvisited.isEmpty()) {
            Entity startVisit = unvisited.first();
            
            ExtendedDFSCodeGenerator algorithm = new ExtendedDFSCodeGenerator(visited);
            DFS<GSCode> next_step = new DFS<>(ml,algorithm,startVisit);
            second.add(next_step.start());
            SortedSet<Entity> loc = next_step.getVisitedVertices();
            visited.addAll(loc);
            unvisited.removeAll(loc);
        }
        
        
        return new NonCompPair<>(new ExtendedGSCode(first,second),start);
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.tweetsmining.engines.graphsminer.Database;

import org.tweetsmining.model.graph.database.Entity;

/**
 *
 * @author gyankos
 */
public class Word extends Entity {

	private static final long serialVersionUID = 6001447548368881284L;
	public static final String WORD_URI = "http://our.project/concepts/words#";
    
    public Word(String name, int v_index) {
        super(getWordUrl(name), v_index);
    }
    
    private static String splitName(String e) {
        return e.split("#")[1];
    }
    
    public static String getStringWordFromEntity(Entity e) {
        return splitName(e.getFullUri());
    }
    
    public static String getWordUrl(String name) {
        return WORD_URI+name.replaceAll("[^a-zA-Z]", "x");
    }
    
    public static Word getWordFromEntity(Entity e) {
        return new Word(splitName(e.getFullUri()),e.getIndex());
    }
    
    /**
     * Returns the word without uri
     * @return 
     */
    @Override
    public String toString() {
        return getName();
    }
    
    /**
     * Returns the word without uri
     * @return 
     */
    public String getName() {
        return splitName(getFullUri());
    }
    
    @Override
    public int compareTo(Entity w) {
        if (w instanceof Word) {
            return getName().compareTo(((Word)w).getName());
        } else
            return super.compareTo(w);
    }
    
}

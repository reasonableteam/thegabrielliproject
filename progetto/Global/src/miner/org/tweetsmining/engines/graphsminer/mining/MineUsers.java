/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.tweetsmining.engines.graphsminer.mining;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;

import javax.inject.Inject;
import javax.inject.Named;

import org.tweetsmining.dao.engines.graphsminer.ClusterToSubgraphsDAO;
import org.tweetsmining.dao.engines.graphsminer.UserToGraphDAO;
import org.tweetsmining.dao.engines.graphsminer.WordPositionDAO;
import org.tweetsmining.engines.graphsminer.graphs.gSpanExtended.ExtendedgSpan;
import org.tweetsmining.engines.graphsminer.matrices.MarkovClustering;
import org.tweetsmining.engines.graphsminer.matrices.MatrixFactory;
import org.tweetsmining.engines.graphsminer.matrices.MatrixOp;
import org.tweetsmining.model.graph.IAdjacencyGraph;
import org.tweetsmining.model.graph.MultiLayerGraph;
import org.tweetsmining.model.graph.database.Entity;
import org.tweetsmining.model.graphsminer.Cluster;
import org.tweetsmining.model.matrices.GMatrix;
import org.tweetsmining.model.matrices.IMatrix;
import org.tweetsmining.model.user.User;

import com.google.common.base.Function;
import com.google.common.collect.Collections2;

/**
 * TODO: inflation
 * @author gyankos
 */
public class MineUsers {
    

    private final UserToGraphDAO myDb;
    private ClusterToSubgraphsDAO newInstance;
    private int myInflate;
    private int myIterate;
    private MultiLayerGraph toy;
	private WordPositionDAO mapPosWords;
    
    public final static String USER_URL = "http://tweetsmining.org/users/";
    public final static String SIMILAR_USERS_URL = "http://tweetsmining.org/rel/similarUserTo";
    
    /**
     * 
     * @param db        		Map of Username-Database
     * @param inflate   		The inflate parameter
     * @param iterate   		The iteration paramter
     * @param new_instance		a brand_new object that maps persistency for mineDifferences result
     * @param wordPositionMap	persistency for word-position mapping
     * 
     */
    @Inject
    public MineUsers(@Named("miner.inflate") int inflate, 
    				 @Named("miner.iterate") int iterate,

    				 ClusterToSubgraphsDAO new_instance, 
    				 WordPositionDAO wordPositionMap,
    				 UserToGraphDAO db) {
    	
//        this.myDb = null;
        this.myInflate = inflate;
        this.myIterate = iterate;
        this.myDb = db;
        this.toy = null;
        this.newInstance = new_instance;
        this.mapPosWords = wordPositionMap;
    }

    
    /**
     * Returns the clusters of a given set
     * @return          Since each cluster is a Collection<String>, we're returning
     *                  the set of all the clusters
     */
    public List<Cluster> dbToCluster() {
    	
    	if (this.myDb == null)
    		return null;
        
        IMatrix toPerformCluster = new GMatrix();
        
        //Maps user to integer positions
        final HashMap<Integer,User> userOrder = new HashMap<>();
        
        //Cleans previous computations
        this.mapPosWords.clear();
        
        int userCount = 0;      //column
        List<String> users = new LinkedList<>();
        
        //For each user in the database
        for (Entry<User, MultiLayerGraph> ke : myDb) {
            
        	System.out.println("User: " + ke.getKey().toString());
            //associates to the i-th user the i-th column of 
            userOrder.put(userCount, ke.getKey());
            
            //Gets the graph of the i-th user
            MultiLayerGraph ml = ke.getValue();
            System.out.println("With vertices: " + ml.getNVertices());
            int max = 0;
            for (Entity x:ml.getVertices())
            	if (x.getIndex() > max)
            		max = x.getIndex();
            System.out.println("With maximum: " + max);
            
            //We'll flatten the MultiLayerGraph on a same given level
            IMatrix tmp = new GMatrix();
            System.out.println("~~ Collapsing layers");
            //Adds all the relations on a same given layer
            for (IAdjacencyGraph<Entity> l : ml.getLayers()) {
            	System.out.println(l.nCols() +" : "+l.nRows());
                tmp = MatrixOp.sum(tmp, l);
            }
            //Matrix Normalization 
            tmp = MarkovClustering.mclNorm(tmp);
            System.out.println("~~ Normalization with "+tmp.nCols() +" : "+tmp.nRows());
            //Returns each word usage probability as sationary distribution
            System.out.println("~~ Distribution");
            tmp = MatrixOp.stationaryDistribution(tmp);
            System.out.println("~~ Distribution "+tmp.nCols() +" : "+tmp.nRows());
            //If the algorithm is correct, it should return a sole column
            //We want to obtain an array that points out the probability of
            //each word usage
            
            double stationary[] = MatrixFactory.toColumn(tmp);
            System.out.println("~~ To Column of size " + stationary.length);
            
            
            //Updates the indexing for all the user's words
            for (String x:ml.getVerticesURI())
                if (!mapPosWords.containsKey(x)) {
                    int old_size = mapPosWords.size();
                    mapPosWords.put(x,old_size);
                }
            
            //Adding the information inside the matrix: for each word index
            for (Integer word_original_pos:ml.getVerticesId()) {
                //Given the integer word_matrix_row that indicates
                //the word position on stationary[], we want to obtain
                //the new position inside the overall map
                String word_key = ml.getVertex(word_original_pos).getFullUri();
                Integer word_matrix_row = mapPosWords.get(word_key);
                
                //We want to put all the users inside the row, since the 
                //laplacian will use "toPerformCluster x t(toPerformCluster)"
                //and hence, we want to relate the users to other users, and
                //not words to words
                
                //is stationary.length < numberofnodes, then something has become 0
                //So, we're not inserting an empty link
                if (word_original_pos < stationary.length) 
                	toPerformCluster.set(userCount, word_matrix_row, stationary[word_original_pos]);
            }

            users.add((USER_URL+ke.getKey()));
            
            userCount++;    // go to the next column
        }
        
        System.out.println("~~ Ok. Final is "+toPerformCluster.nCols() +" : "+toPerformCluster.nRows());
        
        /*File file = new File("/home/gyankos/hypergraph.txt");
        FileOutputStream fop1 = null;
        try {
			fop1 = new FileOutputStream(file);
		} catch (FileNotFoundException e) {
			System.exit(9);
		}
        for (org.apache.commons.collections4.keyvalue.MultiKey x : toPerformCluster.getValueRange()){
        	String towrite = x.getKey(0)+":"+x.getKey(1)+" -> "+toPerformCluster.get(x);
        	try {
				fop1.write(towrite.getBytes());
			} catch (IOException e) {
				System.exit(9);
			}
        }
        try {
			fop1.close();
		} catch (IOException e) {
			System.exit(9);
		}*/
        
        //Given the matrix hypergraph, we want to obtain its regularized laplacian.
        //Before applying the MLC, we have to regularize the matrix
        toPerformCluster = MatrixOp.regularizedLaplacianMatrix(toPerformCluster);
        System.out.println("~~ Laplacian Done " + toPerformCluster.nCols() + " : " + toPerformCluster.nRows());
        
        toy = MultiLayerGraph.create(toPerformCluster, users, users, SIMILAR_USERS_URL);
        
        //Then we apply the mlc algorithm
        toPerformCluster = MarkovClustering.mcl(toPerformCluster, myInflate, myIterate);
        System.out.println("~~ MCL Done " + toPerformCluster.nCols() + " : " + toPerformCluster.nRows());
        //Then we obtain the clusters
        Collection<LinkedList<Integer>> intClusters =  MarkovClustering.collectMCLFeatures(toPerformCluster);
        System.out.println("Got clusters of size " + intClusters.size());

        mapPosWords.clear(); //removes the tmp
        
        return new LinkedList<>(Collections2.transform(intClusters, new Function<LinkedList<Integer>, Cluster>() {
            @Override
            public Cluster apply(LinkedList<Integer> input) {
                return new Cluster(new LinkedList<>(Collections2.transform(input, new Function<Integer, User>() {
                    @Override
                    public User apply(Integer input) {
                        return userOrder.get(input);
                    }
                })));
            }
        }));
        
    }
    
    public MultiLayerGraph getUserCluster() {
        return toy;
    }
    
    /**
     * Applies the extended gSpan algorithm in order to mine the similar paths between the users
     * @param cluster   Set of clusters previously mined, over which perform the markov clustering
     * @param db        User database
     * @return 
     */
    public ClusterToSubgraphsDAO mineDifferences(List<Cluster> clusters/*, final Map<String,MultiLayerGraph> db*/) {
        
    	newInstance.clear();
        for (Cluster aCluster: clusters) {
        		List<MultiLayerGraph> mlgdb = myDb.asList();//newly created list
        		List<MultiLayerGraph> returned = myDb.asList();//returned subgraphs
//                listIterator.
                for (User y : aCluster.getListUsers()) {
                    mlgdb.add(myDb.get(y));
                }

                ExtendedgSpan egs = new ExtendedgSpan();
                egs.setDatabase(mlgdb);
                egs.gSpan(0.5, 3,returned);
                
                newInstance.put(aCluster,  returned);//object modified by gSpan
                mlgdb.clear();//Forces file removal
                returned.clear();//Forces removal after adding
        }
        
        newInstance.commit();
        return newInstance;
        
    }
    
    
}

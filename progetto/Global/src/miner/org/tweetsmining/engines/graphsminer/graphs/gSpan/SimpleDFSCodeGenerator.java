/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.tweetsmining.engines.graphsminer.graphs.gSpan;

import java.util.Collection;
import java.util.SortedSet;

import org.tweetsmining.engines.graphsminer.graphs.DFS.AbstractDBVisit;
import org.tweetsmining.model.graph.database.ERTriple;
import org.tweetsmining.model.graph.database.Entity;
import org.tweetsmining.model.graph.database.Relation;

/**
 * DFS Algorithm to generate the DFSCode for the gSpan Algorithm
 *
 * @author gyankos
 */
public class SimpleDFSCodeGenerator extends AbstractDBVisit<GSCode> {

    
    
    public SimpleDFSCodeGenerator() {
        super();
    }
    private GSCode dfsCode = new GSCode();
    private ERTriple first = null;

    @Override
    public void visitVertex(Entity v) {
        //Void
    }

    @Override
    public void visitEdge(Entity src, Relation l, Entity dst) {
        ERTriple toadd = new ERTriple(src, l, dst);
        if (dfsCode.isEmpty()) {
            first = toadd;
        }
        dfsCode.add(toadd);
    }

    @Override
    public GSCode getOutput() {
        return dfsCode;
    }

    @Override
    public Collection<ERTriple> getEdgeVisitOrder(SortedSet<ERTriple> cert) {
        return cert;
    }

    public ERTriple getFirst() {
        return first;
    }
}

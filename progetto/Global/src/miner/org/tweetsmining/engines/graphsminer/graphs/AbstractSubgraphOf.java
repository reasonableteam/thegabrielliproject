/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.tweetsmining.engines.graphsminer.graphs;

import org.tweetsmining.model.graph.MultiLayerGraph;



/**
 * Defines the classes that perform the subgraph isomorphism ~ used in support functions
 * @author gyankos
 */
public abstract class AbstractSubgraphOf<T> {
    
    public abstract boolean subgraphOf(T cmpSub, T cmpSup);
    public abstract boolean subgraphOf(MultiLayerGraph sub, MultiLayerGraph sup);
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.tweetsmining.engines.graphsminer.graphs.DFS;

import java.util.List;

import org.tweetsmining.model.graph.database.ERTriple;

/**
 * Interface that is common to all the generated codes
 * @author gyankos
 */
public interface ICode extends List<ERTriple> {
    
}

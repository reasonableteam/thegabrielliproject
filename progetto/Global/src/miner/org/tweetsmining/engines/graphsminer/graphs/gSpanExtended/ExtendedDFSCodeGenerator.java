/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.tweetsmining.engines.graphsminer.graphs.gSpanExtended;

import java.util.Collection;
import java.util.Set;
import java.util.SortedSet;

import org.tweetsmining.engines.graphsminer.graphs.DFS.AbstractDBVisit;
import org.tweetsmining.engines.graphsminer.graphs.gSpan.GSCode;
import org.tweetsmining.model.graph.database.ERTriple;
import org.tweetsmining.model.graph.database.Entity;
import org.tweetsmining.model.graph.database.Relation;

import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;

/**
 * Implements the visit of the other bits of the graph. ~ Algorithmic Step
 * @author gyankos
 */
public class ExtendedDFSCodeGenerator extends AbstractDBVisit<GSCode> {

    private Set<Entity> toAvoid;
    private GSCode next;
    
    public ExtendedDFSCodeGenerator(Set<Entity> visited) {
        super();
        toAvoid = visited;
        next = new GSCode();
    }
    
    /**
     * Removes the edges that start from the vertices that have been already visited
     * @param cert  List of possible incoming edges
     * @return      filtered edges
     */
    @Override
    public Collection<ERTriple> getEdgeVisitOrder(SortedSet<ERTriple> cert) {
        return Collections2.filter(cert, new Predicate<ERTriple>() {
            @Override
            public boolean apply(ERTriple input) {
                return (!(toAvoid.contains(input.getSource())));
            }
        });
    }

    @Override
    public void visitVertex(Entity v) {
        
    }

    @Override
    public void visitEdge(Entity src, Relation l, Entity dst) {
        next.add(new ERTriple(src, l, dst));//forms the extension
    }

    @Override
    public GSCode getOutput() {
        return next;//returns the extension
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.tweetsmining.engines.graphsminer.graphs.gSpan;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.SortedSet;

import org.tweetsmining.engines.graphsminer.graphs.DFS.AbstractDBVisit;
import org.tweetsmining.model.graph.database.ERTriple;
import org.tweetsmining.model.graph.database.Entity;
import org.tweetsmining.model.graph.database.Relation;

/**
 * This class returns the edges over which we shall later on implement the
 * extensions
 *
 * @author gyankos
 */
public class SimpleRightmostExpansion extends AbstractDBVisit<List<Entity>> {

    public SimpleRightmostExpansion() {
    }
    List<Entity> q = new LinkedList<>();

    @Override
    public Collection<ERTriple> getEdgeVisitOrder(SortedSet<ERTriple> cert) {
        LinkedList<ERTriple> er = new LinkedList<>();
        if (!cert.isEmpty()) {
            er.add(cert.last());
        }
        return er;
    }

    @Override
    public void visitVertex(Entity v) {
    }

    @Override
    public void visitEdge(Entity src, Relation l, Entity dst) {
        if ((q.isEmpty()) || (!q.get(q.size() - 1).equals(src))) {
            q.add(src);
        }

        q.add(dst);
    }

    @Override
    public List<Entity> getOutput() {
        return q;
    }
}

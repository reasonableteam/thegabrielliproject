package org.tweetsmining.engines.wordsrelator._di.guice.module.core;

import org.tweetsmining.engines.wordsrelator.babelnetsearcher._di.guice.module.BabelnetSearcherModule;

import com.google.inject.AbstractModule;

public class WordsRelatorCoreModule extends AbstractModule {

	@Override
	protected void configure() {
		install( new BabelnetSearcherModule() );
	}

}

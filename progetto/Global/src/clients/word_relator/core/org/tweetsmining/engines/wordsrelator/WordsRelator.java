/*******************************************************************************
 * Copyleft (c) 2014, "Massimiliano Leone - <maximilianus@gmail.com> - https://plus.google.com/+MassimilianoLeone"
 * This file (WordsRelatorByBabelnet.java) is part of WordsRelatorByBabelnet.
 * 
 *     WordsRelatorByBabelnet.java is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     WordsRelatorByBabelnet.java is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with .  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.tweetsmining.engines.wordsrelator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import javax.inject.Inject;

import org.tweetsmining.dao.engines.tweetsparser.UserToStemmedWordsDAO;
import org.tweetsmining.dao.engines.wordsrelator.UserToRelatedWordsDAO;
import org.tweetsmining.dao.engines.wordsrelator.WordRelatorDAO;
import org.tweetsmining.engines.wordsrelator.babelnetsearcher.BabelnetSearcher;
import org.tweetsmining.engines.wordsrelator.babelnetsearcher.exceptions.NullRelatedMapException;
import org.tweetsmining.engines.wordsrelator.babelnetsearcher.exceptions.NullSynsetsException;
import org.tweetsmining.engines.wordsrelator.printer.Printer;
import org.tweetsmining.model.user.User;
import org.tweetsmining.model.wordsrelator.WordRelations;
import org.tweetsmining.model.wordsrelator.exceptions.RelationStringNullException;

public class WordsRelator {

	private final BabelnetSearcher babelnetSearcher;
	private final WordRelatorDAO wordRelatorDAO;
	private Printer printer;
	private UserToRelatedWordsDAO userToRelatedWordsDAO;
	private UserToStemmedWordsDAO userToStemmedWordsDAO;

	@Inject
	public WordsRelator
		   (BabelnetSearcher babelnetSearcher,
			UserToStemmedWordsDAO userToStemmedWordsDAO,
			WordRelatorDAO wordRelatorDAO,
			UserToRelatedWordsDAO userToRelatedWordsDAO,
			String tweetCorpusName, Printer printer) {
		this.babelnetSearcher = babelnetSearcher;
		this.userToStemmedWordsDAO = userToStemmedWordsDAO;
		this.wordRelatorDAO = wordRelatorDAO;				// cache
		this.userToRelatedWordsDAO = userToRelatedWordsDAO; // data for 3^ step
		this.printer = printer;
		// this.wordRelatorDAO.init(tweetCorpusName);
	}

	public void searchForWords() {

		/*
		 * Map<String, Set<String>> userToWordsMap = //
		 * deserializeInputFile(inputFileName); null;
		 */

		/*
		 * Iterator<Entry<String, List<WordRelations>>> iterator =
		 * userToRelatedWordsDAO.getIterator(); while (iterator.hasNext()) {
		 * Entry<String, List<WordRelations>> next = iterator.next(); }
		 */

		int usersCounter = 1;
		Iterator<Entry<User, Set<String>>> userToStemmedWordIterator = userToStemmedWordsDAO.iterator();
		System.out.println("Found " + userToStemmedWordsDAO.size() + " users. Done:");
		while (userToStemmedWordIterator.hasNext()) {
			Entry<User, Set<String>> next = userToStemmedWordIterator.next();
			User user = next.getKey();
			System.out.println("My User:" + user.toString());
			Set<String> stemmedWords = next.getValue();
			System.out.println((stemmedWords == null? "null" : "HAS") );

			System.out.print("\n" + usersCounter + ": ");
			// Set<String> userWords = userToWordsMap.get(user);
			List<WordRelations> wordRelationsList = new ArrayList<WordRelations>();
			// TODO parallel
			for (String word: stemmedWords) {
				printer.print(word);
				WordRelations relationsForWordFounded = null;
				if (wordRelatorDAO.containsKey(word)) { // found in cache
					relationsForWordFounded = wordRelatorDAO.get(word);
					wordRelationsList.add(relationsForWordFounded);
					System.out.println("(l) ");
				} else {
					try { // use babelnet
						relationsForWordFounded = babelnetSearcher.findRelationsForWord(word);
						if (!relationsForWordFounded.getMainsenses().isEmpty()) {
							wordRelatorDAO.put(word,relationsForWordFounded);
							wordRelationsList.add(relationsForWordFounded);
						}
						System.out.println("(b) ");
					} catch (NullSynsetsException e) {
						e.printStackTrace();
						System.out.println("(bE) ");
					} catch (NullRelatedMapException e) {
						e.printStackTrace();
						System.out.println("(bE) ");
					} catch (RelationStringNullException e) {
						e.printStackTrace();
						System.out.println("(bE) ");
					}
				}
			}
			if (!wordRelationsList.isEmpty()) // add to cache
				userToRelatedWordsDAO.put(user, wordRelationsList);
			
			usersCounter++;
		}
		userToRelatedWordsDAO.commit();
	}

	/*
	 * public static Map<String, Set<String>> deserializeInputFile(String
	 * inputFileName) throws IOException, ClassNotFoundException { Map<String,
	 * Set<String>> usersAndWords = PersistenceUtils.<Map<String,
	 * Set<String>>>readFromFile(inputFileName); return usersAndWords; }
	 */

	/*
	public static void main(String[] args) {

		// if (args.length!=1) {
		// System.out.println("needed argument: [filename_without_extension_as_tweet_corpus_name]");
		// } else {

//		String tweetCorpusName = args[0];
		
		Injector injector = Guice.createInjector( new WordsRelatorMainModule());
		WordsRelator wordsRelatorByBabelnetMain = injector.getInstance(WordsRelator.class);
		System.out.println("ok!");
		wordsRelatorByBabelnetMain.searchForWords();
		// }
	}*/

}

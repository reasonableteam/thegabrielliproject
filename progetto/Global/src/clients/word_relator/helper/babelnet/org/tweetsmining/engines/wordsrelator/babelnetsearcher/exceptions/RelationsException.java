/*******************************************************************************
 * Copyleft (c) 2014, "Massimiliano Leone - <maximilianus@gmail.com> - https://plus.google.com/+MassimilianoLeone"
 * This file (RelationsException.java) is part of BabelnetSearcher.
 * 
 *     RelationsException.java is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     RelationsException.java is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with .  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.tweetsmining.engines.wordsrelator.babelnetsearcher.exceptions;

public class RelationsException extends Exception {

	private static final long serialVersionUID = 1881556558430311562L;

	public RelationsException() {
		super();
	}

	public RelationsException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public RelationsException(String message, Throwable cause) {
		super(message, cause);
	}

	public RelationsException(String message) {
		super(message);
	}

	public RelationsException(Throwable cause) {
		super(cause);
	}

}

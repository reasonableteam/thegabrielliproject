package org.tweetsmining.engines.wordsrelator.babelnetsearcher.test;

import java.util.Set;

import org.tweetsmining.engines.wordsrelator.babelnetsearcher.BabelnetSearcher;
import org.tweetsmining.engines.wordsrelator.babelnetsearcher._di.guice.module.BabelnetSearcherModule;
import org.tweetsmining.engines.wordsrelator.babelnetsearcher.exceptions.NullRelatedMapException;
import org.tweetsmining.engines.wordsrelator.babelnetsearcher.exceptions.NullSynsetsException;
import org.tweetsmining.model.wordsrelator.RelationedTerm;
import org.tweetsmining.model.wordsrelator.WordRelations;
import org.tweetsmining.model.wordsrelator.exceptions.RelationStringNullException;

import com.google.inject.Guice;

public class TestMain {

	public static void main(String[] args) {
		BabelnetSearcher babelnetSearcher = Guice.createInjector( new BabelnetSearcherModule() ).getInstance(BabelnetSearcher.class);
		String toSearch = args[0];
		toSearch = "home";
		System.out.println("trying to search: "+toSearch);
		try {
			WordRelations findRelationsForWord = babelnetSearcher.findRelationsForWord(toSearch);
			System.out.println("word: "+findRelationsForWord.getWord());
			System.out.println("main senses and related terms: ");
			for (String mainsense: findRelationsForWord.getMainsenses()) {
				System.out.print("\n"+mainsense+": ");
				Set<RelationedTerm> termsForMainsense = findRelationsForWord.getTermsForMainsense(mainsense);
				for (RelationedTerm relationedTerm: termsForMainsense)
					System.out.print(relationedTerm.getTerm()+"("+relationedTerm.getRelationType()+")");
			}
		} catch (NullSynsetsException e) {
			e.printStackTrace();
		} catch (NullRelatedMapException e) {
			e.printStackTrace();
		} catch (RelationStringNullException e) {
			e.printStackTrace();
		}
	}
}

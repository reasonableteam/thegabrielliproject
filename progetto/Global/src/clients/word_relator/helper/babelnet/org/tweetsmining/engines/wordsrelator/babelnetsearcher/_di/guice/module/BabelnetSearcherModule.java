package org.tweetsmining.engines.wordsrelator.babelnetsearcher._di.guice.module;

import javax.inject.Singleton;

import it.uniroma1.lcl.babelnet.BabelNet;
import it.uniroma1.lcl.jlt.util.Language;

import org.tweetsmining.engines.wordsrelator.babelnetsearcher._di.annotations.BabelnetLanguage;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;


public class BabelnetSearcherModule extends AbstractModule {

	@Override
	protected void configure() {
		bind(Language.class).annotatedWith(BabelnetLanguage.class).toInstance(Language.EN);	
	}
	
	@Provides @Singleton
	public BabelNet provideBabelnet() {
		return BabelNet.getInstance();
	}


}

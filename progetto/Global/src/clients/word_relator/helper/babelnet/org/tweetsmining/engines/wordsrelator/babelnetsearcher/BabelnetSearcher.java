/*******************************************************************************
 * Copyleft (c) 2014, "Massimiliano Leone - <maximilianus@gmail.com> - https://plus.google.com/+MassimilianoLeone"
 * This file (BabelnetSearcher.java) is part of BabelnetSearcher.
 * 
 *     BabelnetSearcher.java is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     BabelnetSearcher.java is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with .  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.tweetsmining.engines.wordsrelator.babelnetsearcher;

import it.uniroma1.lcl.babelnet.BabelNet;
import it.uniroma1.lcl.babelnet.BabelSynset;
import it.uniroma1.lcl.jlt.util.Language;

import java.io.IOException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.inject.Inject;

import org.tweetsmining.engines.wordsrelator.babelnetsearcher._di.annotations.BabelnetLanguage;
import org.tweetsmining.engines.wordsrelator.babelnetsearcher.exceptions.NullRelatedMapException;
import org.tweetsmining.engines.wordsrelator.babelnetsearcher.exceptions.NullSynsetsException;
import org.tweetsmining.engines.wordsrelator.printer.Printer;
import org.tweetsmining.model.wordsrelator.RelationType;
import org.tweetsmining.model.wordsrelator.RelationedTerm;
import org.tweetsmining.model.wordsrelator.WordRelations;
import org.tweetsmining.model.wordsrelator.exceptions.RelationStringNullException;

import edu.mit.jwi.item.IPointer;

public class BabelnetSearcher {

	private final BabelNet babelNet;
	private final Language language;
	private final Printer printer;

	@Inject
	public BabelnetSearcher(BabelNet babelNet, /*RelatorDAO relationCache,*/ @BabelnetLanguage Language language, Printer printer) {
		this.babelNet = babelNet;
		this.language = language;
		this.printer = printer;
	}

	public WordRelations findRelationsForWord(String word) throws NullSynsetsException, NullRelatedMapException, RelationStringNullException {
		//we are here because local cache get empty results, so we call babelnet
		
		WordRelations wordRelations = new WordRelations(word,RelationType.DISAMBIGUATION);

		// ricerca dei sinonimi
		List<BabelSynset> synsets;
		try {
			synsets = babelNet.getSynsets(language, word);
//			System.out.println("find a synset for: "+word+" ("+synsets.size()+" terms)");
			printer.print("\n"+word+" ("+synsets.size()+" terms): ");
		} catch (IOException e) {
			e.printStackTrace();
			throw new NullSynsetsException();
		}
		Iterator<BabelSynset> synsetIterator = synsets.iterator();
		int m = 1;
		while (synsetIterator.hasNext()) {
//		for (BabelSynset synset : synsets) {
			BabelSynset synset = synsetIterator.next();
			
			String mainsense = cleanLemma(synset.getMainSense());
//System.out.print(" "+mainsense);
			
/*			Iterator<BabelSense> babelSenseIterator = synset.iterator();
			while (babelSenseIterator.hasNext()) {
				BabelSense babelSense = babelSenseIterator.next();
			}
*/

			/*Map<IPointer, List<BabelSynset>> relatedSynsets;
			try {
				relatedSynsets = synset.getRelatedMap();
			} catch (IOException e) {
				throw new NullRelatedMapException();
			}*/
			
			Iterator<Entry<IPointer, List<BabelSynset>>> ipointerBabelSynsetListEntryIterator = null;
			try {
				Map<IPointer, List<BabelSynset>> relatedMap = synset.getRelatedMap();
				ipointerBabelSynsetListEntryIterator = relatedMap.entrySet().iterator();
			} catch (IOException e1) {
				e1.printStackTrace();
				throw new NullRelatedMapException();
			}
			
			Set<RelationedTerm> terms = new HashSet<>();
			//HashSet<Object> newHashSet = Sets.newHashSet();
			
//			for (IPointer relationTypePointer : relatedSynsets.keySet()) {
//				List<BabelSynset> relationSynsets = relatedSynsets.get(relationTypePointer);
			while (ipointerBabelSynsetListEntryIterator.hasNext()) {
				Entry<IPointer, List<BabelSynset>> ipointerBabelSynsetListEntry = ipointerBabelSynsetListEntryIterator.next();
				IPointer relationTypePointer = ipointerBabelSynsetListEntry.getKey();
				List<BabelSynset> relationSynsets = ipointerBabelSynsetListEntry.getValue();

				Iterator<BabelSynset> relationSynsetsIterator = relationSynsets.iterator();
				while (relationSynsetsIterator.hasNext()) {
					BabelSynset relationSynset = relationSynsetsIterator.next();

					if (relationSynset.getSynsetSource() != null) { 
						String term = cleanLemma(relationSynset);
						
						RelationType relationType = RelationType.getRelationType( relationTypePointer.getName() );
//						addToMultiLayerGraph(mainsense, term, multiLayerGraph, relationType);

						/*if (!synset.getMainSense().contains(term)) {
							DBPEDIA?
						}*/
						
						terms.add( new RelationedTerm(term, relationType));
						wordRelations.addMainsenseTermsEntry(mainsense, terms);
					}
				}
			}
			printer.print(" "+m);
			m++;
		}
//		return relatorDAO.addRelationsFor(word, wordRelations);
		return wordRelations;
	}

	private String cleanLemma(BabelSynset lemma) {
		return cleanLemma(lemma.toString());
	}
	
	private String cleanLemma(String lemma) {
		String toret = lemma.toString().split("#")[0]; //Obtaining 
		if (toret.contains(":")) {
			toret = toret.split(":")[2];
		}
		return toret.toLowerCase();
	}
}

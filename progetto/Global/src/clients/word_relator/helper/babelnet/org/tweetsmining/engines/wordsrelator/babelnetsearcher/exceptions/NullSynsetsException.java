/*******************************************************************************
 * Copyleft (c) 2014, "Massimiliano Leone - <maximilianus@gmail.com> - https://plus.google.com/+MassimilianoLeone"
 * This file (NullSynsetsException.java) is part of BabelnetSearcher.
 * 
 *     NullSynsetsException.java is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     NullSynsetsException.java is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with .  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.tweetsmining.engines.wordsrelator.babelnetsearcher.exceptions;

public class NullSynsetsException extends RelationsException {

	private static final long serialVersionUID = 8049324932532763025L;
	
	public NullSynsetsException() {}

	public NullSynsetsException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public NullSynsetsException(String message, Throwable cause) {
		super(message, cause);
	}

	public NullSynsetsException(String message) {
		super(message);
	}

	public NullSynsetsException(Throwable cause) {
		super(cause);
	}
}

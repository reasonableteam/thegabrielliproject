package org.tweetsmining.engines.tweetsparser.utils.csv;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListSet;

import javax.inject.Inject;
import javax.inject.Named;

import org.tweetsmining.dao.engines.tweetsparser.UserToStemmedWordsDAO;
import org.tweetsmining.engines.tweetsparser._di.annotations.TweetDateFormat;
import org.tweetsmining.engines.tweetsparser.engines.stemmer.WordNetStemmer;
import org.tweetsmining.engines.tweetsparser.engines.stopwordsremover.StopwordsRemover;
import org.tweetsmining.engines.tweetsparser.engines.tweetparser.TweetParser;
import org.tweetsmining.engines.tweetsparser.model.Tweet;
import org.tweetsmining.model.user.User;

import au.com.bytecode.opencsv.CSVReader;

/**
 * Class that can parse CSV file or a set of CSV files which contains tweets
 * 
 * 
 * @author Antonio Notarangelo (<a
 *         href="mailto:antonio.notarangelo@studio.unibo.it"
 *         >antonio.notarangelo@studio.unibo.it</a> - <a
 *         href="http://it.linkedin.com/pub/antonio-notarangelo/87/798/507/"
 *         >LinkedIN Profile</a>)
 * 
 */
public class TweetsCSVParser {
	private final TweetParser tweetParser;
	private final WordNetStemmer stemmer;
	private final StopwordsRemover stopwordsRemover;
//	private final CSVReader reader;
	private final SimpleDateFormat formatter;
	private final Map<String, Set<String>> usersAndWords;
	private final UserToStemmedWordsDAO userToStemmedWordsDAO;
	private final String corpusDirPath;
	
//	public static String dateFormatPattern = "yyyy-MM-dd HH:mm:ss";

	/**
	 * Constructor of the class
	 * @throws IOException 
	 * 
	 * @throws Exception
	 */
	/*
	public TweetsCSVParser() throws IOException {
		tweetParser = new TweetParser();
		stemmer = new WordNetStemmer();
		stopwordsRemover = new StopwordsRemover();
		formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		usersAndWords = new HashMap<String, Set<String>>();
	}
	*/
	
	
	@Inject
	public TweetsCSVParser(TweetParser tweetParser, 
			WordNetStemmer stemmer,
			StopwordsRemover stopwordsRemover,
			@TweetDateFormat SimpleDateFormat formatter
			, UserToStemmedWordsDAO userToStemmedWordsDAO
			, @Named("corpus") String corpusDirPath) {
		this.tweetParser = tweetParser;
		this.stemmer = stemmer;
		this.stopwordsRemover = stopwordsRemover;
		this.userToStemmedWordsDAO = userToStemmedWordsDAO;
		this.corpusDirPath = corpusDirPath;
//		formatter.applyLocalizedPattern(dateFormatPattern);
		this.formatter = formatter;		
		this.usersAndWords = 
//				new HashMap<String, Set<String>>();
				new ConcurrentHashMap<>(100);
	}


	/**
	 * Returns the list of tweets from a csv file passing the path as argument
	 * 
	 * @param fileName
	 * @return
	 * @throws NumberFormatException
	 * @throws IOException
	 */
	protected List<Tweet> extractTweetsFromCSVFile(String fileName) throws NumberFormatException, IOException {
		return extractTweetsFromCSVFile(new File(fileName));
	}

	/**
	 * Returns the list of tweets from a csv file passing the file as argument
	 * 
	 * @param file
	 * @return
	 * @throws NumberFormatException
	 * @throws IOException
	 */
	protected List<Tweet> extractTweetsFromCSVFile(File file) throws NumberFormatException, IOException {
		CSVReader reader = new CSVReader(new FileReader(file));
		List<Tweet> tweets = new ArrayList<Tweet>();

		// skip csv's header line
		String[] fields = reader.readNext();
		String temp;

		System.out.println("Start parsing file " + file.getPath());
		while ((fields = reader.readNext()) != null) {

			String text = fields[1];
			Boolean favorited = Boolean.parseBoolean(fields[2]);
			Integer favoriteCount = Integer.parseInt(fields[3]);
			String replyToSN = fields[4];

			Date created;
			try {
				created = formatter.parse(fields[5]);
			} catch (ParseException e) {
				created = null;
			}

			Boolean truncated = Boolean.parseBoolean(fields[6]);

			Double replyToSID;
			if (!fields[7].equals("NA"))
				replyToSID = Double.parseDouble(fields[7]);
			else
				replyToSID = null;

			Double id = Double.parseDouble(fields[8]);
			Integer replyToUID;
			if (!fields[9].equals("NA"))
				replyToUID = Integer.parseInt(fields[9]);
			else
				replyToUID = null;

			String statusSource = fields[10];
			String screenName = fields[11];
			Integer retweetCount = Integer.parseInt(fields[12]);
			Boolean isRetweet = Boolean.parseBoolean(fields[13]);
			Boolean retweeted = Boolean.parseBoolean(fields[14]);

			Double longitude;
			if (!fields[15].equals("NA"))
				longitude = Double.parseDouble(fields[15]);
			else
				longitude = null;

			Double latitude;
			if (!fields[16].equals("NA"))
				latitude = Double.parseDouble(fields[16]);
			else
				latitude = null;

			temp = tweetParser.parseTweet(text);
			Set<String> hashtags = tweetParser.getHashtags();
			Set<String> taggedUsers = tweetParser.getTaggedUsers();
			Set<String> links = tweetParser.getLinks();
			Set<String> emoticons = tweetParser.getEmoticons();
			Set<String> hearts = tweetParser.getHearts();
			Set<String> significantWords = stemmer.stem(stopwordsRemover
					.removeStopwordsFromMyString(temp));

			Tweet tweet = new Tweet(text, favorited, favoriteCount, replyToSN,
					created, truncated, replyToSID, id, replyToUID,
					statusSource, screenName, retweetCount, isRetweet,
					retweeted, longitude, latitude, hashtags, taggedUsers,
					links, emoticons, hearts, significantWords);
			tweets.add(tweet);
		}
		reader.close();

		System.out.println("End parsing file " + file.getPath());
		System.out.println("________________________________");
		return tweets;
	}

	/**
	 * Returns a map of file - list of tweets couples from csv files contained
	 * in a directory passed as argument
	 * 
	 * @param folderName
	 * @return
	 * @throws NumberFormatException
	 * @throws IOException
	 */
	protected Map<File, List<Tweet>> extractTweetsFromCSVDirectory(
			String folderName) throws NumberFormatException, IOException {
		return extractTweetsFromCSVDirectory(new File(folderName));
	}

	/**
	 * Returns a map of file - list of tweets couples from csv files contained
	 * in a directory passed as argument
	 * 
	 * @param folder
	 * @return
	 * @throws NumberFormatException
	 * @throws IOException
	 */
	protected Map<File, List<Tweet>> extractTweetsFromCSVDirectory(File folder)
			throws NumberFormatException, IOException {
		if (!folder.isDirectory())
			return null;

		System.out.println("Start parsing directory " + folder.getPath());
		System.out.println("________________________________");
		File[] listOfFiles = folder.listFiles();
		Map<File, List<Tweet>> parsedCSVs = new HashMap<File, List<Tweet>>();

		for (File file : listOfFiles)
			parsedCSVs.put(file, extractTweetsFromCSVFile(file));

		System.out.println("End parsing directory " + folder.getPath());
		System.out.println("________________________________");
		return parsedCSVs;
	}

	/**
	 * Returns a map of pairs made by users' nicknames and their set of words
	 * 
	 * @param fileName
	 * @throws IOException
	 */
	protected Map<String, Set<String>> extractUsersAndWordsFromCSVFile(
			String fileName) throws IOException {
		return extractUsersAndWordsFromCSVFile(new File(fileName));
	}

	/**
	 * Returns a map of pairs made by users' nicknames and their set of words
	 * 
	 * @param file
	 * @throws IOException
	 */
	protected Map<String, Set<String>> extractUsersAndWordsFromCSVFile(File file) throws IOException {
		CSVReader reader = new CSVReader(new FileReader(file));

		// skip csv's header line
		String[] fields = reader.readNext();

		System.out.println("Start parsing file " + file.getPath());
		while ((fields = reader.readNext()) != null) {
			//System.out.println("Fields size: " + fields.length + " - Fields: " + Arrays.toString(fields));
			if(fields.length == 17) {
				String text = fields[1];
				String screenName = fields[11];
				//stemmer.stem return a ConcurrentSkipListSet
				Set<String> temp = stemmer.stem( stopwordsRemover.removeStopwordsFromMyString(tweetParser.parseTweet(text)) );
				Set<String> significantWords = 
						//new HashSet<>();
						new ConcurrentSkipListSet<>();
				// TODO parallel
				for (String x: temp)
					if (x.length()>1)
						significantWords.add(x);
				temp.clear();
				
				if (usersAndWords.containsKey(screenName))
					usersAndWords.get(screenName).addAll(significantWords);
				else
					usersAndWords.put(screenName, significantWords);
			}
		}
		reader.close();

		System.out.println("End parsing file " + file.getPath());
		System.out.println("________________________________");
		return usersAndWords;
	}

	/**
	 * Returns a map of pairs made by users' nicknames and their set of words
	 * contained in every CSV file in the directory passed as argument
	 * 
	 * @param folderName
	 * @throws IOException
	 */
	public Map<String, Set<String>> extractUsersAndWordsFromCSVDirectoryAndApplyStemming() throws IOException {
		return extractUsersAndWordsFromCSVDirectory(new File(corpusDirPath));
	}

	/**
	 * Returns a map of pairs made by users' nicknames and their set of words
	 * contained in every CSV file in the directory passed as argument
	 * 
	 * @param folder
	 * @throws IOException
	 */
	protected Map<String, Set<String>> extractUsersAndWordsFromCSVDirectory(File folder) throws IOException {
		if (!folder.isDirectory())
			return null;

		System.out.println("Start parsing directory " + folder.getPath());
		System.out.println("________________________________");
		File[] listOfFiles = folder.listFiles();

		//TODO parallel ?
		for (File file : listOfFiles)
			extractUsersAndWordsFromCSVFile(file);

		System.out.println("End parsing directory " + folder.getPath());
		System.out.println("________________________________");
		return usersAndWords;
	}

	/**
	 * Creates a serialized representation of the map of pairs made by users'
	 * nicknames and their set of words
	 * 
	 * @throws IOException
	 */
	public void serializeUsersAndWords(String filePath) throws IOException {
		FileOutputStream fos = new FileOutputStream(filePath);
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		System.out.println("Start serializing file " + filePath);
		oos.writeObject(usersAndWords);
		System.out.println("End serializing file " + filePath);
		System.out.println("________________________________");
		oos.close();
		fos.close();
	}

	/**
	 * Deserialize the file into a map object of pairs made by users' nicknames
	 * and their set of words
	 * 
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	@SuppressWarnings("unchecked")
	public Map<String, Set<String>> deserializeUsersAndWords(String filePath) throws ClassNotFoundException, IOException {
		FileInputStream fis = new FileInputStream(filePath);
		ObjectInputStream ois = new ObjectInputStream(fis);
		System.out.println("Start deserializing file " + filePath);
		Map<String, Set<String>> usersAndWords = (Map<String, Set<String>>) ois
				.readObject();
		System.out.println("End deserializing file " + filePath);
		System.out.println("________________________________");
		ois.close();
		fis.close();
		return usersAndWords;
	}
	
	public void persistUsersWithStemmedWords() {
		// TODO parallel
		for (Entry<String, Set<String>> entrySet: usersAndWords.entrySet()) {
			String userString = entrySet.getKey();
			User user = new User(userString);
			Set<String> stemmedWords = entrySet.getValue();
			userToStemmedWordsDAO.put(user, stemmedWords);
		}
		userToStemmedWordsDAO.commit();
	}
}

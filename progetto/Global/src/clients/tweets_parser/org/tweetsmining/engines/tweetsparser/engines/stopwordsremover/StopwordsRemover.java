package org.tweetsmining.engines.tweetsparser.engines.stopwordsremover;
/*
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see .
 */

/*
 *    Stopwords.java
 *    Copyright (C) 2001-2012 University of Waikato, Hamilton, New Zealand
 */

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Class that can test whether a given string is a stop word. Lowercases all
 * words before the test. You can also delete stopwords from a string.
 * 
 * 
 * @author Eibe Frank (eibe@cs.waikato.ac.nz)
 * @author Ashraf M. Kibriya (amk14@cs.waikato.ac.nz)
 * @author FracPete (fracpete at waikato dot ac dot nz)
 * @author Antonio Notarangelo (<a
 *         href="mailto:antonio.notarangelo@studio.unibo.it"
 *         >antonio.notarangelo@studio.unibo.it</a> - <a
 *         href="http://it.linkedin.com/pub/antonio-notarangelo/87/798/507/"
 *         >LinkedIN Profile</a>)
 */
public class StopwordsRemover {

	/** The hash set containing the list of stopwords */
	private final Set<String> stopWords = new HashSet<String>();
	
	public static final String STOPWORDS_FILE = "stopwords.txt";

	/**
	 * initializes the stopwords
	 */
	public StopwordsRemover() {}
	
	public StopwordsRemover init(String stopwordsFileName) throws IOException {
		// loads stopwords from a file
		read(stopwordsFileName);
		return this;
	}

	/**
	 * initializes the stopwords from a custom file
	 * @throws IOException 
	 */
	@Inject
	public StopwordsRemover(@Named("stemmer.resources_dir") String dirPathForConfigFile) throws IOException  {
		read(dirPathForConfigFile+File.separatorChar+STOPWORDS_FILE);
	}

	/**
	 * removes all stopwords
	 */
	public void clear() {
		stopWords.clear();
	}

	/**
	 * adds the given word to the stopword list (is automatically converted to
	 * lower case and trimmed)
	 * 
	 * @param word
	 *            the word to add
	 */
	public void add(String word) {
		if (word.trim().length() > 0)
			stopWords.add(word.trim().toLowerCase());
	}

	/**
	 * removes the word from the stopword list
	 * 
	 * @param word
	 *            the word to remove
	 * @return true if the word was found in the list and then removed
	 */
	public boolean remove(String word) {
		return stopWords.remove(word);
	}

	/**
	 * Returns true if the given string is a stop word.
	 * 
	 * @param word
	 *            the word to test
	 * @return true if the word is a stopword
	 */
	public boolean isStopword(String word) {
		return stopWords.contains(word.toLowerCase());
	}

	/**
	 * Returns a sorted enumeration over all stored stopwords
	 * 
	 * @return the enumeration over all stopwords
	 */
	public Enumeration<String> elements() {
		Iterator<String> iter;
		Vector<String> list;

		iter = stopWords.iterator();
		list = new Vector<String>();

		while (iter.hasNext())
			list.add(iter.next());

		// sort list
		Collections.sort(list);

		return list.elements();
	}

	/**
	 * Generates a new Stopwords object from the given file
	 * 
	 * @param filename
	 *            the file to read the stopwords from
	 * @throws IOException 
	 * @throws Exception
	 *             if reading fails
	 */
	public void read(String filename) throws IOException {
		read(new File(filename));
	}

	/**
	 * Generates a new Stopwords object from the given file
	 * 
	 * @param file
	 *            the file to read the stopwords from
	 * @throws IOException 
	 * @throws Exception
	 *             if reading fails
	 */
	public void read(File file) throws IOException {
		FileReader fileReader = new FileReader(file);
		BufferedReader bufferedReader = new BufferedReader(fileReader);
		read( bufferedReader );
	}

	/**
	 * Generates a new Stopwords object from the reader. The reader is closed
	 * automatically.
	 * 
	 * @param reader
	 *            the reader to get the stopwords from
	 * @throws IOException 
	 * @throws Exception
	 *             if reading fails
	 */
	public void read(BufferedReader reader) throws IOException {
		String line;
		clear();

		while ((line = reader.readLine()) != null) {
			line = line.trim();
			// comment?
			if (line.startsWith("#"))
				continue;
			add(line);
		}

		reader.close();
	}

	/**
	 * Writes the current stopwords to the given file
	 * 
	 * @param filename
	 *            the file to write the stopwords to
	 * @throws Exception
	 *             if writing fails
	 */
	public void write(String filename) throws Exception {
		write(new File(filename));
	}

	/**
	 * Writes the current stopwords to the given file
	 * 
	 * @param file
	 *            the file to write the stopwords to
	 * @throws Exception
	 *             if writing fails
	 */
	public void write(File file) throws Exception {
		write(new BufferedWriter(new FileWriter(file)));
	}

	/**
	 * Writes the current stopwords to the given writer. The writer is closed
	 * automatically.
	 * 
	 * @param writer
	 *            the writer to get the stopwords from
	 * @throws Exception
	 *             if writing fails
	 */
	public void write(BufferedWriter writer) throws Exception {
		Enumeration<?> enm;

		// header
		writer.write("# generated " + new Date());
		writer.newLine();

		enm = elements();

		while (enm.hasMoreElements()) {
			writer.write(enm.nextElement().toString());
			writer.newLine();
		}

		writer.flush();
		writer.close();
	}

	/**
	 * returns the current stopwords in a string
	 * 
	 * @return the current stopwords
	 */
	public String toString() {
		Enumeration<?> enm;
		StringBuffer result;

		result = new StringBuffer();
		enm = elements();
		while (enm.hasMoreElements()) {
			result.append(enm.nextElement().toString());
			if (enm.hasMoreElements())
				result.append(",");
		}

		return result.toString();
	}

	/**
	 * returns the set of words without any stopword
	 * 
	 * @return the set of words without any stopword
	 */
	public Set<String> removeStopwordsFromMyString(String myString) {
		String[] wordsList = myString
				.split("([ \t\n,'\\.\"!?$~()\\[\\]\\{\\}:;/\\\\<>+=%*\\-\\|\\d])+");
		for (int i = 0; i < wordsList.length; i++)
			wordsList[i] = wordsList[i].toLowerCase();
		Set<String> words = new HashSet<String>(Arrays.asList(wordsList));

		// difference between two sets
		words.removeAll(stopWords);

		return words;
	}

	/**
	 * Accepts the following parameter:
	 * 
	 * 
	 * 
	 * -i file
	 * 
	 * loads the stopwords from the given file
	 * 
	 * 
	 * -o file
	 * 
	 * saves the stopwords to the given file
	 * 
	 * 
	 * -p
	 * 
	 * outputs the current stopwords on stdout
	 * 
	 * 
	 * Any additional parameters are interpreted as words to test as stopwords.
	 * 
	 * @param args
	 *            commandline parameters
	 * @throws Exception
	 *             if something goes wrong
	 */
	public static void main(String[] args) throws Exception {
		String input = args[0];
		String output = args[1];
		boolean print = Boolean.parseBoolean(args[2]);

		// words to process?
		Vector<String> words = new Vector<String>();
		for (int i = 0; i < args.length; i++) {
			if (args[i].trim().length() > 0)
				words.add(args[i].trim());
		}

		StopwordsRemover stopwordsRemover = new StopwordsRemover();

		// load from file?
		if (input.length() != 0)
			stopwordsRemover.read(input);

		// write to file?
		if (output.length() != 0)
			stopwordsRemover.write(output);

		// output to stdout?
		if (print) {
			System.out.println("\nStopwords:");
			Enumeration<?> enm = stopwordsRemover.elements();
			int i = 0;
			while (enm.hasMoreElements()) {
				System.out.println((i + 1) + ". " + enm.nextElement());
				i++;
			}
		}

		// check words for being a stopword
		if (words.size() > 0) {
			System.out.println("\nChecking for stopwords:");
			for (int i = 0; i < words.size(); i++) {
				System.out.println((i + 1) + ". " + words.get(i) + ": "
						+ stopwordsRemover.isStopword(words.get(i).toString()));
			}
		}

		// deletes stopwords from a string
		String str = "RT @EuropeRider: Not sure how reliable this is, but it won't hurt to change your passwords..... #heartbleed #SSL #hackers http://t.co/gEVYL&";
		System.out.println("Before stopwords removal: " + str);
		Set<String> significantWords = stopwordsRemover
				.removeStopwordsFromMyString(str);
		System.out.println("After stopwords removal: " + significantWords);
	}
}
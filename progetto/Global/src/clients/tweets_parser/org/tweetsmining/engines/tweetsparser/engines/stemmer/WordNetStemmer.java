package org.tweetsmining.engines.tweetsparser.engines.stemmer;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;

import javax.inject.Inject;
import javax.inject.Named;

import net.didion.jwnl.JWNL;
import net.didion.jwnl.JWNLException;
import net.didion.jwnl.data.IndexWord;
import net.didion.jwnl.data.POS;
import net.didion.jwnl.dictionary.Dictionary;
import net.didion.jwnl.dictionary.MorphologicalProcessor;

/**
 * Class that can stem a word to its root form (es: "wolves -> wolf", "running"
 * -> "run") A WordNet installation on your pc is needed in order to use this
 * class Build your own properties file and then pass it to the constructor How
 * to build your own xlm properties file: <a href=
 * "https://www.stanford.edu/class/cs276a/projects/docs/jwnl/property%20file%20configuration.txt"
 * >XML properties configuration</a>
 * 
 * 
 * @author Andreas Hartl (<a
 *         href="http://tipsandtricks.runicsoft.com/Other/JavaStemmer.html"
 *         >WordNet and JWNL stemmer</a>)
 * @author Antonio Notarangelo (<a
 *         href="mailto:antonio.notarangelo@studio.unibo.it"
 *         >antonio.notarangelo@studio.unibo.it</a> - <a
 *         href="http://it.linkedin.com/pub/antonio-notarangelo/87/798/507/"
 *         >LinkedIN Profile</a>)
 */
public class WordNetStemmer {
	private Dictionary dic;
	private MorphologicalProcessor morph;
	private boolean isInitialized = false;
	private final Map<String, String> allWords = new HashMap<String, String>();

	/**
	 * establishes connection to the WordNet database, passing parent dir for default properties file 
	 */
	@Inject
	public WordNetStemmer(@Named("stemmer.resources_dir") String dirPathForConfigFile) {
		try {
			FileInputStream fileInputStream = new FileInputStream(dirPathForConfigFile+File.separatorChar+"jwnl-properties.xml");
			JWNL.initialize(fileInputStream);
			dic = Dictionary.getInstance();
			morph = dic.getMorphologicalProcessor();
			isInitialized = true;
		} catch (FileNotFoundException e) {
			System.out.println("Error initializing Stemmer: jwnl-properties.xml not found");
			e.printStackTrace();
		} catch (Throwable e) {
			e.printStackTrace();
			System.out.println("Error initializing Stemmer: " + e.toString());
		}
	}

	/**
	 * establishes connection to the WordNet database using a custom properties
	 * file
	 * @param dirPathForConfigFile
	 * @param configFileName
	 */
	/*
	public WordNetStemmer(String dirPathForConfigFile, String configFileName) {
		allWords = new HashMap<String, String>();

		try {
			JWNL.initialize(new FileInputStream(dirPathForConfigFile+File.separatorChar+configFileName));
			dic = Dictionary.getInstance();
			morph = dic.getMorphologicalProcessor();
			isInitialized = true;
		} catch (FileNotFoundException e) {
			System.out
					.println("Error initializing Stemmer: "+configFileName+" not found");
		} catch (JWNLException e) {
			System.out.println("Error initializing Stemmer: " + e.toString());
		}
	}*/

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#finalize()
	 */
	@Override
	protected void finalize() throws Throwable {
		try {
			dic.close();
			Dictionary.uninstall();
			JWNL.shutdown();
		} catch (Throwable t) {
			throw t;
		} finally {
			super.finalize();
		}
	}

	/**
	 * stems a word with wordnet
	 * 
	 * @param word
	 *            word to stem
	 * @return the stemmed word or null if it was not found in WordNet
	 */
	private String stemWordWithWordNet(String word) {
		if (!isInitialized)
			return word;
		if (word == null)
			return null;
		if (morph == null)
			morph = dic.getMorphologicalProcessor();

		IndexWord w;
		try {
			w = morph.lookupBaseForm(POS.VERB, word);
			if (w != null)
				return w.getLemma().toString();
			w = morph.lookupBaseForm(POS.NOUN, word);
			if (w != null)
				return w.getLemma().toString();
			w = morph.lookupBaseForm(POS.ADJECTIVE, word);
			if (w != null)
				return w.getLemma().toString();
			w = morph.lookupBaseForm(POS.ADVERB, word);
			if (w != null)
				return w.getLemma().toString();
		} catch (JWNLException e) {
		}
		return null;
	}

	/**
	 * Stem a single word tries to look up the word in the AllWords HashMap If
	 * the word is not found it is stemmed with WordNet and put into AllWords
	 * 
	 * @param word
	 *            word to be stemmed
	 * @return stemmed word
	 */
	public String stem(String word) {
		// check if we already know the word
		String stemmedword = allWords.get(word);
		if (stemmedword != null)
			return stemmedword; // return it if we already know it

		// don't check words with digits in them
		if (word.matches(".*\\d.*"))
			stemmedword = null;
		else
			// unknown word: try to stem it
			stemmedword = stemWordWithWordNet(word);

		if (stemmedword != null) {
			// word was recognized and stemmed with wordnet:
			// add it to hashmap and return the stemmed word
			allWords.put(word, stemmedword);
			return stemmedword;
		}
		// word could not be stemmed by wordnet,
		// thus it is no correct english word
		// just add it to the list of known words so
		// we won't have to look it up again
		allWords.put(word, word);
		return word;
	}

	/**
	 * performs Stem on each element in the given list
	 * @param words
	 * @return
	 */
	/*
	public Map<String, String> stem(List<String> words) {
		if (!isInitialized)
			return null;

		Map<String, String> stemmedCouples = new HashMap<String, String>();

		for (String word : words)
			stemmedCouples.put(word, stem(word));

		return stemmedCouples;
	}*/

	/**
	 * performs Stem on each element in the given set
	 * @param words
	 * @return
	 */
	public Set<String> stem(Set<String> words) {
		if (!isInitialized)
			return null;

		// for concurrence
		Set<String> stemmedWords = 
				//new HashSet<String>();
				new ConcurrentSkipListSet<>();

		// TODO parallel
		for (String word : words)
			stemmedWords.add(stem(word));

		return stemmedWords;
	}

	public static void main(String[] args) {
		String str = "wolves";
		WordNetStemmer stemmer = new WordNetStemmer(args[0]);
		String stemmedWord = stemmer.stem(str);
		System.out.println("Before stemming: " + str + "; After stemming: " + stemmedWord);
	}

}

/**
 * 
 */
package org.tweetsmining.engines.tweetsparser._di.guice.provider;

import java.text.SimpleDateFormat;

import javax.inject.Provider;
import javax.inject.Singleton;

/**
 * @author k0smik0
 *
 */
@Singleton
public class TweetDateFormatProvider implements Provider<SimpleDateFormat> {

	private final SimpleDateFormat dateFormat;

	public TweetDateFormatProvider() {
		dateFormat = new SimpleDateFormat();
		String dateFormatPattern = "yyyy-MM-dd HH:mm:ss";
		dateFormat.applyLocalizedPattern(dateFormatPattern);
	}
	
	@Override
	public SimpleDateFormat get() {
		return dateFormat;
	}

}

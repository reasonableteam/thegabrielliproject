package org.tweetsmining.engines.tweetsparser._di.guice.module.core;

import java.text.SimpleDateFormat;

import org.tweetsmining.engines.tweetsparser._di.annotations.TweetDateFormat;
import org.tweetsmining.engines.tweetsparser._di.guice.provider.TweetDateFormatProvider;

import com.google.inject.AbstractModule;

public class TweetsParserCoreModule extends AbstractModule {

	@Override
	protected void configure() {
		bind(SimpleDateFormat.class).annotatedWith(TweetDateFormat.class).toProvider(TweetDateFormatProvider.class);
	}
}

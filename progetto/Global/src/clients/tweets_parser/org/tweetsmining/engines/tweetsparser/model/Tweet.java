package org.tweetsmining.engines.tweetsparser.model;
import java.util.Date;
import java.util.Set;

/**
 * Class that can hold every field pulled from a csv file which contains these
 * 16 fields:
 * <ul>
 * <li>text</li>
 * <li>favorited</li>
 * <li>favoriteCount</li>
 * <li>replyToSN</li>
 * <li>created</li>
 * <li>truncated</li>
 * <li>replyToSID</li>
 * <li>id</li>
 * <li>replyToUID</li>
 * <li>statusSource</li>
 * <li>screenName</li>
 * <li>retweetCount</li>
 * <li>isRetweet</li>
 * <li>retweeted</li>
 * <li>longitude</li>
 * <li>latitude</li>
 * </ul>
 * The instance of this class will also contain 
 * <ul>
 * <li>hashtags</li>
 * <li>taggedUsers</li>
 * <li>links</li>
 * <li>emoticons</li>
 * <li>hearts</li>
 * <li>significantWords</li>
 * </ul>
 * 
 * 
 * @author Antonio Notarangelo (<a
 *         href="mailto:antonio.notarangelo@studio.unibo.it"
 *         >antonio.notarangelo@studio.unibo.it</a> - <a
 *         href="http://it.linkedin.com/pub/antonio-notarangelo/87/798/507/"
 *         >LinkedIN Profile</a>)
 */
public class Tweet {
	private String text;
	private Boolean favorited;
	private Integer favoriteCount;
	private String replyToSN;
	private Date created;
	private Boolean truncated;
	private Double replyToSID;
	private Double id;
	private Integer replyToUID;
	private String statusSource;
	private String screenName;
	private Integer retweetCount;
	private Boolean isRetweet;
	private Boolean retweeted;
	private Double longitude;
	private Double latitude;
	private Set<String> hashtags;
	private Set<String> taggedUsers;
	private Set<String> links;
	private Set<String> emoticons;
	private Set<String> hearts;
	private Set<String> significantWords;

	/**
	 * @param text
	 * @param favorited
	 * @param favoriteCount
	 * @param replyToSN
	 * @param created
	 * @param truncated
	 * @param replyToSID
	 * @param id
	 * @param replyToUID
	 * @param statusSource
	 * @param screenName
	 * @param retweetCount
	 * @param isRetweet
	 * @param retweeted
	 * @param longitude
	 * @param latitude
	 * @param hashtags
	 * @param taggedUsers
	 * @param links
	 * @param emoticons
	 * @param hearts
	 * @param significantWords
	 */
	public Tweet(String text, Boolean favorited, Integer favoriteCount,
			String replyToSN, Date created, Boolean truncated,
			Double replyToSID, Double id, Integer replyToUID,
			String statusSource, String screenName, Integer retweetCount,
			Boolean isRetweet, Boolean retweeted, Double longitude,
			Double latitude, Set<String> hashtags, Set<String> taggedUsers,
			Set<String> links, Set<String> emoticons, Set<String> hearts,
			Set<String> significantWords) {
		this.text = text;
		this.favorited = favorited;
		this.favoriteCount = favoriteCount;
		this.replyToSN = replyToSN;
		this.created = created;
		this.truncated = truncated;
		this.replyToSID = replyToSID;
		this.id = id;
		this.replyToUID = replyToUID;
		this.statusSource = statusSource;
		this.screenName = screenName;
		this.retweetCount = retweetCount;
		this.isRetweet = isRetweet;
		this.retweeted = retweeted;
		this.longitude = longitude;
		this.latitude = latitude;
		this.hashtags = hashtags;
		this.taggedUsers = taggedUsers;
		this.links = links;
		this.emoticons = emoticons;
		this.hearts = hearts;
		this.significantWords = significantWords;
	}

	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}

	/**
	 * @param text
	 *            the text to set
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * @return the favorited
	 */
	public Boolean getFavorited() {
		return favorited;
	}

	/**
	 * @param favorited
	 *            the favorited to set
	 */
	public void setFavorited(Boolean favorited) {
		this.favorited = favorited;
	}

	/**
	 * @return the favoriteCount
	 */
	public Integer getFavoriteCount() {
		return favoriteCount;
	}

	/**
	 * @param favoriteCount
	 *            the favoriteCount to set
	 */
	public void setFavoriteCount(Integer favoriteCount) {
		this.favoriteCount = favoriteCount;
	}

	/**
	 * @return the replyToSN
	 */
	public String getReplyToSN() {
		return replyToSN;
	}

	/**
	 * @param replyToSN
	 *            the replyToSN to set
	 */
	public void setReplyToSN(String replyToSN) {
		this.replyToSN = replyToSN;
	}

	/**
	 * @return the created
	 */
	public Date getCreated() {
		return created;
	}

	/**
	 * @param created
	 *            the created to set
	 */
	public void setCreated(Date created) {
		this.created = created;
	}

	/**
	 * @return the truncated
	 */
	public Boolean getTruncated() {
		return truncated;
	}

	/**
	 * @param truncated
	 *            the truncated to set
	 */
	public void setTruncated(Boolean truncated) {
		this.truncated = truncated;
	}

	/**
	 * @return the replyToSID
	 */
	public Double getReplyToSID() {
		return replyToSID;
	}

	/**
	 * @param replyToSID
	 *            the replyToSID to set
	 */
	public void setReplyToSID(Double replyToSID) {
		this.replyToSID = replyToSID;
	}

	/**
	 * @return the id
	 */
	public Double getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Double id) {
		this.id = id;
	}

	/**
	 * @return the replyToUID
	 */
	public Integer getReplyToUID() {
		return replyToUID;
	}

	/**
	 * @param replyToUID
	 *            the replyToUID to set
	 */
	public void setReplyToUID(Integer replyToUID) {
		this.replyToUID = replyToUID;
	}

	/**
	 * @return the statusSource
	 */
	public String getStatusSource() {
		return statusSource;
	}

	/**
	 * @param statusSource
	 *            the statusSource to set
	 */
	public void setStatusSource(String statusSource) {
		this.statusSource = statusSource;
	}

	/**
	 * @return the screenName
	 */
	public String getScreenName() {
		return screenName;
	}

	/**
	 * @param screenName
	 *            the screenName to set
	 */
	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}

	/**
	 * @return the retweetCount
	 */
	public Integer getRetweetCount() {
		return retweetCount;
	}

	/**
	 * @param retweetCount
	 *            the retweetCount to set
	 */
	public void setRetweetCount(Integer retweetCount) {
		this.retweetCount = retweetCount;
	}

	/**
	 * @return the isRetweet
	 */
	public Boolean getIsRetweet() {
		return isRetweet;
	}

	/**
	 * @param isRetweet
	 *            the isRetweet to set
	 */
	public void setIsRetweet(Boolean isRetweet) {
		this.isRetweet = isRetweet;
	}

	/**
	 * @return the retweeted
	 */
	public Boolean getRetweeted() {
		return retweeted;
	}

	/**
	 * @param retweeted
	 *            the retweeted to set
	 */
	public void setRetweeted(Boolean retweeted) {
		this.retweeted = retweeted;
	}

	/**
	 * @return the longitude
	 */
	public Double getLongitude() {
		return longitude;
	}

	/**
	 * @param longitude
	 *            the longitude to set
	 */
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	/**
	 * @return the latitude
	 */
	public Double getLatitude() {
		return latitude;
	}

	/**
	 * @param latitude
	 *            the latitude to set
	 */
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	/**
	 * @return the hashtags
	 */
	public Set<String> getHashtags() {
		return hashtags;
	}

	/**
	 * @param hashtags
	 *            the hashtags to set
	 */
	public void setHashtags(Set<String> hashtags) {
		this.hashtags = hashtags;
	}

	/**
	 * @return the taggedUsers
	 */
	public Set<String> getTaggedUsers() {
		return taggedUsers;
	}

	/**
	 * @param taggedUsers
	 *            the taggedUsers to set
	 */
	public void setTaggedUsers(Set<String> taggedUsers) {
		this.taggedUsers = taggedUsers;
	}

	/**
	 * @return the links
	 */
	public Set<String> getLinks() {
		return links;
	}

	/**
	 * @param links
	 *            the links to set
	 */
	public void setLinks(Set<String> links) {
		this.links = links;
	}

	/**
	 * @return the emoticons
	 */
	public Set<String> getEmoticons() {
		return emoticons;
	}

	/**
	 * @param emoticons
	 *            the emoticons to set
	 */
	public void setEmoticons(Set<String> emoticons) {
		this.emoticons = emoticons;
	}

	/**
	 * @return the hearts
	 */
	public Set<String> getHearts() {
		return hearts;
	}

	/**
	 * @param hearts
	 *            the hearts to set
	 */
	public void setHearts(Set<String> hearts) {
		this.hearts = hearts;
	}

	/**
	 * @return the significantWords
	 */
	public Set<String> getSignificantWords() {
		return significantWords;
	}

	/**
	 * @param significantWords
	 *            the significantWords to set
	 */
	public void setSignificantWords(Set<String> significantWords) {
		this.significantWords = significantWords;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Tweet [text=" + text + ", favorited=" + favorited
				+ ", favoriteCount=" + favoriteCount + ", replyToSN="
				+ replyToSN + ", created=" + created + ", truncated="
				+ truncated + ", replyToSID=" + replyToSID + ", id=" + id
				+ ", replyToUID=" + replyToUID + ", statusSource="
				+ statusSource + ", screenName=" + screenName
				+ ", retweetCount=" + retweetCount + ", isRetweet=" + isRetweet
				+ ", retweeted=" + retweeted + ", longitude=" + longitude
				+ ", latitude=" + latitude + ", hashtags=" + hashtags
				+ ", taggedUsers=" + taggedUsers + ", links=" + links
				+ ", emoticons=" + emoticons + ", hearts=" + hearts
				+ ", significantWords=" + significantWords + "]";
	}

}

package org.tweetsmining.engines.tweetsparser.engines.tweetparser;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class which extracts from a tweet every important info such as:
 * <ul>
 * <li>hashtags</li>
 * <li>taggedUsers</li>
 * <li>links</li>
 * <li>emoticons</li>
 * <li>hearts</li>
 * </ul>
 * 
 * 
 * @author Antonio Notarangelo (<a
 *         href="mailto:antonio.notarangelo@studio.unibo.it"
 *         >antonio.notarangelo@studio.unibo.it</a> - <a
 *         href="http://it.linkedin.com/pub/antonio-notarangelo/87/798/507/"
 *         >LinkedIN Profile</a>)
 */
public class TweetParser {

	// sets of parsed data
	private Set<String> hashtags;
	private Set<String> taggedUsers;
	private Set<String> links;
	private Set<String> emoticons;
	private Set<String> hearts;

	// Hashtags
	private String hashtagPattern = "(?:\\s|\\A)[##]+([A-Za-z0-9-_]+)";

	// Users
	private String userPattern = "(?:\\s|\\A)[@]+([A-Za-z0-9-_]+)";

	// Links
	private String linkPattern = "(^|[ \t\r\n])((ftp|http|https|mailto|aim|webcal|skype):(([A-Za-z0-9$_.+!*(),;/?:@&~=-])|%[A-Fa-f0-9]{2}){2,}(#([a-zA-Z0-9][a-zA-Z0-9$_.+!*(),;/?:@&~=%-]*))?([A-Za-z0-9$_+!*();/?:~-]))";

	// Emoticons
	private String normalEyes = "(?iu)[:=]"; // 8 and x are eyes but cause
												// problems
	private String wink = "[;]";
	private String noseArea = "(?:|-|[^a-zA-Z0-9 ])"; // doesn't get :'-(
	private String happyMouths = "[D\\)\\]\\}]+";
	private String sadMouths = "[\\(\\[\\{]+";
	private String tongue = "[pPd3]+";
	private String otherMouths = "(?:[oO]+|[/\\\\]+|[vV]+|[Ss]+|[|]+)"; // remove
																		// forward
																		// slash
																		// if
																		// http://'s
																		// aren't
																		// cleaned

	// mouth repetition examples:
	// @aliciakeys Put it in a love song :-))
	// @hellocalyclops =))=))=)) Oh well
	private String bfLeft = "(♥|0|o|°|v|\\$|t|x|;|\\u0CA0|@|ʘ|•|・|◕|\\^|¬|\\*)";
	private String bfCenter = "(?:[\\.]|[_-]+)";
	private String bfRight = "\\2";
	private String s3 = "(?:--['\"])";
	private String s4 = "(?:<|&lt;|>|&gt;)[\\._-]+(?:<|&lt;|>|&gt;)";
	private String s5 = "(?:[.][_]+[.])";
	private String basicface = "(?:(?i)" + bfLeft + bfCenter + bfRight + ")|"
			+ s3 + "|" + s4 + "|" + s5;

	private String eeLeft = "[＼\\\\ƪԄ\\(（<>;ヽ\\-=~\\*]+";
	private String eeRight = "[\\-=\\);'\\u0022<>ʃ）/／ノﾉ丿╯σっµ~\\*]+";
	private String eeSymbol = "[^A-Za-z0-9\\s\\(\\)\\*:=-]";
	private String eastEmote = eeLeft + "(?:" + basicface + "|" + eeSymbol
			+ ")+" + eeRight;

	public String emoticonPattern = OR(
	// Standard version :) :( :] :D :P
			"(?:>|&gt;)?"
					+ OR(normalEyes, wink)
					+ OR(noseArea, "[Oo]")
					+ OR(tongue + "(?=\\W|$|RT|rt|Rt)", otherMouths
							+ "(?=\\W|$|RT|rt|Rt)", sadMouths, happyMouths),

			// reversed version (: D: use positive lookbehind to remove
			// "(word):"
			// because eyes on the right side is more ambiguous with the
			// standard usage of : ;
			"(?<=(?: |^))" + OR(sadMouths, happyMouths, otherMouths) + noseArea
					+ OR(normalEyes, wink) + "(?:<|&lt;)?",

			// inspired by
			// http://en.wikipedia.org/wiki/User:Scapler/emoticons#East_Asian_style
			eastEmote.replaceFirst("2", "1"), basicface
	// iOS 'emoji' characters (some smileys, some symbols) [\ue001-\uebbb]
	// TODO should try a big precompiled lexicon from Wikipedia, Dan Ramage told
	// me (BTO) he does this
	);

	// Hearts
	private String heartPattern = "(?:<+/?3+)+"; // the other hearts are in
													// decorations

	/**
	 * Constructor of the class
	 */
	public TweetParser() {
		this.hashtags = new HashSet<String>();
		this.taggedUsers = new HashSet<String>();
		this.links = new HashSet<String>();
		this.emoticons = new HashSet<String>();
		this.hearts = new HashSet<String>();
	}

	/**
	 * Function which puts together some regex patterns
	 * 
	 * @param parts
	 * @return
	 */
	private String OR(String... parts) {
		String prefix = "(?:";
		StringBuilder sb = new StringBuilder();
		for (String s : parts) {
			sb.append(prefix);
			prefix = "|";
			sb.append(s);
		}
		sb.append(")");
		return sb.toString();
	}

	/**
	 * Function which extracts infos from the tweet, stores them in each set and
	 * returns the tweet without the infos
	 * 
	 * @param tweet
	 * @return
	 */
	public String parseTweet(String tweet) {
		Pattern pattern;
		Matcher matcher;
		String foundValue = "";
		String tempTweet = new String(tweet);

		// clear all sets
		hashtags.clear();
		taggedUsers.clear();
		links.clear();
		emoticons.clear();
		hearts.clear();

		// hashtags
		pattern = Pattern.compile(hashtagPattern);
		matcher = pattern.matcher(tempTweet);
		while (matcher.find()) {
			foundValue = matcher.group();
			hashtags.add(foundValue.trim());
			tempTweet = tempTweet.replace(foundValue, "");
		}

		// Users
		pattern = Pattern.compile(userPattern);
		matcher = pattern.matcher(tempTweet);
		while (matcher.find()) {
			foundValue = matcher.group();
			taggedUsers.add(foundValue.trim());
			tempTweet = tempTweet.replace(foundValue, "");
		}

		// links
		pattern = Pattern.compile(linkPattern);
		matcher = pattern.matcher(tempTweet);
		while (matcher.find()) {
			foundValue = matcher.group();
			links.add(foundValue.trim());
			tempTweet = tempTweet.replace(foundValue, "");
		}

		// emoticons
		pattern = Pattern.compile(emoticonPattern);
		matcher = pattern.matcher(tempTweet);
		while (matcher.find()) {
			foundValue = matcher.group();
			emoticons.add(foundValue.trim());
			tempTweet = tempTweet.replace(foundValue, "");
		}

		// hearts
		pattern = Pattern.compile(heartPattern);
		matcher = pattern.matcher(tempTweet);
		while (matcher.find()) {
			foundValue = matcher.group();
			hearts.add(foundValue.trim());
			tempTweet = tempTweet.replace(foundValue, "");
		}

		return tempTweet;
	}

	/**
	 * @return the hashtags
	 */
	public Set<String> getHashtags() {
		return hashtags;
	}

	/**
	 * @return the taggedUsers
	 */
	public Set<String> getTaggedUsers() {
		return taggedUsers;
	}

	/**
	 * @return the links
	 */
	public Set<String> getLinks() {
		return links;
	}

	/**
	 * @return the emoticons
	 */
	public Set<String> getEmoticons() {
		return emoticons;
	}

	/**
	 * @return the hearts
	 */
	public Set<String> getHearts() {
		return hearts;
	}

	public static void main(String[] args) {
		String str = "RT @EuropeRider: Not sure how reliable this is, but it won't hurt to change your passwords..... #heartbleed #SSL #hackers http://t.co/gEVYL&";
		TweetParser tweetParser = new TweetParser();
		System.out.println("Before parsing: " + str);
		str = tweetParser.parseTweet(str);
		System.out.println("After parsing: " + str);
		System.out.println("Hashtags: " + tweetParser.getHashtags());
		System.out.println("Users: " + tweetParser.getTaggedUsers());
		System.out.println("Links: " + tweetParser.getLinks());
	}
}

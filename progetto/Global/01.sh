#!/bin/bash

getJarsPath() {
   local jars=$(for i in $(find libs -type f -iname *jar | grep -v datanu | grep -v 2008); do echo -n "$i:" ; done)
   echo $jars
}

CLASSPATH="$(getJarsPath)""bin"

#echo $CLASSPATH
java -cp $CLASSPATH org.tweetsmining.main.engines.tweetsparser.TweetsParserMain

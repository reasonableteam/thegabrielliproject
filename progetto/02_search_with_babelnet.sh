#!/bin/bash

#CLASSPATH="../libs/guice/*:WordsRelator/bin:BabelnetSearcher/libs/babelnet/2.5/needed/*:BabelnetSearcher/libs/babelnet/2.5/*:BabelnetSearcher/bin"

CLASSPATH="../libs/guice/javax.inject.jar:../libs/guice/aopalliance-1.0.jar:../libs/guice/guice-3.0.jar:libs/babelnet/2.5/needed/je-4.1.6.jar:libs/babelnet/2.5/needed/commons-configuration-1.5.jar:libs/babelnet/2.5/needed/jlt-1.0.0.jar:libs/babelnet/2.5/needed/httpcore-4.2.2.jar:libs/babelnet/2.5/needed/omegawiki-api-v2.jar:libs/babelnet/2.5/needed/commons-io-2.0.1.jar:libs/babelnet/2.5/needed/jena-sdb-1.4.0.jar:libs/babelnet/2.5/needed/omegawiki-api-0.1alpha.jar:libs/babelnet/2.5/needed/jgrapht-0.8.0.jar:libs/babelnet/2.5/needed/jung-graph-impl-2.0.1.jar:libs/babelnet/2.5/needed/commons-logging-1.1.1.jar:libs/babelnet/2.5/needed/jena-core-2.11.0.jar:libs/babelnet/2.5/needed/commons-collections-3.2.1.jar:libs/babelnet/2.5/needed/jena-iri-1.0.0.jar:libs/babelnet/2.5/needed/slf4j-log4j12-1.6.4.jar:libs/babelnet/2.5/needed/jwi-2.1.4.jar:libs/babelnet/2.5/needed/xml-apis-1.4.01.jar:libs/babelnet/2.5/needed/collections-generic-4.01.jar:libs/babelnet/2.5/needed/mysql-connector-java-5.1.12-bin.jar:libs/babelnet/2.5/needed/jung-algorithms-2.0.1.jar:libs/babelnet/2.5/needed/jung-api-2.0.1.jar:libs/babelnet/2.5/needed/httpclient-4.2.3.jar:libs/babelnet/2.5/needed/log4j-1.2.14.jar:libs/babelnet/2.5/needed/xercesImpl-2.11.0.jar:libs/babelnet/2.5/needed/commons-httpclient-3.1.jar:libs/babelnet/2.5/needed/commons-lang-2.4.jar:libs/babelnet/2.5/needed/jbzip2-0.9.1.jar:libs/babelnet/2.5/needed/jena-arq-2.11.0.jar:libs/babelnet/2.5/needed/lucene-core-2.9.1.jar:libs/babelnet/2.5/needed/owlapi-distribution-3.4.8.jar:libs/babelnet/2.5/needed/commons-beanutils.jar:libs/babelnet/2.5/needed/jena-tdb-1.0.0.jar:libs/babelnet/2.5/needed/slf4j-api-1.6.4.jar:libs/babelnet/2.5/needed/commons-codec-1.4.jar:libs/babelnet/2.5/needed/google-collections-20080820.jar:libs/babelnet/2.5/babelnet-api-2.5.jar:../WordsRelator/bin:./bin"


#java -cp -e "../libs/guice/*:WordsRelator/bin:BabelnetSearcher/libs/babelnet/2.5/needed/*:BabelnetSearcher/libs/babelnet/2.5/*:BabelnetSearcher/bin" org.tweetsmining.engines.babelnetsearcher.BabelnetSearcherMain" "$@"

cd WordsRelatorByBabelnet

INPUT="../$1"
OUTPUT="../$2"

java -cp $CLASSPATH org.tweetsmining.engines.wordsrelatorbybabelnet.WordsRelatorByBabelnetMain $INPUT $OUTPUT

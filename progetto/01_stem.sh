#!/bin/bash

#[ $# -ne 1 ] && echo "needed corpusName" && exit 1
#DIR_PARENT="$1"
#DIR_TO_PARSE="$2"

function getJarsPath() {
   DIR=$1
   local path
   for i in $(ls -1 $DIR/*jar); do path="$path:$i" ; done
   echo $path
}

COUCHDB_JARS="libs/couchdb4j/commons-beanutils-1.8.0.jar:libs/couchdb4j/commons-codec-1.6.jar:libs/couchdb4j/commons-collections-3.2.jar:libs/couchdb4j/commons-httpclient-3.1.jar:libs/couchdb4j/commons-lang-2.3.jar:libs/couchdb4j/commons-logging-1.1.3.jar:libs/couchdb4j/commons-logging-adapters-1.1.3.jar:libs/couchdb4j/couchdb4j-0.1.2.jar:libs/couchdb4j/ezmorph-1.0.4.jar:libs/couchdb4j/fluent-hc-4.3.4.jar:libs/couchdb4j/json-lib-2.4-jdk15.jar" # "libs/couchdb4j/*"
GUICE_JARS="libs/guice/aopalliance-1.0.jar:libs/guice/guice-3.0.jar:libs/guice/javax.inject.jar" #"libs/guice/*" 
JENA_JARS="libs/jena/jena-core-2.10.0.jar"
APACHE_COLLECTIONS_JARS="libs/miner/commons-collections4-4.0.jar"

#CLASSPATH_JARS="$GUICE_JARS:$COUCHDB_JARS:$JENA_JARS:$APACHE_COLLECTIONS_JARS"

CSV_JARS="TweetsParser/libs/csv/apache-commons-lang.jar:TweetsParser/libs/csv/commons-logging.jar:TweetsParser/libs/csv/opencsv-2.4.jar:TweetsParser/libs/wordnet/jwnl.jar" # TweetsParser/libs/csv/*"

PROJECTS_PATHS="UserDB/bin:MapDAO/bin:Persister/bin:Model/bin:Config/bin"

NEEDED_PATH="$(getJarsPath "libs/guice"):$CSV_JARS:$JENA_JARS:$APACHE_COLLECTIONS_JARS:$COUCHDB_JARS"
#$(getJarsPath "libs/jena"):$(getJarsPath "libs/miner")
#echo $NEEDED_PATH:$PROJECTS_PATHS

java -Xmx4g -cp "$NEEDED_PATH:$PROJECTS_PATHS:TweetsParser/bin" org.tweetsmining.engines.tweetsparser.main.TweetsParserMain

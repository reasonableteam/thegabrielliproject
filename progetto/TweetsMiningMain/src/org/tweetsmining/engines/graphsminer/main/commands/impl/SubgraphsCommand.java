/*******************************************************************************
 * Copyleft (c) 2014, "Massimiliano Leone - <maximilianus@gmail.com> - https://plus.google.com/+MassimilianoLeone"
 * This file (SubgraphsCommand.java) is part of TweetsMiningMain.
 * 
 *     SubgraphsCommand.java is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     SubgraphsCommand.java is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with .  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.tweetsmining.engines.graphsminer.main.commands.impl;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.tweetsmining.engines.graphsminer.main.analyzer.graph.UserToMultiLayerGraphMapHolder;
import org.tweetsmining.engines.graphsminer.main.analyzer.subgraphs.DbToClusterHolder;
import org.tweetsmining.engines.graphsminer.main.analyzer.subgraphs.MineDifferencesResultHolder;
import org.tweetsmining.engines.graphsminer.main.commands.base.Command;
import org.tweetsmining.engines.graphsminer.main.commands.base.exceptions.CommandException;
import org.tweetsmining.engines.graphsminer.main.utils.PersistenceUtils;
import org.tweetsmining.engines.graphsminer.mining.MineUsers;
import org.tweetsmining.model.graph.MultiLayerGraph;

public class SubgraphsCommand implements Command {

	@Override
	public void execute(String... commandArgs) throws CommandException {
		try {
			
			String userToMultiLayerGraphMapHolderFileNameToRead = commandArgs[0];
			String dbToClusterHolderFileNameToRead = commandArgs[1];
		
			DbToClusterHolder dbToClusterHolder = PersistenceUtils.<DbToClusterHolder>readFromFile(dbToClusterHolderFileNameToRead);
			// String sono gli id/user degli utenti
			List<List<String>> dbToCluster = dbToClusterHolder.getDbToCluster();
			
			Map<String, MultiLayerGraph> userToMultiLayerGraphsMap = 
					PersistenceUtils.<UserToMultiLayerGraphMapHolder>readFromFile(userToMultiLayerGraphMapHolderFileNameToRead)
						.getUserToMultiLayerGraphsMap();
			
			int inflate = Integer.parseInt(commandArgs[2]);
			int iterate = Integer.parseInt(commandArgs[3]);
			
			MineUsers mineUsers = new MineUsers(userToMultiLayerGraphsMap, inflate, iterate);
			
			Map<List<String>, List<MultiLayerGraph>> mineDifferences = mineUsers.mineDifferences(dbToCluster);
			
			MineDifferencesResultHolder mineDifferencesResultHolder = new MineDifferencesResultHolder(mineDifferences);
			
			String mineDifferencesHolderSerializedFileName = commandArgs[4];
		
			PersistenceUtils.writeToFile(mineDifferencesHolderSerializedFileName, mineDifferencesResultHolder);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new CommandException(e);
		} catch (IOException e) {
			e.printStackTrace();
			throw new CommandException(e);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			throw new CommandException(e);
		}
	}

}

/*******************************************************************************
 * Copyleft (c) 2014, "Massimiliano Leone - <maximilianus@gmail.com> - https://plus.google.com/+MassimilianoLeone"
 * This file (UserToMultiLayerGraphMapHolder.java) is part of TweetsMiningMain.
 * 
 *     UserToMultiLayerGraphMapHolder.java is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     UserToMultiLayerGraphMapHolder.java is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with .  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.tweetsmining.engines.graphsminer.main.analyzer.graph;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.Map;

import org.tweetsmining.model.graph.MultiLayerGraph;

public class UserToMultiLayerGraphMapHolder implements Externalizable {

	private Map<String, MultiLayerGraph> userToMultiLayerGraphsMap;

	public UserToMultiLayerGraphMapHolder(Map<String, MultiLayerGraph> userToMultiLayerGraphsMap) {
		this.userToMultiLayerGraphsMap = userToMultiLayerGraphsMap;
	}

	@Override
	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {		
		int size = in.readInt();
		
		for (int i = 0; i< size; i++) {
			String user = (String) in.readObject();
			String multiLayerGraphsAsString = (String) in.readObject();
			MultiLayerGraph multiLayerGraph = new MultiLayerGraph(multiLayerGraphsAsString); 
			userToMultiLayerGraphsMap.put(user, multiLayerGraph);
		}
	}

	@Override
	public void writeExternal(ObjectOutput out) throws IOException {
		int size = userToMultiLayerGraphsMap.size();
		out.writeInt(size);
		for (String user: userToMultiLayerGraphsMap.keySet()) {
			MultiLayerGraph multiLayerGraph = userToMultiLayerGraphsMap.get(user);
			out.writeObject(user);
			out.writeObject(multiLayerGraph.toString());
		}
	}
	
	public Map<String, MultiLayerGraph> getUserToMultiLayerGraphsMap() {
		return userToMultiLayerGraphsMap;
	}

}

/*******************************************************************************
 * Copyleft (c) 2014, "Massimiliano Leone - <maximilianus@gmail.com> - https://plus.google.com/+MassimilianoLeone"
 * This file (MineCommand.java) is part of TweetsMiningMain.
 * 
 *     MineCommand.java is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     MineCommand.java is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with .  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.tweetsmining.engines.graphsminer.main.commands.impl;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.tweetsmining.engines.graphsminer.main.analyzer.graph.UserToMultiLayerGraphMapHolder;
import org.tweetsmining.engines.graphsminer.main.analyzer.subgraphs.DbToClusterHolder;
import org.tweetsmining.engines.graphsminer.main.commands.base.Command;
import org.tweetsmining.engines.graphsminer.main.commands.base.exceptions.CommandException;
import org.tweetsmining.engines.graphsminer.main.utils.PersistenceUtils;
import org.tweetsmining.engines.graphsminer.mining.MineUsers;

public class MineCommand implements Command {

	@Override
	public void execute(String... commandArgs) throws CommandException {
		String userToMultiLayerGraphHolderSerialized = commandArgs[0];
		UserToMultiLayerGraphMapHolder userToMultiLayerGraphsHolder;
		try {
			
			userToMultiLayerGraphsHolder = PersistenceUtils.<UserToMultiLayerGraphMapHolder>readFromFile(userToMultiLayerGraphHolderSerialized);
			
			int inflate = Integer.parseInt(commandArgs[1]);
			int iterate = Integer.parseInt(commandArgs[2]);
			
			MineUsers mineUsers = new MineUsers(userToMultiLayerGraphsHolder.getUserToMultiLayerGraphsMap(), inflate, iterate);
			mineUsers.setDB(db);
			List<List<String>> dbToCluster = mineUsers.dbToCluster();
			
			String dbToClusterSerializedName = commandArgs[3];
			PersistenceUtils.writeToFile(dbToClusterSerializedName, new DbToClusterHolder(dbToCluster) );
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			throw new CommandException(e);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new CommandException(e);
		} catch (IOException e) {
			e.printStackTrace();
			throw new CommandException(e);
		}
	}
}

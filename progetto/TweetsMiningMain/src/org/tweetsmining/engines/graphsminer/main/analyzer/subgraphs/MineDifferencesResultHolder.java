/*******************************************************************************
 * Copyleft (c) 2014, "Massimiliano Leone - <maximilianus@gmail.com> - https://plus.google.com/+MassimilianoLeone"
 * This file (MineDifferencesResultHolder.java) is part of TweetsMiningMain.
 * 
 *     MineDifferencesResultHolder.java is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     MineDifferencesResultHolder.java is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with .  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.tweetsmining.engines.graphsminer.main.analyzer.subgraphs;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.List;
import java.util.Map;

import org.tweetsmining.model.graph.MultiLayerGraph;

public class MineDifferencesResultHolder implements Externalizable {

	private Map<List<String>, List<MultiLayerGraph>> mineDifferences;

	public MineDifferencesResultHolder(Map<List<String>, List<MultiLayerGraph>> mineDifferences) {
		this.mineDifferences = mineDifferences;
	}

//	@SuppressWarnings("unchecked")
	@Override
	public void readExternal(ObjectInput objectInput) throws IOException, ClassNotFoundException {
		mineDifferences = (Map<List<String>, List<MultiLayerGraph>>) objectInput.readObject();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		objectOutput.writeObject(mineDifferences);
	}

}

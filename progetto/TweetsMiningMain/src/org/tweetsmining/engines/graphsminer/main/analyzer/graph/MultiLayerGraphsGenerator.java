/*******************************************************************************
 * Copyleft (c) 2014, "Massimiliano Leone - <maximilianus@gmail.com> - https://plus.google.com/+MassimilianoLeone"
 * This file (MultiLayerGraphsGenerator.java) is part of TweetsMiningMain.
 * 
 *     MultiLayerGraphsGenerator.java is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     MultiLayerGraphsGenerator.java is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with .  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.tweetsmining.engines.graphsminer.main.analyzer.graph;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.text.html.parser.Entity;

import org.tweetsmining.engines.graphsminer.graphs.GraphPruning;
import org.tweetsmining.engines.wordsrelator.model.RelationType;
import org.tweetsmining.engines.wordsrelator.model.RelationedTerm;
import org.tweetsmining.engines.wordsrelator.model.WordRelations;
import org.tweetsmining.model.graph.IAdjacencyGraph;
import org.tweetsmining.model.graph.MultiLayerGraph;

public class MultiLayerGraphsGenerator {
	
	public UserToMultiLayerGraphMapHolder generateGraphs(Map<String, List<WordRelations>> userToWordsMapFromSerializedFile) {
		
		Map<String, MultiLayerGraph> userToMultiLayerGraphMap = new HashMap<>();
		for (String user: userToWordsMapFromSerializedFile.keySet()) {
			MultiLayerGraph multiLayerGraph = new MultiLayerGraph();
			List<WordRelations> list = userToWordsMapFromSerializedFile.get(user);
			for (WordRelations wordRelations: list) {
				String word = wordRelations.getWord();
				Set<String> mainsenses = wordRelations.getMainsenses();
				Map<String, Set<RelationedTerm>> mainsensesToTermMap = wordRelations.getMainsensesToTermMap();
				for (String mainsense: mainsenses) {
					addToMultiLayerGraph(word, mainsense, multiLayerGraph, RelationType.DISAMBIGUATION);
					Set<RelationedTerm> relationedTerms = mainsensesToTermMap.get(mainsense);
					for (RelationedTerm relationedTerm: relationedTerms) {
						addToMultiLayerGraph(mainsense, relationedTerm.getTerm(), multiLayerGraph, relationedTerm.getRelationType());
					}
				}
			}
		}
		
		// old
/*		Map<String, MultiLayerGraph> userToMultiLayerGraphsMap = new HashMap<>();
		for (String user: userToWordsMapFromSerializedFile.keySet()) {
			Set<String> stemmedWords = userToWordsMapFromSerializedFile.get(user);
			MultiLayerGraph multiLayerGraph = new MultiLayerGraph();
			for (String stemmedWord: stemmedWords) {
				wordsRelator.findRelationsForWordAndAddToMultiLayerGraph(stemmedWord, multiLayerGraph);
			}			
			userToMultiLayerGraphsMap.put(user, multiLayerGraph);			
		}
		*/
		new GraphPruning<>(null).getVerticesAboveMode(db);
		GraphPruning.getVerticesAboveMode(userToMultiLayerGraphMap);
		
		return new UserToMultiLayerGraphMapHolder(userToMultiLayerGraphMap);
	}
	
	
	protected void addToMultiLayerGraph(String word, String term, MultiLayerGraph multiLayerGraph, RelationType relationType) {
		String uriPrefix = "http://tweetmining.org/rel/"; // to be extract ?
		IAdjacencyGraph<Entity> adjacencyGraph = multiLayerGraph.newLayer(uriPrefix + relationType.name() ); // to be trimmed, bla bla		
		adjacencyGraph.addEdge(word,term);
	}
}

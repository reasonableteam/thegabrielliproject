/*******************************************************************************
 * Copyleft (c) 2014, "Massimiliano Leone - <maximilianus@gmail.com> - https://plus.google.com/+MassimilianoLeone"
 * This file (HelpCommand.java) is part of TweetsMiningMain.
 * 
 *     HelpCommand.java is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     HelpCommand.java is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with .  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.tweetsmining.engines.graphsminer.main.commands.impl;

import org.tweetsmining.engines.graphsminer.main.commands.base.Command;
import org.tweetsmining.engines.graphsminer.main.commands.base.exceptions.CommandException;

public class HelpCommand implements Command {

	@Override
	public void execute(String... commandArgs) throws CommandException {
		System.out.println("TweetMinings:"
//			+"\n"+COMMAND_LINE_ARGUMENTS_STRING_PARSE+" [dirs to parse]: parse csv"
//			+"\n"+COMMAND_LINE_ARGUMENTS_STRING_PARSE_ANALYZE+" [dirs to parse]: parse csv and start analysis"
			+"\n"+COMMAND_LINE_ARGUMENTS_STRING_GENERATE_GRAPHS+" ['userToRelationedWordsAsSerializedInputFile'.ser 'userToMultiLayerGraphAsSerializedOutputFile'.ser]: generate multilayergraphs"
//			+"\n"+COMMAND_LINE_ARGUMENTS_STRING_ANALYZE+" ['userToMultiLayerGraphAsSerializedOutputFile'.ser]: analyze serialized files"
			+"\n"+COMMAND_LINE_ARGUMENTS_STRING_MINING+" ['userToMultiLayerGraphAsSerializedInputFileToMiningOn'.ser] [inflateParameter (int)] [iteratParameter (int)] ['dbToClusterSerializedNameAsOutput'.ser]: make data mining on tweets and generate subgraphs"
			+"\n"+COMMAND_LINE_ARGUMENTS_STRING_SUBGRAPHS+" ['userToMultiLayerGraphAsSerializedInputFile'.ser] ['dbToClusterSerializedNameAsInput'.ser] [iterateParameter (int)] [inflatParameter (int)] ['mineDifferencesHolderSerializedFileNameAsOutputFile'.ser]: analyze serialized files"
		);
	}
}

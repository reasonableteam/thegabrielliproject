/*******************************************************************************
 * Copyleft (c) 2014, "Massimiliano Leone - <maximilianus@gmail.com> - https://plus.google.com/+MassimilianoLeone"
 * This file (GraphMinerMain.java) is part of TweetsMiningMain.
 * 
 *     GraphMinerMain.java is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     GraphMinerMain.java is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with .  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.tweetsmining.engines.graphsminer.main;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.tweetsmining.engines.graphsminer._guice.GraphMinerModule;
import org.tweetsmining.engines.graphsminer.main.commands.Commands;
import org.tweetsmining.engines.graphsminer.main.commands.base.exceptions.CommandException;
import org.tweetsmining.engines.graphsminer.main.commands.impl.HelpCommand;

import com.google.inject.Guice;
import com.google.inject.Injector;

public class GraphMinerMain {

//	private static Injector injector;
	private final Commands argumentsCommands;
	
	@Inject
	public GraphMinerMain(Commands argumentsCommands) {
		this.argumentsCommands = argumentsCommands;
	}
	
	public void executeCommand(String command, String... commandArgs) throws CommandException {
		argumentsCommands.getCommand(command).execute(commandArgs);
	}
	
	

	public static void main(String[] args) {
		try {
			if (args.length > 0) {
				Injector injector = Guice.createInjector(new GraphMinerModule());
				GraphMinerMain graphMinerMain = injector.getInstance(GraphMinerMain.class);
				
				String command = args[0];
				List<String> commandArgs = new ArrayList<>();
				for (int i = 1; i < args.length; i++) {
					commandArgs.add(args[i]);
				}
				String[] commandArgsArray = new String[commandArgs.size()];
				commandArgs.toArray(commandArgsArray);
				graphMinerMain.executeCommand(command, commandArgsArray);
			} else
				new HelpCommand().execute(null);
		} catch (CommandException e) {
			e.printStackTrace();
		}
	}

}

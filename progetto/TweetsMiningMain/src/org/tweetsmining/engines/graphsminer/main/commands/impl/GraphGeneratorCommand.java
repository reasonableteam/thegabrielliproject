/*******************************************************************************
 * Copyleft (c) 2014, "Massimiliano Leone - <maximilianus@gmail.com> - https://plus.google.com/+MassimilianoLeone"
 * This file (GraphGeneratorCommand.java) is part of TweetsMiningMain.
 * 
 *     GraphGeneratorCommand.java is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     GraphGeneratorCommand.java is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with .  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.tweetsmining.engines.graphsminer.main.commands.impl;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.tweetsmining.engines.graphsminer.main.analyzer.graph.MultiLayerGraphsGenerator;
import org.tweetsmining.engines.graphsminer.main.analyzer.graph.UserToMultiLayerGraphMapHolder;
import org.tweetsmining.engines.graphsminer.main.commands.base.Command;
import org.tweetsmining.engines.graphsminer.main.commands.base.exceptions.CommandException;
import org.tweetsmining.engines.graphsminer.main.utils.PersistenceUtils;
import org.tweetsmining.engines.wordsrelator.model.WordRelations;

public class GraphGeneratorCommand implements Command {
	
//	private TweetsParser tweetsParser;
	private MultiLayerGraphsGenerator graphsGenerator;
	
	@Inject
	public GraphGeneratorCommand(/*TweetsParser tweetsParser,*/ MultiLayerGraphsGenerator graphsGenerator) {
//		this.tweetsParser = tweetsParser;
		this.graphsGenerator = graphsGenerator;
	}

	@Override
	public void execute(String... commandArgs) throws CommandException {
		Map<String, List<WordRelations>> userToWordsMapFromSerializedFile;
		try {
			String userToWordRelationsListMapSerializedFileName = commandArgs[0];
			userToWordsMapFromSerializedFile =
					// read from persister instead of tweetsparser
					PersistenceUtils.<Map<String, List<WordRelations>>readFromFile(userToWordRelationsListMapSerializedFileName);
//					tweetsParser.getUserToWordsMapFromSerializedFile(userToWordRelationsListMapSerializedFileName);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			throw new CommandException(e);
		} catch (IOException e) {
			e.printStackTrace();
			throw new CommandException(e);
		}
		UserToMultiLayerGraphMapHolder userToMultiLayerGraphMapHolder = graphsGenerator.generateGraphs(userToWordsMapFromSerializedFile);
		
		String multiLayerGraphHolderSerializedFileName = commandArgs[1];		
		try {
			PersistenceUtils.writeToFile(multiLayerGraphHolderSerializedFileName, userToMultiLayerGraphMapHolder);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new CommandException(e);
		} catch (IOException e) {
			e.printStackTrace();
			throw new CommandException(e);
		}
	}

}

/*******************************************************************************
 * Copyleft (c) 2014, "Massimiliano Leone - <maximilianus@gmail.com> - https://plus.google.com/+MassimilianoLeone"
 * This file (PersistenceUtils.java) is part of TweetsMiningMain.
 * 
 *     PersistenceUtils.java is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     PersistenceUtils.java is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with .  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.tweetsmining.engines.graphsminer.main.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class PersistenceUtils {

	@SuppressWarnings("unchecked")
	public static <T> T readFromFile(String fileNameToRead) throws FileNotFoundException, IOException, ClassNotFoundException {
		ObjectInputStream objectInputStream = new ObjectInputStream( new BufferedInputStream( new FileInputStream(fileNameToRead)));
		T t = (T) objectInputStream.readObject();
		objectInputStream.close();
		return t;
	}
	public static void writeToFile(String fileNameToWrite, Object objectToWrite) throws FileNotFoundException, IOException {
		ObjectOutputStream objectOutputStream = new ObjectOutputStream( new BufferedOutputStream( new FileOutputStream(fileNameToWrite)));
		objectOutputStream.writeObject(objectToWrite);
		objectOutputStream.close();
	}
}

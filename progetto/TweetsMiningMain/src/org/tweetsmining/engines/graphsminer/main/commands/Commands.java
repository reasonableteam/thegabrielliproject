/*******************************************************************************
 * Copyleft (c) 2014, "Massimiliano Leone - <maximilianus@gmail.com> - https://plus.google.com/+MassimilianoLeone"
 * This file (Commands.java) is part of TweetsMiningMain.
 * 
 *     Commands.java is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     Commands.java is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with .  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.tweetsmining.engines.graphsminer.main.commands;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import org.tweetsmining.engines.graphsminer.main.commands.base.Command;
import org.tweetsmining.engines.graphsminer.main.commands.impl.GraphGeneratorCommand;
import org.tweetsmining.engines.graphsminer.main.commands.impl.HelpCommand;
import org.tweetsmining.engines.graphsminer.main.commands.impl.MineCommand;
import org.tweetsmining.engines.graphsminer.main.commands.impl.SubgraphsCommand;

public class Commands {
	
	private Map<String,Command> commands = new HashMap<String, Command>();
	
	@Inject
	public Commands(/*ParseCommand parseCommand,*/ GraphGeneratorCommand graphGeneratorCommand, MineCommand mineCommand, SubgraphsCommand subgraphsCommand) {
		populateCommands(/*parseCommand, */graphGeneratorCommand, mineCommand, subgraphsCommand);
	}
	
	private void populateCommands(/*ParseCommand parseCommand,*/ GraphGeneratorCommand graphGeneratorCommand, MineCommand mineCommand, SubgraphsCommand subgraphsCommand) {
		commands.put("help", new HelpCommand() );
//		commands.put(Command.COMMAND_LINE_ARGUMENTS_STRING_PARSE, parseCommand ); // parse tweets and generate serialized		
		commands.put(Command.COMMAND_LINE_ARGUMENTS_STRING_GENERATE_GRAPHS, graphGeneratorCommand); // read serialized tweets and generate multilayergraph
		commands.put(Command.COMMAND_LINE_ARGUMENTS_STRING_MINING, mineCommand);
		commands.put(Command.COMMAND_LINE_ARGUMENTS_STRING_SUBGRAPHS, subgraphsCommand);
	}
	
	public Command getCommand(String command) {
		return commands.get(command);
	}

}

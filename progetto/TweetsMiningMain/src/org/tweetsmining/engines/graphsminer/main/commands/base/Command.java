/*******************************************************************************
 * Copyleft (c) 2014, "Massimiliano Leone - <maximilianus@gmail.com> - https://plus.google.com/+MassimilianoLeone"
 * This file (Command.java) is part of TweetsMiningMain.
 * 
 *     Command.java is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 * 
 *     Command.java is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 * 
 *     You should have received a copy of the GNU General Public License
 *     along with .  If not, see <http://www.gnu.org/licenses/>.
 ******************************************************************************/
package org.tweetsmining.engines.graphsminer.main.commands.base;

import org.tweetsmining.engines.graphsminer.main.commands.base.exceptions.CommandException;

public interface Command {
	void execute(String... commandArgs) throws CommandException;
	
	static String COMMAND_LINE_ARGUMENTS_STRING_PARSE = "p";
	static String COMMAND_LINE_ARGUMENTS_STRING_PARSE_ANALYZE = "pa";
	static String COMMAND_LINE_ARGUMENTS_STRING_GENERATE_GRAPHS = "g";
	static String COMMAND_LINE_ARGUMENTS_STRING_ANALYZE = "a";
	static String COMMAND_LINE_ARGUMENTS_STRING_MINING = "m";
	static String COMMAND_LINE_ARGUMENTS_STRING_SUBGRAPHS = "s"; 
}

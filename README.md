# README #

This is an academic project for Artifical Intelligence exam. 

The main idea is a twitter **sentiment analysis**, starting from a focused hashtag: 
we choised a political event, the launch of shoe against Hillary Clinton.

The application consts of various parts:

1) the tweets _stemming_
2) the creation of a _wordnet_ using Babelnet, starting from stemmed words
3) the _analysis_ of this wordnet using typical AI algorithms: A* and gSpan
4) the _clustering_ of analyzed data, searching from similar patterns

The application also uses tipical architecture and common component 
for its levels: Guice as IoC container, CouchDB (version 1) and BerkeleyDB 
(version 2) for persistence (hence the data are just key-value couples), 
and so on.

The mathematical model for the data is **hypergraph**, that is a (vertical) 
multilevel of connected graphs (each graph lives in a level).
The **A* algorithm** is so used for levels traversal, while the **gSpan** 
is useful for recognize subgraphs isomorphism within a certain subset 
of graphs: finally, the isomorphism recognition gives us the base for
data clustering.

As at beginning, this is an academic project, so most of develop is 
"by hand" (and this project has been completed in 2014): actually (2019), a lot of 
"bad" works could be done by modern architecture/components for big data,
such as a kafka+spark pipeline in stemming+net_creation phases, 
and some library for data mining as weka or similar.

We hope this could be a inspiration for future computer scientist ;) .
Enjoy it!
